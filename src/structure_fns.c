#include <stdio.h>
#include <math.h>
#include <errno.h>
#include "header"

extern int kmax;
extern PREC H,dzsav;

extern PREC r10,r01,r20,r11a,r11b,r02,*c_root0,zc,c,c0;
extern PREC alpha10,alpha01,alpha20,alpha11a,alpha11b,alpha02,*E10;
extern int I;
extern char file_in[BUFSIZ],file_out[BUFSIZ];
extern PREC subphi1,subphi2,subphi3,subphi4,subphi5,subphi6;

int i_max;
PREC subphi,dsubphi,subz,dsubphi1,dsubphi2,dsubphi3,dsubphi4,dsubphi5,dsubphi6;

/***************************************************************************/
/*****                 Linear vertical structure function              *****/
/***************************************************************************/

void get_phi(nvar,step,z,phi,phi_dz,v)
int nvar,step;
PREC v,*z,*phi,*phi_dz;
{
int i,nok,nbad;
PREC Dz,dz,phi_max, *y,*VECTOR();
void free_VECTOR(),load(),score(),odeint(),derivs1();

y = VECTOR(1,nvar);

Dz = H/(step-1);
dz = Dz/kmax;
dzsav = 0.8*dz;

load(z[1],&v,y);
phi[1] = y[1];
phi_dz[1] = y[2];

for(i=2;i<=step;++i){
   odeint(y,nvar,z[i-1],z[i],eps2,dz,0.0,&nok,&nbad,derivs1);
   phi[i] = y[1];
   phi_dz[i] = y[2];
  }
  
phi_max = 0.0;
for(i=1;i<=step;++i){
   if(fabs(phi[i])>phi_max){
   i_max = i;
   phi_max = fabs(phi[i]);
 }
 }
   
printf("maximum value of phi, set to one, is at z[%d] = %f\n\n",i_max,z[i_max]);
for(i=1;i<=step;++i){
   phi[i] = phi[i]/phi_max;
   phi_dz[i] = phi_dz[i]/phi_max;
 }
  
free_VECTOR(y,1);
 }

/***************************************************************************/
/***************************************************************************/
/*****                                                                 *****/
/*****       First-order dispersion vertical structure function        *****/
/*****                                                                 *****/
/***************************************************************************/
/***************************************************************************/

void get_phi01(nvar,step,z,phi,phi_dz,phi01,phi01_dz)
int nvar,step;
PREC *z,*phi,*phi_dz,*phi01,*phi01_dz;
{
int i,nok,nbad;
PREC Dz,dz,*y,*VECTOR(),U0();
void free_VECTOR(),load2(),score(),odeint(),derivs5();
  
y = VECTOR(1,nvar);
Dz = H/(step-1);
dz = Dz/kmax;
dzsav = 0.8*dz;

load2(z[1],y);
phi01[1] = y[1];
phi01_dz[1] = y[2];

for(i=2;i<=step;++i){
   subphi1=phi[i-1];
   odeint(y,nvar,z[i-1],z[i],eps2,dz,0.0,&nok,&nbad,derivs5);
   phi01[i] = y[1];
   phi01_dz[i] = y[2];
 }

for(i=1;i<=step;++i){
   phi01[i] = phi01[i]+alpha01*phi[i];
   phi01_dz[i] = phi01_dz[i]+alpha01*phi_dz[i];
 }

printf("Ending get_phi01.    r01 = %1.12e\n",r01);
free_VECTOR(y,1);
}

/***************************************************************************/
/***************************************************************************/
/*****                                                                 *****/
/*****       First-order nonlinear vertical structure function         *****/
/*****                                                                 *****/
/***************************************************************************/
/***************************************************************************/
void get_phi10(step,z,phi,phi_dz,phi10,phi10_dz)
int step;
PREC *z,*phi,*phi_dz,*phi10,*phi10_dz;
{
int i;
PREC Dz,dz,*phi10star,*phi10star_dz,*VECTOR();
void free_VECTOR(),get_crit_alp10(),shoot10(),load10(),derivs10();
PREC v,delv,f;

phi10star = VECTOR(1,step);
phi10star_dz = VECTOR(1,step);

Dz = z[2] - z[1];
dz = Dz/kmax;
dzsav = 0.8*dz;


printf("Starting get_phi10.    r10 = %1.16e\n",r10);
v = r10;
delv = 0.00001*v;
phi10star[1] = 0.0;
phi10star_dz[1] = 0.0;
shoot10(step,&v,&delv,eps2,dz,0.0,&f,derivs10,load10
                         ,z,phi,phi10star,phi10star_dz);
while( fabs(f) > 0.000000001){ 
   shoot10(step,&v,&delv,eps2,dz,0.0,&f,derivs10,load10,
                              z,phi,phi10star,phi10star_dz);
 }
r10 = v;

/*
alpha10 = -phi10star[i_max]/phi[i_max];
printf("i_max = %d,  phi10star = %1.6e,  phi = %1.6e\n",i_max,phi10star[i_max],phi[i_max]);
alpha10 = 0.0;
*/
printf("In get_phi10 (structure_fns.c) using alpha10 = %1.6e\n\n",alpha10);

for(i=1;i<=step;++i){
   phi10[i] = phi10star[i]+alpha10*phi[i];
   phi10_dz[i] = phi10star_dz[i]+alpha10*phi_dz[i];
 }

printf("Ending get_phi10.      r10 = %1.16e\n",r10);

free_VECTOR(phi10star,1);
free_VECTOR(phi10star_dz,1);
}

/************************************************************************/
/************************************************************************/
void shoot10(step,v,delv,eps,h1,hmin,f,derivs10,load10,z,phi,phi10star,phi10star_dz)
PREC *v,*delv,*f,eps,h1,hmin,*z,*phi,*phi10star,*phi10star_dz;
void (*derivs10)(),(*load10)();
int step;
{
PREC *y,*VECTOR(),dff,dfdv;
void odeint(),free_VECTOR();
int nok,nbad,i;

y = VECTOR(1,3);
load10(v,y);
for(i=2;i<=step;++i){
    subphi=phi[i-1];
    subphi2=phi[i-1]*phi[i-1];
    dsubphi = (phi[i]-phi[i-1])/(z[i]-z[i-1]);
    dsubphi2 = (phi[i]*phi[i]-subphi2)/(z[i]-z[i-1]);
    subz = z[i-1];
    odeint(y,3,z[i-1],z[i],eps,h1,hmin,&nok,&nbad,derivs10);
    phi10star[i] = y[1];
    phi10star_dz[i] = y[2];
  }
*f = y[1];
*v += *delv;
load10(v,y);
for(i=2;i<=step;++i){
    subphi = phi[i-1];
    subphi2 = phi[i-1]*phi[i-1];
    dsubphi = (phi[i]-phi[i-1])/(z[i]-z[i-1]);
    dsubphi2 = (phi[i]*phi[i]-subphi2)/(z[i]-z[i-1]);
    subz = z[i-1];
    odeint(y,3,z[i-1],z[i],eps,h1,hmin,&nok,&nbad,derivs10);
    phi10star[i] = y[1];
    phi10star_dz[i] = y[2];
  }
dff = y[1];
dfdv = (dff- *f)/(*delv);
dff = -dff/dfdv;
*delv = dff;

free_VECTOR(y,1);
}

/***************************************************************************/
/***************************************************************************/
/*****                                                                 *****/
/*****      Second-order nonlinear vertical structure function         *****/
/*****                                                                 *****/
/***************************************************************************/
/***************************************************************************/

void get_phi20(step,z,phi,phi_dz,phi20,phi20_dz)
int step;
PREC *z,*phi,*phi_dz,*phi20,*phi20_dz;
{
int i;
PREC Dz,dz,*phi20star,*phi20star_dz,*VECTOR();
void free_VECTOR(),shoot20(),derivs20();
PREC v,delv,f;

phi20star = VECTOR(1,step);
phi20star_dz = VECTOR(1,step);

Dz = z[2] - z[1];
dz = Dz/kmax;
dzsav = 0.8*dz;

printf("Starting get_phi20.    r20 = %1.16e\n",r20);
v = r20;
delv = 0.00001*v;
phi20star[1] = 0.0;
phi20star_dz[1] = 0.0;
shoot20(step,&v,&delv,eps2,dz,0.0,&f,derivs20,z,phi20star,phi20star_dz);
while( fabs(phi20star[step]) > 0.000000001)
   shoot20(step,&v,&delv,eps2,dz,0.0,&f,derivs20,z,phi20star,phi20star_dz);
r20 = v;

for(i=1;i<=step;++i){
   phi20[i] = phi20star[i]+alpha20*phi[i];
   phi20_dz[i] = phi20star_dz[i]+alpha20*phi_dz[i];
 }
printf("Ending get_phi20.      r20 = %1.16e\n",r20);

free_VECTOR(phi20star,1);
free_VECTOR(phi20star_dz,1);
}

/************************************************************************/
/************************************************************************/
void shoot20(step,v,delv,eps,h1,hmin,f,derivs20,z,phi20star,phi20star_dz)

PREC *v,*delv,*f,eps,h1,hmin,*z,*phi20star,*phi20star_dz;
void (*derivs20)();
int step;
{
PREC *y,*VECTOR(),dff,dfdv,invdz;
extern PREC *phi,*phi_dz,*phi10,*phi10_dz;
void free_VECTOR(),odeint();
int nok,nbad,i;

y = VECTOR(1,3);

y[1] = 0.0;
y[2] = 0.0;
y[3] = *v;

for(i=2;i<=step;++i){
    subphi6=phi[i-1];
    subphi3=subphi6*subphi6;
    subphi2=subphi3*subphi6;
    subphi4=subphi6*phi_dz[i-1];
    subphi5=phi10[i-1];
    subphi1=subphi6*subphi5;
    invdz = 1.0/(z[i]-z[i-1]);
    dsubphi1=(phi[i]*phi10[i] - subphi1)*invdz;
    dsubphi2=(phi[i]*phi[i]*phi[i] - subphi2)*invdz;
    dsubphi3=(phi[i]*phi[i] - subphi3)*invdz;
    dsubphi4=(phi[i]*phi_dz[i] - subphi4)*invdz;
    dsubphi5=(phi10[i] - subphi5)*invdz;
    dsubphi6=(phi[i] - subphi6)*invdz;
    subz = z[i-1];
    odeint(y,3,z[i-1],z[i],eps,h1,hmin,&nok,&nbad,derivs20);
    phi20star[i] = y[1];
    phi20star_dz[i] = y[2];
  }
*f = y[1];
*v += *delv;
y[1] = 0.0;
y[2] = 0.0;
y[3] = *v;
for(i=2;i<=step;++i){

    subphi6=phi[i-1];
    subphi3=subphi6*subphi6;
    subphi2=subphi3*subphi6;
    subphi4=subphi6*phi_dz[i-1];
    subphi5=phi10[i-1];
    subphi1=subphi6*subphi5;
    invdz = 1.0/(z[i]-z[i-1]);
    dsubphi1=(phi[i]*phi10[i] - subphi1)*invdz;
    dsubphi2=(phi[i]*phi[i]*phi[i] - subphi2)*invdz;
    dsubphi3=(phi[i]*phi[i] - subphi3)*invdz;
    dsubphi4=(phi[i]*phi_dz[i] - subphi4)*invdz;
    dsubphi5=(phi10[i] - subphi5)*invdz;
    dsubphi6=(phi[i] - subphi6)*invdz;
    subz = z[i-1];
    odeint(y,3,z[i-1],z[i],eps,h1,hmin,&nok,&nbad,derivs20);
    phi20star[i] = y[1];
    phi20star_dz[i] = y[2];
  }
dff = y[1];
dfdv = (dff- *f)/(*delv);
dff = -dff/dfdv;
*delv = dff;

free_VECTOR(y,1);
}


/***************************************************************************/
/***************************************************************************/
/*****                                                                 *****/
/*****    First of nonlinear-dispersive vertical structure functions   *****/
/*****                                                                 *****/
/***************************************************************************/
/***************************************************************************/

void get_phi11a(step,z,phi,phi_dz,phi11a,phi11a_dz)
int step;
PREC *z,*phi,*phi_dz,*phi11a,*phi11a_dz;
{
int i;
PREC Dz,dz,*phi11astar,*phi11astar_dz,*VECTOR();
void free_VECTOR(),shoot11a(),derivs11a();
PREC v,delv,f;

phi11astar = VECTOR(1,step);
phi11astar_dz = VECTOR(1,step);

Dz = z[2] - z[1];
dz = Dz/kmax;
dzsav = 0.8*dz;

printf("Starting get_phi11a.    r11a = %1.16e\n",r11a);
v = r11a;
delv = 0.00001*v;
phi11astar[1] = 0.0;
phi11astar_dz[1] = 0.0;
shoot11a(step,&v,&delv,eps2,dz,0.0,&f,derivs11a,z,phi11astar,phi11astar_dz);
while( fabs(phi11astar[step]) > 0.000000001)
   shoot11a(step,&v,&delv,eps2,dz,0.0,&f,derivs11a,z,phi11astar,phi11astar_dz);
r11a = v;

for(i=1;i<=step;++i){
   phi11a[i] = phi11astar[i]+alpha11a*phi[i];
   phi11a_dz[i] = phi11astar_dz[i]+alpha11a*phi_dz[i];
 }
printf("Ending get_phi11a.      r11a = %1.16e\n",r11a);

free_VECTOR(phi11astar,1);
free_VECTOR(phi11astar_dz,1);
}

/************************************************************************/
/************************************************************************/
void shoot11a(step,v,delv,eps,h1,hmin,f,derivs11a,z,phi11astar,phi11astar_dz)

PREC *v,*delv,*f,eps,h1,hmin,*z,*phi11astar,*phi11astar_dz;
void (*derivs11a)();
int step;
{
PREC *y,*VECTOR(),dff,dfdv,invdz;
extern PREC *phi,*phi_dz,*phi10,*phi10_dz,*phi01,*phi01_dz;
void free_VECTOR(),odeint();
int nok,nbad,i;

y = VECTOR(1,3);

y[1] = 0.0;
y[2] = 0.0;
y[3] = *v;

for(i=2;i<=step;++i){
    subphi1=phi10[i-1];
    subphi6=phi[i-1];
    subphi5=phi01[i-1];
    subphi2 = subphi6*subphi5;
    subphi3 = subphi6*subphi6;
    subphi4 = subphi6*phi_dz[i-1];
    invdz = 1.0/(z[i]-z[i-1]);
    dsubphi1=(phi10[i] - subphi1)*invdz;
    dsubphi2=(phi[i]*phi01[i] - subphi2)*invdz;
    dsubphi3=(phi[i]*phi[i] - subphi3)*invdz;
    dsubphi4=(phi[i]*phi_dz[i] - subphi4)*invdz;
    dsubphi5=(phi01[i] - subphi5)*invdz;
    dsubphi6=(phi[i] - subphi6)*invdz;
    subz = z[i-1];
    odeint(y,3,z[i-1],z[i],eps,h1,hmin,&nok,&nbad,derivs11a);
    phi11astar[i] = y[1];
    phi11astar_dz[i] = y[2];
  }
*f = y[1];
*v += *delv;
y[1] = 0.0;
y[2] = 0.0;
y[3] = *v;
for(i=2;i<=step;++i){

    subphi1=phi10[i-1];
    subphi6=phi[i-1];
    subphi5=phi01[i-1];
    subphi2 = subphi6*subphi5;
    subphi3 = subphi6*subphi6;
    subphi4 = subphi6*phi_dz[i-1];
    invdz = 1.0/(z[i]-z[i-1]);
    dsubphi1=(phi10[i] - subphi1)*invdz;
    dsubphi2=(phi[i]*phi01[i] - subphi2)*invdz;
    dsubphi3=(phi[i]*phi[i] - subphi3)*invdz;
    dsubphi4=(phi[i]*phi_dz[i] - subphi4)*invdz;
    dsubphi5=(phi01[i] - subphi5)*invdz;
    dsubphi6=(phi[i] - subphi6)*invdz;
    subz = z[i-1];
    odeint(y,3,z[i-1],z[i],eps,h1,hmin,&nok,&nbad,derivs11a);
    phi11astar[i] = y[1];
    phi11astar_dz[i] = y[2];
  }
dff = y[1];
dfdv = (dff- *f)/(*delv);
dff = -dff/dfdv;
*delv = dff;

free_VECTOR(y,1);
}





/***************************************************************************/
/***************************************************************************/
/*****                                                                 *****/
/*****    First of nonlinear-dispersive vertical structure functions   *****/
/*****                                                                 *****/
/***************************************************************************/
/***************************************************************************/

void get_phi11b(step,z,phi,phi_dz,phi11b,phi11b_dz)
int step;
PREC *z,*phi,*phi_dz,*phi11b,*phi11b_dz;
{
int i;
PREC Dz,dz,*phi11bstar,*phi11bstar_dz,*VECTOR();
void free_VECTOR(),shoot11b(),derivs11b();
PREC v,delv,f;

phi11bstar = VECTOR(1,step);
phi11bstar_dz = VECTOR(1,step);

Dz = z[2] - z[1];
dz = Dz/kmax;
dzsav = 0.8*dz;

printf("Starting get_phi11b.    r11b = %1.16e\n",r11b);
v = r11b;
delv = 0.00001*v;
phi11bstar[1] = 0.0;
phi11bstar_dz[1] = 0.0;
shoot11b(step,&v,&delv,eps2,dz,0.0,&f,derivs11b,z,phi11bstar,phi11bstar_dz);
while( fabs(phi11bstar[step]) > 0.000000001)
   shoot11b(step,&v,&delv,eps2,dz,0.0,&f,derivs11b,z,phi11bstar,phi11bstar_dz);
r11b = v;

for(i=1;i<=step;++i){
   phi11b[i] = phi11bstar[i]+alpha11b*phi[i];
   phi11b_dz[i] = phi11bstar_dz[i]+alpha11b*phi_dz[i];
 }
printf("Ending get_phi11b.      r11b = %1.16e\n",r11b);

free_VECTOR(phi11bstar,1);
free_VECTOR(phi11bstar_dz,1);
}

/************************************************************************/
/************************************************************************/
void shoot11b(step,v,delv,eps,h1,hmin,f,derivs11b,z,phi11bstar,phi11bstar_dz)

PREC *v,*delv,*f,eps,h1,hmin,*z,*phi11bstar,*phi11bstar_dz;
void (*derivs11b)();
int step;
{
PREC *y,*VECTOR(),dff,dfdv,invdz;
extern PREC *phi,*phi_dz,*phi10,*phi10_dz,*phi01,*phi01_dz;
void free_VECTOR(),odeint();
int nok,nbad,i;

y = VECTOR(1,3);

y[1] = 0.0;
y[2] = 0.0;
y[3] = *v;

for(i=2;i<=step;++i){
    subphi1=phi10[i-1];
    subphi6=phi[i-1];
    subphi5=phi01[i-1];
    subphi2 = subphi6*subphi5;
    subphi3 = subphi6*subphi6;
    subphi4 = subphi6*phi_dz[i-1];
    invdz = 1.0/(z[i]-z[i-1]);
    dsubphi1=(phi10[i] - subphi1)*invdz;
    dsubphi2=(phi[i]*phi01[i] - subphi2)*invdz;
    dsubphi3=(phi[i]*phi[i] - subphi3)*invdz;
    dsubphi4=(phi[i]*phi_dz[i] - subphi4)*invdz;
    dsubphi5=(phi01[i] - subphi5)*invdz;
    dsubphi6=(phi[i] - subphi6)*invdz;
    subz = z[i-1];
    odeint(y,3,z[i-1],z[i],eps,h1,hmin,&nok,&nbad,derivs11b);
    phi11bstar[i] = y[1];
    phi11bstar_dz[i] = y[2];
  }
*f = y[1];
*v += *delv;
y[1] = 0.0;
y[2] = 0.0;
y[3] = *v;
for(i=2;i<=step;++i){

    subphi1=phi10[i-1];
    subphi6=phi[i-1];
    subphi5=phi01[i-1];
    subphi2 = subphi6*subphi5;
    subphi3 = subphi6*subphi6;
    subphi4 = subphi6*phi_dz[i-1];
    invdz = 1.0/(z[i]-z[i-1]);
    dsubphi1=(phi10[i] - subphi1)*invdz;
    dsubphi2=(phi[i]*phi01[i] - subphi2)*invdz;
    dsubphi3=(phi[i]*phi[i] - subphi3)*invdz;
    dsubphi4=(phi[i]*phi_dz[i] - subphi4)*invdz;
    dsubphi5=(phi01[i] - subphi5)*invdz;
    dsubphi6=(phi[i] - subphi6)*invdz;
    subz = z[i-1];
    odeint(y,3,z[i-1],z[i],eps,h1,hmin,&nok,&nbad,derivs11b);
    phi11bstar[i] = y[1];
    phi11bstar_dz[i] = y[2];
  }
dff = y[1];
dfdv = (dff- *f)/(*delv);
dff = -dff/dfdv;
*delv = dff;

free_VECTOR(y,1);
}



/***************************************************************************/
/***************************************************************************/
/*****                                                                 *****/
/*****    Second-order dispersive vertical structure function          *****/
/*****                                                                 *****/
/***************************************************************************/
/***************************************************************************/

void get_phi02(step,z,phi,phi_dz,phi02,phi02_dz)
int step;
PREC *z,*phi,*phi_dz,*phi02,*phi02_dz;
{
int i;
PREC Dz,dz,*phi02star,*phi02star_dz,*VECTOR();
void free_VECTOR(),shoot02(),derivs02();
PREC v,delv,f;

phi02star = VECTOR(1,step);
phi02star_dz = VECTOR(1,step);

Dz = z[2] - z[1];
dz = Dz/kmax;
dzsav = 0.8*dz;

printf("Starting get_phi02.    r02 = %1.16e\n",r02);
v = r02;
delv = -0.00001*v;
phi02star[1] = 0.0;
phi02star_dz[1] = 0.0;
shoot02(step,&v,&delv,eps2,dz,0.0,&f,derivs02,z,phi02star,phi02star_dz);
while( fabs(phi02star[step]) > 0.0000001)
   shoot02(step,&v,&delv,eps2,dz,0.0,&f,derivs02,z,phi02star,phi02star_dz);
r02 = v;

for(i=1;i<=step;++i){
   phi02[i] = phi02star[i]+alpha02*phi[i];
   phi02_dz[i] = phi02star_dz[i]+alpha02*phi_dz[i];
 }
printf("Ending get_phi02.      r02 = %1.16e\n",r02);

free_VECTOR(phi02star,1);
free_VECTOR(phi02star_dz,1);
}

/************************************************************************/
/************************************************************************/
void shoot02(step,v,delv,eps,h1,hmin,f,derivs02,z,phi02star,phi02star_dz)

PREC *v,*delv,*f,eps,h1,hmin,*z,*phi02star,*phi02star_dz;
void (*derivs02)();
int step;
{
PREC *y,*VECTOR(),dff,dfdv,invdz;
extern PREC *phi,*phi_dz,*phi10,*phi10_dz,*phi01,*phi01_dz;
void free_VECTOR(),odeint();
int nok,nbad,i;

y = VECTOR(1,3);

y[1] = 0.0;
y[2] = 0.0;
y[3] = *v;

for(i=2;i<=step;++i){
    subphi1=phi01[i-1];
    subphi2=phi[i-1];
    invdz = 1.0/(z[i]-z[i-1]);
    dsubphi1=(phi01[i] - subphi1)*invdz;
    dsubphi2=(phi[i] - subphi2)*invdz;
    subz = z[i-1];
    odeint(y,3,z[i-1],z[i],eps,h1,hmin,&nok,&nbad,derivs02);
    phi02star[i] = y[1];
    phi02star_dz[i] = y[2];
  }
*f = y[1];
printf("v = %1.12e     phi02(H) = %1.12e\n",*v,*f);
*v += *delv;
y[1] = 0.0;
y[2] = 0.0;
y[3] = *v;
for(i=2;i<=step;++i){

    subphi1=phi01[i-1];
    subphi2=phi[i-1];
    invdz = 1.0/(z[i]-z[i-1]);
    dsubphi1=(phi01[i] - subphi1)*invdz;
    dsubphi2=(phi[i] - subphi2)*invdz;
    subz = z[i-1];
    odeint(y,3,z[i-1],z[i],eps,h1,hmin,&nok,&nbad,derivs02);
    phi02star[i] = y[1];
    phi02star_dz[i] = y[2];
  }
printf("v = %1.12e     phi02(H) = %1.12e\n",*v,y[1]);
dff = y[1];
dfdv = (dff- *f)/(*delv);
dff = -dff/dfdv;
*delv = dff;

free_VECTOR(y,1);
}

