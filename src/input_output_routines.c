#include <stdio.h>
#include <math.h>
#include "header.h"

extern inline double xval(int i, double dx);
extern inline double zval(int j, double dz, double Lz);
double density(double),d_density(double ),NN0(double);
double Uback(double),dUback(double),d2Uback(double);

/*****************************************************************************/
/*****************************************************************************/
/***   Test for coefficients: 2-layer fluid                                ***/
/***                                                                       ***/
/***   c = sqrt(g'*h1*h2/(h1+h2),   beta = -r01 = c*h1*h2/6                ***/
/***  alpha = -2*r10*c    = 1.5*c*(h1-h2)/(h1*h2)                          ***/
/***  alpha1 = -3*r20*c*c = -3*c/(8*h1^2*h2^2)*(h1^2+h2^2+6*h1*h2)         ***/
/***                                                                       ***/
/***  for h1 = 40, h2 = 60, delta_rho/rho_0 = 0.01 get                     ***/
/***    c = 1.5344,  beta = 613.8, alpha = -0.01918, alpha1 = -0.001958    ***/
/***    ==> r10 = 6.25e-3,  r20 = 2.77e-4                                  ***/
/***                                                                       ***/
/*****************************************************************************/
/*****************************************************************************/

extern double H,g;
extern int UBACKGROUND;

/*********************************************************/
/*********************************************************/
double NN0(double z)
{
/*return(-g*d_density(z/1.025)); */
return(-g*d_density(z));
}

/*********************************************************/
/***     returns integral of f(x) = 1+tanh((x-a)/x)    ***/
/*********************************************************/

double inttanh(double x,double a,double s)
{
  if( (x-a)/s > 20.0) return(2*(x-a));
  else{
     if( (x-a)/s < -20.0) return(0.0);
     else return(x-a+s*(log(2.0*cosh( (x-a)/s))));
  }
}


/*********************************************************/
/*********************************************************/

void store_background_profiles(){
  int j;
  FILE *f1;
  double dz,Ri,Uzsq;

  extern int J;

  dz = H/J;
  f1 = fopen("density_vs_z","w");
  for(j=0;j<J;j++) fprintf(f1,"%1.8e  %1.8e\n",density(zval(j,dz,H)),zval(j,dz,H));
  fclose(f1);
  f1 = fopen("d_density_vs_z","w");
  for(j=0;j<J;j++) fprintf(f1,"%1.8e  %1.8e\n",d_density(zval(j,dz,H)),zval(j,dz,H));
  fclose(f1);
  f1 = fopen("density_vs_z","w");
  for(j=0;j<J;j++) fprintf(f1,"%1.8e  %1.8e\n",density(zval(j,dz,H)),zval(j,dz,H));
  fclose(f1);
  f1 = fopen("density_profile","w");
  for(j=0;j<J;j++) fprintf(f1,"%1.8e\n",density(zval(j,dz,H)));
  fclose(f1);
  f1 = fopen("NN_vs_z","w");
  for(j=0;j<J;j++) fprintf(f1,"%1.8e  %1.8e\n",NN0(zval(j,dz,H)),zval(j,dz,H));
  fclose(f1);
  f1 = fopen("N_profile","w");
  for(j=0;j<J;j++) fprintf(f1,"%1.8e\n",sqrt(NN0(zval(j,dz,H))));
  fclose(f1);
  f1 = fopen("N_vs_z","w");
  for(j=0;j<J;j++) fprintf(f1,"%1.8e %1.8e\n",sqrt(NN0(zval(j,dz,H))),zval(j,dz,H));
  fclose(f1);
  f1 = fopen("Nsq_profile","w");
  for(j=0;j<J;j++) fprintf(f1,"%1.8e %1.8e\n",NN0(zval(j,dz,H)),zval(j,dz,H));
  fclose(f1);
  if(UBACKGROUND){
	f1 = fopen("Uback_vs_z","w");
	for(j=0;j<J;j++) fprintf(f1,"%1.8e  %1.8e\n",Uback(zval(j,dz,H)),zval(j,dz,H));
	fclose(f1);
	f1 = fopen("dUback_vs_z","w");
	for(j=0;j<J;j++) fprintf(f1,"%1.8e  %1.8e\n",dUback(zval(j,dz,H)),zval(j,dz,H));
	fclose(f1);
	f1 = fopen("d2Uback_vs_z","w");
	for(j=0;j<J;j++) fprintf(f1,"%1.8e  %1.8e\n",d2Uback(zval(j,dz,H)),zval(j,dz,H));
	fclose(f1);

	f1 = fopen("Ri_backgrnd_vs_z","w");
        for(j=1;j<J;j++){
 	   Uzsq = dUback(zval(j,dz,H));
  	   Uzsq *= Uzsq;
	   Ri = NN0(zval(j,dz,H));
	   if (fabs(Uzsq) < 1.0e-14) Ri = 1000.0;
	   else Ri = Ri/Uzsq;
	   fprintf(f1,"%1.6e %1.6e\n",Ri,zval(j,dz,H));
	}
	fclose(f1);
  }

#if density3
  f1 = fopen("density_parameters","w");
  fprintf(f1,"z1 = %1.6e, d1 = %1.6e, drho1 = %1.6e\n",z1,d1,drho1);
  fprintf(f1,"z2 = %1.6e, d2 = %1.6e, drho2 = %1.6e\n",z2,d2,drho2);
  fclose(f1);
#endif 
#if density2
  f1 = fopen("density_parameters","w");
  fprintf(f1,"z0 = %1.6e, d0 = %1.6e\n",z0,d0);
  fclose(f1);
#endif 
  f1 = fopen("z","w");
  for(j=0;j<J;j++) fprintf(f1,"%1.8e\n",zval(j,dz,H));
  fclose(f1);

}



/******************************/
/***  clear output files  *****/
/******************************/
void clear_output_files(){
  FILE *f1;

   f1=fopen("solitary_wave_data","w"); fclose(f1);

   f1=fopen("bottom_transport_vals","w"); fclose(f1);
   f1=fopen("cvals","w"); fclose(f1);
   f1=fopen("integrated_halfwidth_vals","w"); fclose(f1);
   f1=fopen ("ext_iso_halfheight_halfwidth_vals", "w"); fclose(f1);
   f1=fopen("sur_halfwidth_vals","w"); fclose(f1);
   f1=fopen("energyvals","w"); fclose(f1);
   f1=fopen("etaextvals","w"); fclose(f1);
   f1=fopen("KEvals","w"); fclose(f1);
   f1=fopen("KEpert_vals","w"); fclose(f1);
   f1=fopen("APEvals","w"); fclose(f1);
   f1=fopen("Rimin_vals","w"); fclose(f1);
   f1=fopen("Rimin_z_vals","w"); fclose(f1);
   f1=fopen("intAPEdens_vals","w"); fclose(f1);
   f1=fopen("massvals","w"); fclose(f1);
   //   f1=fopen("maxUsurvals","w"); fclose(f1);
   f1=fopen("maxUvals","w"); fclose(f1);
   f1=fopen("maxUvals_with_ij","w"); fclose(f1);
   f1=fopen("minUvals","w"); fclose(f1);
   f1=fopen("minUvals_with_ij","w"); fclose(f1);
   f1=fopen("PEvals","w"); fclose(f1);
   f1=fopen("surf_transport_vals","w"); fclose(f1);
   f1=fopen ("Lx_vals", "w"); fclose(f1);
   f1=fopen ("Lx_0p2_vals", "w"); fclose(f1);
   f1=fopen ("Lx_over_lambda_vals", "w"); fclose(f1);

   f1=fopen ("U_at_max_shear_vals", "w"); fclose(f1);
   f1=fopen ("U_at_pycnocline_centre_vals", "w"); fclose(f1);
   f1=fopen ("Usur_WaveCentre_vals", "w"); fclose(f1);
   f1=fopen ("Uz_max_vals", "w"); fclose(f1);
   f1=fopen ("Uz_max_z_vals", "w"); fclose(f1);
   f1=fopen ("lambda_vals", "w"); fclose(f1);
   f1=fopen ("z_at_pycnocline_centre_vals", "w"); fclose(f1);
}
