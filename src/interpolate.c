/* Routines for interpolating on the grid.  Generally, external routines will
   call at_i_j.  This will do quadratic interpolation to any point, where i and
   j are grid co-ordinates (that is, not actual x or z values).
  
   interp_find_poi gives the location of a local max/min

   interp_right_intersect_index computes the location of the intersection of
     the parabola defined by points f0..2 at i..i+2 and the value v
   2009, Ben Langmuir
   */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define FEQ_STRICT(a,b) ((a) - (b) < 1.0e-10)
#define MAX(a,b) ((a) > (b) ? a : b)
#define MAX3(a,b,c) ((MAX((a),(b))) > (MAX((b),(c))) ? (MAX((a),(b))) : (MAX((b),(c))))

double interp_find_poi (double f0, double f1, double f2, int i);
double interp_find_poi_index (double f0, double f1, double f2, int i);
double interp_at_i (double i, double f0, double f1, double f2,  int j);
double interp_right_intersect_index (double v, double f0, double f1, double f2, int i);
static inline double get_A (double f0, double f1, double f2);
static inline double get_B (double A, double f0, double f1, int i);
static inline double get_C (double A, double B, double f0, int i);


/* This routine interpolates the value of the field G (dimensions N,M) at the grid location
   (i, j) which is generally not at a defined grid point.  The routine uses quadratic
   interpolation using the nearest points.  Interpolation may be done in multiple directions
   to get to the point using the nearest points possible.
   */
double at_i_j (double i, double j, int N, int M, double *G)
{
	/* extrapolating outside the grid
	 * Note: we try to always use the nearest good values instead of layering extrapolations
	 */
	
	/* outside corners
	 * worst-case: make 3 extraps in each direction then average them */

	if ( i < 0.0  && j < 0.0 ) {
		double f0x, f1x, f2x, f0z, f1z, f2z;
		double using_x, using_z;
		/* in the i direction */
		f0x = interp_at_i (i, G[0*M + 0], G[1*M + 0], G[2*M + 0], 0);
		f1x = interp_at_i (i, G[0*M + 1], G[1*M + 1], G[2*M + 1], 0);
		f2x = interp_at_i (i, G[0*M + 2], G[1*M + 2], G[2*M + 2], 0);
		using_x = interp_at_i (j, f0x, f1x, f2x, 0);
		
		/* in the j direction */
		f0z = interp_at_i (j, G[0*M + 0], G[0*M + 1], G[0*M + 2], 0);
		f1z = interp_at_i (j, G[1*M + 0], G[1*M + 1], G[1*M + 2], 0);
		f2z = interp_at_i (j, G[2*M + 0], G[2*M + 1], G[2*M + 2], 0);
		using_z = interp_at_i (i, f0z, f1z, f2z, 0);
		
		return (using_x + using_z)/2.0;
	}
	else if ( i < 0.0  && j > (double)M-1.0 ) {
		double f0x, f1x, f2x, f0z, f1z, f2z;
		double using_x, using_z;
		/* in the i direction */
		f0x = interp_at_i (i, G[0*M + M-3], G[1*M + M-3], G[2*M + M-3], 0);
		f1x = interp_at_i (i, G[0*M + M-2], G[1*M + M-2], G[2*M + M-2], 0);
		f2x = interp_at_i (i, G[0*M + M-1], G[1*M + M-1], G[2*M + M-1], 0);
		using_x = interp_at_i (j, f0x, f1x, f2x, M-3);
		
		/* in the j direction */
		f0z = interp_at_i (j, G[0*M + M-3], G[0*M + M-2], G[0*M + M-1], M-3);
		f1z = interp_at_i (j, G[1*M + M-3], G[1*M + M-2], G[1*M + M-1], M-3);
		f2z = interp_at_i (j, G[2*M + M-3], G[2*M + M-2], G[2*M + M-1], M-3);
		using_z = interp_at_i (i, f0z, f1z, f2z, 0);
		
		return (using_x + using_z)/2.0;
	}
	else if ( i > (double)N-1.0  && j < 0.0 ) {
		double f0x, f1x, f2x, f0z, f1z, f2z;
		double using_x, using_z;
		/* in the i direction */
		f0x = interp_at_i (i, G[(N-3)*M + 0], G[(N-2)*M + 0], G[(N-1)*M + 0], N-3);
		f1x = interp_at_i (i, G[(N-3)*M + 1], G[(N-2)*M + 1], G[(N-1)*M + 1], N-3);
		f2x = interp_at_i (i, G[(N-3)*M + 2], G[(N-2)*M + 2], G[(N-1)*M + 2], N-3);
		using_x = interp_at_i (j, f0x, f1x, f2x, 0);
		
		/* in the j direction */
		f0z = interp_at_i (j, G[(N-3)*M + 0], G[(N-3)*M + 1], G[(N-3)*M + 2], 0);
		f1z = interp_at_i (j, G[(N-2)*M + 0], G[(N-2)*M + 1], G[(N-2)*M + 2], 0);
		f2z = interp_at_i (j, G[(N-1)*M + 0], G[(N-1)*M + 1], G[(N-1)*M + 2], 0);
		using_z = interp_at_i (i, f0z, f1z, f2z, N-3);

		return (using_x + using_z)/2.0;
	}
	else if ( i > (double)N-1.0  && j > (double)M-1.0 ) {
		double f0x, f1x, f2x, f0z, f1z, f2z;
		double using_x, using_z;
		/* in the i direction */
		f0x = interp_at_i (i, G[(N-3)*M + M-3], G[(N-2)*M + M-3], G[(N-1)*M + M-3], N-3);
		f1x = interp_at_i (i, G[(N-3)*M + M-2], G[(N-2)*M + M-2], G[(N-1)*M + M-2], N-3);
		f2x = interp_at_i (i, G[(N-3)*M + M-1], G[(N-2)*M + M-1], G[(N-1)*M + M-1], N-3);
		using_x = interp_at_i (j, f0x, f1x, f2x, M-3);
		
		/* in the j direction */
		f0z = interp_at_i (j, G[(N-3)*M + M-3], G[(N-3)*M + M-2], G[(N-3)*M + M-1], M-3);
		f1z = interp_at_i (j, G[(N-2)*M + M-3], G[(N-2)*M + M-2], G[(N-2)*M + M-1], M-3);
		f2z = interp_at_i (j, G[(N-1)*M + M-3], G[(N-1)*M + M-2], G[(N-1)*M + M-1], M-3);
		using_z = interp_at_i (i, f0z, f1z, f2z, N-3);
		
		return (using_x + using_z)/2.0;
	}

	/* j inside, i outside */
	else if ( i < 0.0 ) {
		int jj = ((int)j == 0) ? (int)j+1: (((int)j == M) ? (int)j-1: (int)j);
		double f0, f1, f2;
		/* in the j direction */
		f0 = interp_at_i (j, G[0*M + jj-1], G[0*M + jj], G[0*M + jj+1], jj-1);
		f1 = interp_at_i (j, G[1*M + jj-1], G[1*M + jj], G[1*M + jj+1], jj-1);
		f2 = interp_at_i (j, G[2*M + jj-1], G[2*M + jj], G[2*M + jj+1], jj-1);
	
		return interp_at_i (i, f0, f1, f2, 0);
	}
	else if ( i > (double)N-1.0 ) {
		int jj = ((int)j == 0) ? (int)j+1: (((int)j == M) ? (int)j-1: (int)j);
		double f0, f1, f2;
		/* in the j direction */
		f0 = interp_at_i (j, G[(N-3)*M + jj-1], G[(N-3)*M + jj], G[(N-3)*M + jj+1], jj-1);
		f1 = interp_at_i (j, G[(N-2)*M + jj-1], G[(N-2)*M + jj], G[(N-2)*M + jj+1], jj-1);
		f2 = interp_at_i (j, G[(N-1)*M + jj-1], G[(N-1)*M + jj], G[(N-1)*M + jj+1], jj-1);
	
		return interp_at_i (i, f0, f1, f2, N-3);
	}
	/* i inside, j outside */
	else if ( j < 0.0 ) {
		int ii = ((int)i == 0) ? (int)i+1: (((int)i == N) ? (int)i-1: (int)i);
		double f0, f1, f2;
		/* in the i direction */
		f0 = interp_at_i (i, G[(ii-1)*M + 0], G[ii*M + 0], G[(ii+1)*M + 0], ii-1);
		f1 = interp_at_i (i, G[(ii-1)*M + 1], G[ii*M + 1], G[(ii+1)*M + 1], ii-1);
		f2 = interp_at_i (i, G[(ii-1)*M + 2], G[ii*M + 2], G[(ii+1)*M + 2], ii-1);
	
		return interp_at_i (j, f0, f1, f2, 0);
	}
	else if ( j > (double)M-1.0 ) {
		int ii = ((int)i == 0) ? (int)i+1: (((int)i == N) ? (int)i-1: (int)i);
		double f0, f1, f2;
		/* in the i direction */
		f0 = interp_at_i (i, G[(ii-1)*M + M-3], G[ii*M + M-3], G[(ii+1)*M + M-3], ii-1);
		f1 = interp_at_i (i, G[(ii-1)*M + M-2], G[ii*M + M-2], G[(ii+1)*M + M-2], ii-1);
		f2 = interp_at_i (i, G[(ii-1)*M + M-1], G[ii*M + M-1], G[(ii+1)*M + M-1], ii-1);
	
		return interp_at_i (j, f0, f1, f2, M-3);
	}

	/* interpolate within grid
	 * Take the interpolation in the i and j directions and average it
	 */
	else {
		int ii = ((int)i == 0) ? (int)i+1: (((int)i == N) ? (int)i-1: (int)i);
		int jj = ((int)j == 0) ? (int)j+1: (((int)j == M) ? (int)j-1: (int)j);
		double using_x, using_z;
		printf("(i,j) = (%1.5e,%1.5e), integer part: (%d,%d)\n",i,j,(int)i,(int)j);
		printf("(ii,jj) = (%d,%d)\n",ii,jj);
		using_x = interp_at_i (i, G[(ii-1)*M + jj], G[ii*M + jj], G[(ii+1)*M + jj], ii-1);
		using_z = interp_at_i (j, G[ii*M + jj-1], G[ii*M + jj], G[ii*M + jj+1], jj-1);
		
		return (using_x + using_z)/2.0;
	}
}


/* Find a local point of interest where dy/dx = 0
 * This returns the value at the point.
 * 
 * Following based on interpolating as f = A*i*i + B*i + C
 * with 
 *      f(i0) = f0,  f(i0+1) = f1, f(i0+2) = f2
 * which gives
 *
 *  A*i0*i0 + B*i0 + C = f0
 *  A*i0*i0 + B*i0 + C + A*(2*i0+1) + B = f1   => A*(2*i0+1) + B = f1-f0
 *  A*i0*i0 + B*i0 + C + A*(4*i0+4) + 2*B = f2 => A*(4*i0+4) + 2*B = f2-f0
 * 
 *  Eliminate B: => 2*A = f2-f0-2*(f1-f0) = f2+f0-2*f1
 *  Then B = f1-f0-A*(2*i0+1) and C = f0 - A*i0*i0 - B*i0
 */
double interp_find_poi (double f0, double f1, double f2, int i0)
{
	double A = get_A (f0, f1, f2);
	double B = get_B (A, f0, f1, i0);
	double poi_i = -B/(2*A);
	
	return interp_at_i (poi_i, f0, f1, f2, i0);
}

/* Find a local point of interest where dy/dx = 0
 * This returns the index of the point.
 */
double interp_find_poi_index (double f0, double f1, double f2, int i)
{
	double A = get_A (f0, f1, f2);
	double B = get_B (A, f0, f1, i);
	
	return -B/(2*A);
}
/* Find the intersection of a parabola with a horizontal line and return the x value. */
double interp_right_intersect_index (double v, double f0, double f1, double f2, int i)
{
	double A = get_A (f0, f1, f2);
	double B = get_B (A, f0, f1, i);
	double C = get_C (A, B, f0, i);

	return (-B + sqrt(B*B - 4.0 * A * (C - v)))/(2.0 * A);
}

/* Find the intersection of a parabola with a horizontal line and return the x value. */
double interp_left_intersect_index (double v, double f0, double f1, double f2, int i)
{
	double A = get_A (f0, f1, f2);
	double B = get_B (A, f0, f1, i);
	double C = get_C (A, B, f0, i);

	return (-B - sqrt(B*B - 4.0 * A * (C - v)))/(2.0 * A);
}

/* Helper routine for at_i_j, takes three values at adjacent grid points
   and interpolates to the point i.  i.e this is in one dimension.
   */
double interp_at_i (double i, double f0, double f1, double f2, int j)
{
	double A = get_A (f0, f1, f2);
	double B = get_B (A, f0, f1, j);
	double C = get_C (A, B, f0, j);

	return (A * i*i + B*i + C);
}

/* A = (f0 + f2)/2 - f1 */
static inline double get_A(double f0, double f1, double f2)
{
	return ((f0 + f2)/2 -  f1);
}

/* B = f1 - f0 - (2i+1)A */
static inline double get_B(double A, double f0, double f1, int i)
{
	return (f1 - f0 - A * (2*((double) i) + 1));
}

/* C = f0 - Ai^2 - Bi */
static inline double get_C(double A, double B, double f0, int i)
{
	return (f0 - A * ((double) i*i) - B * ((double) i));
}
