/*******************************************************************/
/*   This subroutine calculates the eta values                     */
/*******************************************************************/ 
#include <stdio.h>
#include <math.h> 
#include <stdlib.h> 
#include "sin_transforms.h" 
#include "header.h" 
#include "allocation.h"

#define N 20

extern int max_iterate; 
extern int min_iterate; 
extern double relax;

#define ACCELERATE 0 
#define accelerate_num 6 
#define accelerate_int 1 
#define lambda_factor 1.5 


void get_WNL_APE();
void get_initial_eta(),check_poisson(); 
extern inline double xval(int i, double dx);
extern inline double zval(int j, double dz, double Lz);
extern double density(),d_density(),Uback(),dUback(); 
extern double NN0(), *alloc_vector();
extern void get_eigfns1(),free_vector(); 
extern double H,L,g,f; 
double clw; 
//static double eta0[I][J]; 
static double **eta0=NULL;
//static double rhs[I][J], den_fine_z[J]; 
//static double d_den_value[I][J],den_value[I][J]; 
static double **rhs=NULL, *den_fine_z=NULL; 
static double **d_den_value=NULL, **den_value=NULL; 

extern int UBACKGROUND;
extern int DETA_OUT;
extern int ETA_VERT_OUT;
extern int ETA_OUT;
extern int TEST_OUT;
//extern double eta[I][J]; 
extern double **eta;

extern int I, J;
 
#if ACCELERATE 
double diff[I][J],prevdiff[I][J]; 
#endif 

void get_eta_allocation(void)
{
  eta0 = alloc_matrix(I,J);
  rhs = alloc_matrix(I,J);
  d_den_value = alloc_matrix(I,J);
  den_value = alloc_matrix(I,J);
  den_fine_z = alloc_vector(J);
}

 
void get_eta(double A_unscaled, double domainwidth, double *coef, char eta_source[], double epsilon)
{ 
 
double Asave, lambdak, ck, t1; 
double F, S1, S2, ff, max_diff, deta, dxdz, dxdzoH, temp, alpha, diag, dz2, dz, dx; 
double dxdxinv, dzdzinv, rjac; 
double mineta, maxeta; 
int i , j, flag, iterate_num, n; 
char file[40]; //filename for use by sprintf, must be longer than longest filename
FILE *f1; 
//static double eta_dx[I][J], eta_dz[I][J]; 
 double **eta_dx, **eta_dz;

 eta_dx = alloc_matrix(I,J);
 eta_dz = alloc_matrix(I,J);

  printf ("\nComputing solitary wave with APE = %1.8e\n\n", A_unscaled); 
  printf ("domainwidth = %1.8e\n", domainwidth); 
  printf ("Convergence criteria: (1) max change in eta <= %1.6e*H = %1.6e\n",epsilon,epsilon*H); 
  printf ("                      (2) max number of iterations = %d\n\n", max_iterate); 
  printf ("domainwidth = %1.8e\n", domainwidth); 
  f1 = fopen ("solitary_wave_data", "a"); 
  fprintf (f1, "APE = %1.8e\n", A_unscaled); 
  fprintf (f1, "     I = %d,   J = %d\n", I, J); 
  fprintf (f1, "     domain length = %1.5e,  water depth = %1.5e\n", L, H); 
  fprintf (f1, "     Convergence criteria: (1) max change in eta <= %1.6e*H = %1.6e\n", epsilon, epsilon*H); 
  fprintf (f1, "                           (2) max number of iterations = %d\n\n", max_iterate); 
  fclose (f1); 
 
  epsilon *= H; 
  Asave = A_unscaled; 
 
  dx = domainwidth/I; 
  dz = H/J; 
  dxdz = dx*dz; 
  dxdzoH = 1.0/H*dx*dz; 
  printf ("dx = %1.6e,  dz = %1.6e\n", dx, dz); 

  if(!strcmp(eta_source,"WNL")) { 
     get_initial_eta (A_unscaled, 0.5*domainwidth, dx, dz); 
     *coef = g*H / (clw*clw);   
     printf ("From WNL get clw = %1.8e \n", clw); 
   }
  else{
     if(!strcmp(eta_source,"prev")) {
     printf("prev branch\n");
//     for(i=0;i<Isol;i++) for(j=0;j<Jsol;j++) eta[i][j] *= 1.0;
     }
     else{
        f1 = fopen(eta_source, "r");
        if(f1 == (FILE*) NULL) {
           fprintf (stderr, "/*** eta file '%s' does not exist ***/\n", eta_source);
           fprintf (stderr, "/***     EXITING.....             ***/\n\n");
           exit (1);
       }
        else{
           for(i=0; i<I; i++) fread (eta[i], sizeof(double), J, f1);
           fclose (f1);
           printf("read initial eta in from file\n");
           *coef = 2.80615e+03;
        }
     }
   }
 
  for(j=0; j<J; j++) den_fine_z[j] = density(zval(j,dz,H)); 
 
  #if ACCELERATE 
    for (i=1; i<I; i++) for(j=1; j<J; j++) diff[i][j] = 0.0; 
  #endif 
 
  maxeta = -99999.9; 
  mineta = 99999.9; 
  for(i=1; i<I; i++) { 
     for(j=1; j<J; j++) { 
        if(eta[i][j] > maxeta) maxeta = eta[i][j]; 
        if(eta[i][j] < mineta) mineta = eta[i][j]; 
     }
   }
  printf("For initial guess: maxeta = %1.6e,   mineta = %1.6e\n", maxeta, mineta); 
 
  flag = 1; 
  iterate_num = 0; 

  #if 0   /** sometimes useful to look at these if solution isn't converging **/
    sprintf(file,"eta_vertprof%d",iterate_num); 
    f1 = fopen(file,"w"); 
    for(j=0;j<=J;j++) fprintf(f1,"%1.8e\n",eta[I/2][j]); 
    fclose(f1); 
     
    sprintf(file,"deta_dz_surf%d",iterate_num); 
    f1 = fopen(file,"w"); 
    for(i=1;i<=I;i++) fprintf(f1,"%1.8e\n",(eta[i][J]-eta[i][J-1])/dz); 
    fclose(f1); 
 
    sprintf(file,"eta_horprof%d_%d",J/3,iterate_num); 
    f1 = fopen(file,"w"); 
    for(i=1;i<=I;i++) fprintf(f1,"%1.8e\n",eta[i][J/3]); 
    fclose(f1); 
 
    sprintf(file,"eta_horprof%d_%d",2*J/3,iterate_num); 
    f1 = fopen(file,"w"); 
    for(i=1;i<=I;i++) fprintf(f1,"%1.8e\n",eta[i][2*J/3]); 
    fclose(f1); 
  #endif 

while(flag){ 
   iterate_num++; 
   lambdak = *coef; 
   ck = sqrt(g*H/lambdak); 
   //   printf ("      g = %1.5e, H = %1.5e, lambdak = %1.6e, ck = %1.6e\n", g, H, lambdak, ck);
   /* Poisson set up starts */

   for(i=0; i<I; i++) { 
      for(j=0; j<J; j++) { 

	 eta0[i][j] = eta[i][j]; 
	 d_den_value[i][j] = d_density(zval(j,dz,H) - eta0[i][j]); 
	 den_value[i][j] = density(zval(j,dz,H) - eta0[i][j]); 
	 if(UBACKGROUND) {
	    t1 = Uback(zval(j,dz,H) - eta0[i][j])/ck - 1.0; 
	    rhs[i][j] = eta0[i][j]*d_den_value[i][j] / (H*t1*t1); 
	 }
	 else rhs[i][j] = eta0[i][j]/H * d_den_value[i][j]; 
      }
   }
   //   printf("here\n");
   if(UBACKGROUND) {
	/*  The derivatives of eta are defined using 2nd order central 
		differences, with interpolation out to grid edges.  If the 
		grid is not equally spaced this will have to be changed  */

	/* Call fftw to get sine based derivatives for eta */
     //      sin_x_deriv ((double*) eta0, (double*) eta_dx, I, J, L);
     //      sin_z_deriv ((double*) eta0, (double*) eta_dz, I, J, H);
      sin_x_deriv (eta0[0], (double*) eta_dx[0], I, J, L);
      sin_z_deriv ((double*) eta0[0], (double*) eta_dz[0], I, J, H);
	/*  The second piece of rhs is now defined  */ 
      //   printf("here 4\n");
      for(i=0; i<I; i++) { 
	 for(j=0; j<J; j++) { 
	    t1 = dUback(zval(j,dz,H) - eta0[i][j]) / (Uback(zval(j,dz,H) - eta0[i][j]) - ck); 
	    t1 *= (1.0-eta_dx[i][j] * eta_dx[i][j] - (1.0-eta_dz[i][j]) * (1.0-eta_dz[i][j])); 
	    rhs[i][j] -= t1 / lambdak; 
	 }
      }
   }
/*  The Poisson Problem is now set up to be solved  */ 
//    printf("here 2\n");
    //   sin_pois_solve ((double*) eta, (double*) rhs, I, J, L, H);
   sin_pois_solve ((double*) eta[0], (double*) rhs[0], I, J, L, H);
   //    printf("here 5\n");
   check_poisson (dx, dz);  
   //    printf("here 6\n");
   F = S1 = S2 = 0.0; 
   for(i=0; i<I; i++) { 
      for(j=0; j<J; j++) { 
	//	 if(iterate_num <= 25 && iterate_num > 1)
	//              eta[i][j] = 0.8*eta0[i][j]/(*coef) + 0.2*eta[i][j];
       	//	 eta[i][j] = 0.6*eta0[i][j]/(*coef) + 0.4*eta[i][j];
	//	 eta[i][j] = 0.4*eta0[i][j]/(*coef) + 0.6*eta[i][j];
 	  eta[i][j] = (1-relax)*eta0[i][j]/(*coef) + relax*eta[i][j];

/* this slows how fast eta changes */
/* N is the number of points for the integral in little f (2.44) */
/* could try Simpson's Rule */
	 deta = eta0[i][j] / N; 
 	 ff = (den_fine_z[j] + den_value[i][j]); 
	 double my_z_val = zval(j,dz,H);
	 ff += 4*density(my_z_val - deta); 
	 for(n=2; n<=N-2; n+=2) {
	    ff += 2*density(my_z_val - n*deta); 
	    ff += 4*density(my_z_val - (n+1)*deta);
	 }
	 ff *= -deta/3; 
	 ff += eta0[i][j]*den_value[i][j]; 
	 F += ff; 
	 temp = d_den_value[i][j]*eta0[i][j]; 
	 S1 -= temp*eta[i][j]; 
	 S2 -= temp*eta0[i][j]; 
      }
   }
   //   printf("here 3\n");
   F *= g*dxdz; 
   S1 *= g*dxdz; 
   S2 *= g*dxdz; 
   *coef = (A_unscaled-F+S2)/S1; 
 
   if(*coef < 0.0) { 
      fprintf (stderr, "coeff = %1.6e\n", *coef); 
      fprintf (stderr, "coef has wrong sign ==> nonconvergence of iteration procedure.\n"); 
      fprintf (stderr, "  ****** exiting in get_eta() ******\n\n"); 
      fprintf (stderr, "   A = %1.8e, F = %1.8e\n", A_unscaled, F); 
      fprintf (stderr, "   S1 = %1.6e, S2 = %1.6e, S2/S1 = %1.6e\n", S1, S2, S2/S1);  
      exit (1); 
   }
   max_diff = 0.0; 
#if ACCELERATE 
   for(i=1; i<I; i++) { 
      for(j=1; j<J; j++) { 
         eta[i][j] = (*coef) * eta[i][j]; 
	 prevdiff[i][j] = diff[i][j]; 
	 diff[i][j] = eta[i][j] - eta0[i][j]; 
	 if(fabs (diff[i][j]) > max_diff) max_diff = fabs(diff[i][j]); 
      }
   }
   if(iterate_num >= accelerate_num && iterate_num%accelerate_int == 0) { 
      for(i=1; i<I; i++) 
	 for (j=1; j<J; j++) eta[i][j] += 2*diff[i][j] - prevdiff[i][j]; 
   }
#else 
   for(i=0; i<I; i++) { 
      for(j=0; j<J; j++) { 
	 eta[i][j] = (*coef) * eta[i][j]; 
	 if (fabs (eta[i][j]-eta0[i][j]) > max_diff)  
	 max_diff = fabs(eta[i][j]-eta0[i][j]); 
      }
   }
#endif 
 
   if(ETA_VERT_OUT) {
      sprintf (file, "eta_vertprof%d", iterate_num); 
      f1 = fopen (file, "w"); 
      for(j=0; j<J; j++) fprintf (f1, "%1.8e\n", eta[I/2][j]); 
      fclose (f1); 
   }
 
   if(DETA_OUT) {
      sprintf (file, "deta_dz_surf%d", iterate_num); 
      f1 = fopen (file, "w"); 
      for(i=1; i<I; i++) fprintf (f1, "%1.8e\n", (eta[i][J-1]-eta[i][J-2])/dz); 
      fclose (f1); 
   }
 
   maxeta = -99999.9; 
   mineta = 99999.9; 
   for(i=0; i<I; i++) { 
      for(j=0; j<J; j++) { 
	 if(eta[i][j] > maxeta) maxeta = eta[i][j]; 
	 if(eta[i][j] < mineta) mineta = eta[i][j]; 
      }
   }
   sprintf (file, "max_diff_history_A%g", A_unscaled); 
   f1 = fopen (file, "a");
   fprintf (f1, "%d %1.6e\n", iterate_num, max_diff); 
   fclose (f1);
   printf ("\niterate_num = %d:  max_diff = %1.8e\n", iterate_num, max_diff); 
   printf ("      maxeta = %1.8e,  mineta = %1.8e\n", maxeta, mineta); 
   printf ("      coef = %1.6e,   ck = %1.6e\n", lambdak, ck); 
   printf ("      A  = %1.6e,  F = %1.6e\n", A_unscaled, F); 
   printf ("      S1 = %1.6e, S2 = %1.6e, S2/S1 = %1.6e\n", S1, S2, S2/S1);  

   if(max_diff <= epsilon && A_unscaled >= 0.99*Asave && iterate_num > min_iterate) flag = 0; 
   if(iterate_num >= max_iterate) { 
      flag = 0; 
      printf("***** Reached maximum number of iterations *****\n"); 
      if(max_diff > 10*epsilon) {
      printf("***** max_diff > 10*epsilon --> exiting program ... *****\n"); 
      fprintf(stderr,"***** max_diff > 10*epsilon --> exiting program ... *****\n"); 
      exit (1);
      }
   }
 }

if(ETA_OUT) { 
   sprintf (file, "eta_A%g", A_unscaled); 
   f1 = fopen (file, "w"); 
   for(i=0; i<I; i++) fwrite (eta[i], sizeof(double), J, f1); 
   fclose (f1);
}

sprintf (file, "etamidverprof_A%g", A_unscaled); 
f1 = fopen (file, "w"); 
if(I%2 == 0) { 
   for (j=0; j<J; j++) fprintf (f1, "%1.6e\n", eta[I/2][j]); 
 }
else{
   for(j=0; j<J; j++) 
   fprintf (f1, "%1.6e\n", 9.0/16.0 * (eta[I/2][j] + eta[I/2+1][j]) 
                                - (eta[I/2-1][j] + eta[I/2+2][j])/16.0); 
 }
fclose (f1); 
 
sprintf (file, "etamidhorprof_A%g", A_unscaled); 
f1 = fopen (file, "w"); 
if(J%2 == 0) { 
   for (i=0; i<I; i++) fprintf (f1, "%1.6e\n", eta[i][J/2]); 
 }
else{
   for(i=0; i<I; i++) fprintf (f1, "%1.6e\n", 9.0/16.0*(eta[i][J/2] + eta[i][J/2+1]) 
                                - (eta[i][J/2-1] + eta[i][J/2+2])/16.0); 
} 
fclose (f1); 
 
f1 = fopen ("solitary_wave_data", "a"); 
fprintf (f1, "     number of iterations taken: %d\n", iterate_num); 
if(iterate_num >= max_iterate) 
   fprintf (f1, "        ====> WARNING:   number of iterations taken = maximum allowed\n"); 
   fclose (f1); 
} 
 
 
/*******************************************************/ 
/***  Routine to provide initial guess for eta       ***/ 
/*******************************************************/ 
 
void get_initial_eta (double A, double half_domainwidth, double dx, double dz) {

  extern int OPPOSITE_POLARITY;
  extern double *phi, *phi_dz, *zz; 
  extern double r01, r10, r20;
  extern int vstep; 
  extern double z1, z2;

  int i, j, k, sign, region; 
  double B[I], lambda, mag_amp, amp, *phivals, se; 
  double KdV_APE, KdV_APE_lin, t1, t2, rescale, Int; 
  double beta, alpha, alpha1; 
  double dz_alpha_comp;

  FILE *f1, *f2, *test; 

  printf ("H = %1.6e\n", H); 
  phivals = alloc_vector (J); 

  get_eigfns1(); 

  f1 = fopen ("solitary_wave_data", "a"); 
  fprintf (f1, "     linear long wave speed: clw = %1.6e\n\n", clw);
  fclose (f1);

  beta   = -r01; 
  alpha  = -2*r10*clw;
  alpha1 = -3*r20*clw*clw;

  if(alpha1>=0){
	if (alpha > 0) region = 1;
	else region = 2;
  } else {
	if (alpha > 0) region = 4;
	else region = 3;
  }

  printf         ("*************************************************************\n");
  printf         ("****        alpha = %+1.6e                        ****\n", alpha);
  printf         ("****         beta = %+1.6e                        ****\n", beta);
  printf         ("****       alpha1 = %+1.6e                        ****\n", alpha1);
  printf         ("****                                                     ****\n");
  if (region == 1) {
	printf     ("****    Region I:  elevation: no limit                   ****\n");
	if (alpha1 !=0)
		printf ("****               depression: amplitude < %1.5e\n       ****\n", -2.0*alpha/alpha1);
  }
  if (region == 2) {
	printf     ("****   Region II:  depression: no limit                  ****\n");
	if (alpha1 !=0)
		printf ("****               elevation:  amplitude < %1.5e   ****\n",-2.0*alpha/alpha1);
  }
  if(region==3){
      printf("****  Region III:  waves of depression                   ****\n");
      if(alpha1 !=0) printf("****               limiting amplitude = %1.5e     ****\n",-alpha/alpha1);
  }
  if(region==4){
      printf("****  Region IV:  waves of elevation                     ****\n");
      if(alpha1 !=0) printf("****              limiting amplitude = %1.5e       ****\n",-alpha/alpha1);
  }
  printf("****                                                     ****\n");
  printf("*************************************************************\n");
  if (TEST_OUT) {
	 test = fopen ("WNL_coeff_test", "w");
	 fprintf (test, "c = %1.5e r10 = %1.5e r01 = %1.5e r20 = %1.5e\n", clw, r10, r01, r20);
	 fclose (test);
  }
  f1 = fopen("WNL_coefficients","w");
  fprintf(f1,"\n For equation in the form\n  B_t = -c*B_x + 2*r10*B*B_x + r01*B_xxx + 3*r20*c*c*B*B*B_x\n\n"); 
  fprintf(f1,"  c = %1.5e,  r10 = %1.5e\n",clw,r10);
  fprintf(f1,"r01 = %1.5e   r20 = %1.5e\n",r01,r20); 

  fprintf(f1,"*************************************************************************\n");
  fprintf(f1,"**** For                                                             ****\n");
  fprintf(f1,"****  B_t + c*B_x + alpha*B*B_x  + beta*B_xxx + alpha1*B*B*B_x = 0   ****\n");
  fprintf(f1,"****                                                                 ****\n");
  fprintf(f1,"****        alpha = %+1.6e                                    ****\n",alpha);
  fprintf(f1,"****         beta = %+1.6e                                    ****\n",beta);
  fprintf(f1,"****       alpha1 = %+1.6e                                    ****\n",alpha1);
  fprintf(f1,"****                                                                 ****\n");
  if(region==1){
      fprintf(f1,"****    Region I:  elevation: no limit                       ****\n");
      if(alpha1 !=0) fprintf(f1,"****               depression: amplitude < %1.5e\n         ****\n",-2.0*alpha/alpha1);
  }
  if(region==2){
      fprintf(f1,"****   Region II:  depression: no limit                               ****\n");
      if(alpha1 !=0) fprintf(f1,"****               elevation:  amplitude < %1.5e               ****\n",-2.0*alpha/alpha1);
  }
  if(region==3){
      fprintf(f1,"****  Region III:  waves of depression                               ****\n");
      if(alpha1 !=0) fprintf(f1,"****               limiting amplitude = %1.5e                 ****\n",-alpha/alpha1);
  }
  if(region==4){
      fprintf(f1,"****  Region IV:  waves of elevation                                 ****\n");
      if(alpha1 !=0) fprintf(f1,"****              limiting amplitude = %1.5e                   ****\n",-alpha/alpha1);
  }
  fprintf(f1,"****                                                                 ****\n");
  fprintf(f1,"*************************************************************************\n");
  fclose(f1);




/***************************************************************/ 
/***  interpolate linear eigenfunction found in get_eigfns1  ***/ 
/***  onto grid used for solitary wave calculation           ***/ 
/***************************************************************/ 
 
  for(j=0;j<J;j++){ 
    if(j==0) phivals[j] = 0.0; 
    else if(j==J-1) phivals[j] = 0.0; 
    else { 
      i=1; 
      while(zz[i] <= zval(j,dz,H))  ++i; 
      t1 = (zval(j,dz,H)-zz[i-1])/(zz[i]-zz[i-1]); 
      phivals[j] = phi[i-1] + (phi[i]-phi[i-1])*t1; 
    } 
  } 
 
/***********************************************************************/ 
/***   Stores APE vs amp predicted by KdV theory (linearized and     ***/ 
/***   nonlinear using APE_lin = int{eta*eta*N^2(z)} and             ***/  
/***   APE = int{eta*eta*N^2(z-eta)*(1-eta_z)}                       ***/ 
/***********************************************************************/ 
 
  f1 = fopen("KdV_APE_lin_vs_amp","w"); 
  f2 = fopen("KdV_APE_vs_amp","w"); 
  if(r10<0) sign = 1; 
  else sign = -1; 
 
  printf("Finding WNL_APE for different wave amplitudes between 0 and H:\n"); 
  mag_amp = 0.05*H; 
  while(mag_amp <= 0.95*H){ 
     amp = sign*mag_amp; 
     lambda = sqrt(6.0*r01/(amp*clw*r10)); 
     printf("amp = %1.5e = %1.5eH, lambda = %1.6e\n",amp,amp/H,lambda); 
 
    for(i=0;i<I;i++){ 
       se = 1.0/(cosh((xval(i,dx) - half_domainwidth)/lambda)); 
       if(se < 1E-20) se = 0.0; 
       B[i] = amp*se*se; 
     } 
    for(i=0;i<I;i++) for(j=0;j<J;j++) eta[i][j] = B[i]*phivals[j]; 
 
    get_WNL_APE(dx,dz,&KdV_APE,&KdV_APE_lin); 
    fprintf(f1,"%1.6e %1.6e\n",amp,KdV_APE_lin); 
    fprintf(f2,"%1.6e %1.6e\n",amp,KdV_APE); 
    mag_amp += 0.025*H; 
  }
  fclose(f1); 
  fclose(f2); 
 
/*************************************************************************/ 
/***  Use KdV solitary wave to predict wave amplitude required to give ***/ 
/***  solitary wave with APE equal to g*H*A                            ***/ 
/*************************************************************************/ 
 
  Int = 0.0; 
  for(j=0;j<J;j++) Int += NN0(zval(j,dz,H))*phivals[j]*phivals[j]; 
  Int *= dz; 
 
  t1 = A/(2.0/3.0*Int); 
  t2 = clw*r10/(6.0*r01)*t1*t1; 
  if(t2>0) amp = pow(t2,1.0/3.0); 
  else amp = -pow(-t2,1.0/3.0); 
 
  lambda = sqrt(6.0*r01/(amp*clw*r10)); 
  printf("\n Iterating over wave amplitude so wave APE = A\n"); 
  printf(" Initial wave from simplified KdV expression for APE\n"); 
  printf("     Initial lambda = %1.6e\n",lambda); 
  printf("     Initial amplitude = %1.6e\n",amp); 
  printf("     Initial KdV propagation speed = %1.6e\n",clw - 2.0/3.0*r10*amp*clw); 
 
  f1 = fopen("Initial_wave_vs_x","w"); 
  for(i=0;i<I;i++){ 
     se = 1.0/(cosh((xval(i,dx) - half_domainwidth)/lambda)); 
     if(se < 1E-20) se = 0.0; 
     B[i] = amp*se*se; 
   } 
  fclose(f1); 
 
  for(i=0;i<I;i++) for(j=0;j<J;j++) eta[i][j] = B[i]*phivals[j]; 
 
  get_WNL_APE(dx,dz,&KdV_APE,&KdV_APE_lin); 
  printf(" For initial wave: amp = %1.6e, KdV_APE_lin = %1.6e,  KdV_APE = %1.6e\n", 
             amp,KdV_APE_lin,KdV_APE); 
 
 
/****  loop to adjust wave amplitude so KdV_APE = A*g*H   ****/ 
#if 1
  for(k=1;k<=10;k++){ 
     rescale = sqrt(A/KdV_APE); 
     printf("\n Rescaling:  multiplying wave amplitude by %1.6e\n",rescale); 
     amp *= rescale; 
     lambda = sqrt(6.0*r01/(amp*clw*r10)); 
     printf("new lambda = %1.6e\n",lambda); 
 
     for(i=0;i<I;i++){ 
        se = 1.0/(cosh((xval(i,dx)-half_domainwidth)/lambda)); 
        if(se < 1E-20) se = 0.0; 
        if(OPPOSITE_POLARITY==1){
            B[i] = -amp*se*se;    // for wave of opposite polarity than KdV predicts
        }
        else{
            B[i] = amp*se*se; 
        }

      } 
     for(i=0;i<I;i++)  for(j=0;j<J;j++)  eta[i][j] = B[i]*phivals[j]; 
 
     get_WNL_APE(dx,dz,&KdV_APE,&KdV_APE_lin); 
     printf("new values: amp = %1.6e,  KdV_APE_lin = %1.6e,  KdV_APE = %1.6e\n", 
                       amp,KdV_APE_lin,KdV_APE); 
     printf("\n"); 
  } 

#else
  lambda = 200.0;
  amp = 7.0;
  for(i=0;i<I;i++){
     se = 1.0/(cosh((xval(i,dx)-half_domainwidth)/lambda)); 
     if(se < 1E-20) se = 0.0; 
     B[i] = amp*se*se; 
  }
  for(i=0;i<I;i++)  for(j=0;j<J;j++)  eta[i][j] = B[i]*phivals[j];
#endif



#if 0
  for(i=1;i<I;i++){
      for(j=1;j<J/2;j++){
	 se = eta[i][j]; 
         eta[i][j] = -eta[i][J-j];
         eta[i][J-j] = -se;
      }
  eta[i][J/2] = -eta[i][J/2];
  }
#endif

  free_vector(phivals); 
} 
 
/************************************************************/ 
/***  This routine calulates the APE according to weakly  ***/ 
/***  nonlinear theory. Given the grid spacing dx and dz  ***/ 
/***  and the displacement field eta(x,z) two expressions ***/ 
/***  for the APE are calculated and returned.            ***/  
/************************************************************/ 
 
void get_WNL_APE(double dx,double dz,double *KdV_APE,double *KdV_APE_lin) {
int i,j; 
double t1,t2; 
 
  *KdV_APE_lin = *KdV_APE = 0.0; 
  for(i=1;i<I;i++){ 
    for(j=1;j<J;j++){ 
      t1 = 0.25*(eta[i][j] + eta[i-1][j] + eta[i][j-1] + eta[i-1][j-1]);  
      t2 = 0.5*(eta[i][j] + eta[i-1][j] - eta[i][j-1] - eta[i-1][j-1])/dz; 
      *KdV_APE_lin += t1*t1*NN0((j-0.5)*dz-H); 
      *KdV_APE += t1*t1*NN0((j-0.5)*dz-H-t1)*(1-t2); 
    } 
  } 
  *KdV_APE_lin *= 0.5*dx*dz; 
  *KdV_APE *= 0.5*dx*dz; 
} 
 
 
/******************************************************************************/ 
/******************************************************************************/ 
/******************************************************************************/ 
 
void check_poisson(double dx,double dz) {
  int i,j; 
  double temp,max_err,min_err; 
 
  max_err = -999999.9; 
  min_err = 999999.9; 
//    printf("here 7\n");
  for(i=1;i<I-1;i++) {
  //  printf("i=%d\n",i);
    for(j=1;j<J-1;j++){ 
    //    printf("i=%d,  j=%d\n",i,j);
       temp = (eta[i+1][j]-2*eta[i][j]+eta[i-1][j])/(dx*dx) 
              + (eta[i][j+1]-2*eta[i][j]+eta[i][j-1])/(dz*dz) 
                - rhs[i][j]; 
       if(temp > max_err) max_err = temp; 
       if(temp < min_err) min_err = temp; 
    } 
   }
  printf("        Maximum error in poisson solution is %1.6e\n",max_err); 
  printf("        Minimum error in poisson solution is %1.6e\n",min_err); 
} 
