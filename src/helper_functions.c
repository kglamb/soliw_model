/* helper_functions.c - some helper functions to reduce reproduced boilerplate throughout IGW */
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
//#include "declarations.h"
#include "helper_functions.h"

#define CFSIZE 100

// clearfile - A wrapper function for creating the filename and
//             clearing the file's contents. Usage follows printf:
// clearfile("KEflux");
// clearfile("KEflux%d",k);
// clearfile("KEflux%d%s",k,appendix);

void clearfile(const char * format, ...)
{
    char cfname[CFSIZE];
    FILE* fp;

    // Safely print the filename
    va_list args;
    va_start (args, format);
    vsnprintf(cfname, CFSIZE, format, args);
    va_end (args);

    // Clear the file
    fp = fopen(cfname,"w");
    fclose(fp);
    return;
}

// iprintf - Prints to stdout and also logs to igw.log.
//           Usage is exactly as you would use printf().
static FILE* igwlog=NULL;
static char igwlogstart = 1;
void iprintf(const char * format, ...)
{
    if (igwlogstart)
    {
        igwlogstart = 0;
        printf("Opening igw.log for logging\n");
        igwlog=fopen("igw.log","w");
        if (igwlog == NULL) printf("Problem opening igw.log, continuing without logging\n");
    }

    va_list args;
    va_start (args, format);
    vprintf(format, args);
    va_end (args);

    if (igwlog) // Only attempt to write if the log has been opened
    {
        va_start (args, format);
        vfprintf(igwlog, format, args);
        va_end (args);
        fflush(igwlog);
    }

    return;
}

/* Extract VmPeak, VmSize, VmHWM and VmRSS from /proc/self/status, return in bytes */
static void get_memory_usage(size_t* VmPeak, size_t* VmSize, size_t* VmHWM, size_t* VmRSS);
static void get_memory_usage(size_t* VmPeak, size_t* VmSize, size_t* VmHWM, size_t* VmRSS)
{
    *VmPeak = (size_t)0L;
    *VmSize = (size_t)0L;
    *VmHWM  = (size_t)0L;
    *VmRSS  = (size_t)0L;
#if defined(__linux__) || defined(__linux) || defined(linux) || defined(__gnu_linux__)
    /* Read the current process' status file from the proc filesystem */
    FILE* procfile = fopen("/proc/self/status", "r");
    long to_read = 8192;
    char buffer[to_read];
    int read = fread(buffer, sizeof(char), to_read, procfile);
    fclose(procfile);

    char *search_result;

    /* Look through proc status contents line by line */
    char delims[] = "\n";
    char *line = strtok(buffer, delims);

    while (line != NULL && (!*VmPeak || !*VmSize || !*VmHWM || !*VmRSS) )
    {
        search_result = strstr(line, "VmPeak:");
        if (search_result != NULL)
        {
            sscanf(line, "%*s %ld", VmPeak);
            *VmPeak *= 1024;
        }

        search_result = strstr(line, "VmSize:");
        if (search_result != NULL)
        {
            sscanf(line, "%*s %ld", VmSize);
            *VmSize *= 1024;
        }

        search_result = strstr(line, "VmHWM:");
        if (search_result != NULL)
        {
            sscanf(line, "%*s %ld", VmHWM);
            *VmHWM *= 1024;
        }

        search_result = strstr(line, "VmRSS:");
        if (search_result != NULL)
        {
            sscanf(line, "%*s %ld", VmRSS);
            *VmRSS *= 1024;
        }

        line = strtok(NULL, delims);
    }
#else
    // Don't know what to do here if we're not on linux
#endif
    return;
}


/* Prints out four ways to quantify memory usage
     VmPeak: The maximum value of VmSize since IGW started
     VmSize: Current virtual memory size
     VmHWM:  The maximum value of VmRSS since IGW started
     VmRSS:  Current actual memory usage
*/
void showmemusage(const char message[])
{
    size_t VmPeak, VmSize, VmHWM, VmRSS;
    get_memory_usage(&VmPeak, &VmSize, &VmHWM, &VmRSS);
    iprintf("Memusage: %8.2f / %8.2f MB VmRSS/VmHWM, %8.2f / %8.2f MB VmSize/VmPeak (%s)\n",
            (double)VmRSS*1e-6,(double)VmHWM*1e-6,(double)VmSize*1e-6,(double)VmPeak*1e-6,message);
//    iprintf("Memusage: %8d / %8d KB VmRSS/VmHWM, %8d / %8d KB VmSize/VmPeak [%.2f%%] (%s)\n",
//            VmRSS/1024,VmHWM/1024,VmSize/1024,VmPeak/1024,100*(double)VmPeak/(double)VmHWM -100.0, message);
}


