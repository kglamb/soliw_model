#include "header.h"

void spline_get_abh(int n, double xvals[], double x, int *lo, int *hi,
		    double *a,double *b, double *h);

/*****************************************************************/
/*****************************************************************/
// routine cubic_spline():           
//
//  INPUTS:
//     n   -> number of data points
//     x[] -> array of x values. Assumed increasing 
//     y[] -> array of y values. 
//   dydx1 -> derivative of y at i=0
//   dydx2 -> derivative of y at i=n-1
//
//  OUTPUTS: 
//   d2y[] -> values of second derivative at the grid points
/*****************************************************************/
/*****************************************************************/

void cubic_spline(int n, double x[], double y[],
               double dydx1, double dydx2, double d2y[])
{
 int i,k;
 double p,qn,r,un,*u;

 u=(double *)malloc((unsigned) (n*sizeof(double)));

 if (fabs(dydx1) > 1.0e20) d2y[0]=u[0]=0.0;
 else{
    d2y[0] = -0.5;
    u[0]=(3.0/(x[1]-x[0]))*((y[1]-y[0])/(x[1]-x[0])-dydx1);
 }
 
 for(i=1;i<n-1;i++) {
    r=(x[i]-x[i-1])/(x[i+1]-x[i-1]);
    p=r*d2y[i-1]+2.0;
    d2y[i]=(r-1.0)/p;
    u[i]=(y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1]);
    u[i]=(6.0*u[i]/(x[i+1]-x[i-1])-r*u[i-1])/p;
 }

 if(fabs(dydx2) > 1.0e20) qn=un=0.0;
 else{
    qn=0.5;
    un=(3.0/(x[n-1]-x[n-2]))*(dydx2-(y[n-1]-y[n-2])/(x[n-1]-x[n-2]));
 }

 d2y[n-1]=(un-qn*u[n-2])/(qn*d2y[n-2]+1.0);
 for (k=n-2;k>=0;k--) d2y[k]=d2y[k]*d2y[k+1]+u[k];

 free((char *)(u));
}


/****************************************/
/****************************************/

void splint_mod(int n, double xvals[], double yvals[], double y2vals[],
                double x, double *y, double *dy, double *d2y)
{
	int lo,hi,k;
	double h,b,a;

	lo=1;
	hi=n;
	while (hi-lo > 1) {
		k=(hi+lo) >> 1;
		if (xvals[k] > x) hi=k;
		else lo=k;
		//		printf("lo = %d,  k = %d,  hi = %d\n",lo,k,hi);
	}
	h=xvals[hi]-xvals[lo];
	//	printf("h = %1.5e\n",h);
	if (h == 0.0){
           printf("two x grid values input to routine splint_mod are the same: Exiting.");
           exit(0);
	}
	a=(xvals[hi]-x)/h;
	b=(x-xvals[lo])/h;
	*y = a*yvals[lo]+b*yvals[hi] 
               + ((a*a*a-a)*y2vals[lo]+(b*b*b-b)*y2vals[hi])*(h*h)/6.0;
	*dy = (yvals[hi]-yvals[lo])/h
                 + ( (3*b*b-1)*y2vals[hi]-(3*a*a-1)*y2vals[lo])*h/6.0;
        *d2y = b*y2vals[hi]+a*y2vals[lo];
}


/****************************************/
/****************************************/

void spline_func_val(int n, double xvals[], double yvals[], double y2vals[],
                double x, double *y)
{
 int lo,hi;
 double h,b,a;
 spline_get_abh(n,xvals,x,&lo,&hi,&a,&b,&h);
 *y=a*yvals[lo]+b*yvals[hi]
              +((a*a*a-a)*y2vals[lo]+(b*b*b-b)*y2vals[hi])*(h*h)/6.0;
}
/****************************************/
/****************************************/

void spline_func_der_val(int n, double xvals[], double yvals[],
                         double y2vals[], double x, double *dy)
{
 int lo,hi;
 double h,b,a;
 spline_get_abh(n,xvals,x,&lo,&hi,&a,&b,&h);
 *dy = (yvals[hi]-yvals[lo])/h 
         + ( (3*b*b-1)*y2vals[hi]-(3*a*a-1)*y2vals[lo])*h/6.0;
}

/****************************************/
/****************************************/

void spline_func_der2_val(int n, double xvals[], double yvals[],
                          double y2vals[],double x, double *d2y)
{
 int lo,hi;
 double h,b,a;
 spline_get_abh(n,xvals,x,&lo,&hi,&a,&b,&h);
 *d2y = b*y2vals[hi]+a*y2vals[lo];
}

/****************************************************************/
/**  this part common to calculation of y and its derivatives  **/
/****************************************************************/

void spline_get_abh(int n, double xvals[], double x, int *lo, int *hi,
                      double *a,double *b, double *h)
{

   int k;
   *lo=1;
   *hi=n;
   while (*hi-*lo > 1) {
      k=(*hi+*lo) >> 1;
      if (xvals[k] > x) *hi=k;
      else *lo=k;
//      printf("lo = %d,  k = %d,  hi = %d\n",lo,k,hi);
   }
   *h=xvals[*hi]-xvals[*lo];
//    printf("h = %1.5e\n",h);
   if((*h) == 0.0){
      printf("two x grid values input to routine splint_mod are the same: Exiting.");
      exit(0);
   }
   *a=(xvals[*hi]-x)/(*h);
   *b=(x-xvals[*lo])/(*h);

 }
