#ifndef __HELPER_FUNCTIONS_H__
#define __HELPER_FUNCTIONS_H__

void clearfile(const char *format, ...);
void iprintf(const char * format, ...);
void showmemusage(const char message[]);

#endif  /* __HELPER_FUNCTIONS_H__ */
