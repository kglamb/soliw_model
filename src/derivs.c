#include "header"

extern PREC K,r10,r01,r20;
extern PREC c;
extern PREC subphi1,subphi2,subphi3,subphi4,subphi5,subphi6;
extern PREC subphi;


#if 1
   void derivs1(x,y,dydx)
   PREC x,*y,*dydx;
   {
   PREC NN0();

   dydx[1] = y[2];
   dydx[2] = -NN0(x)/(y[3]*y[3])*y[1];
   dydx[3] = 0.0;
   }

#else

   void derivs1(x,y,dydx)
   PREC x,*y,*dydx;
 
   {
   PREC U0(),ddU0(),NN0();

   dydx[1] = y[2];
   dydx[2] = (K*K - NN0(x)/((U0(x)-y[3])*(U0(x)-y[3]))
               + ddU0(x)/(U0(x)-y[3]))*y[1];
   dydx[3] = 0.0;
}
#endif

/***********************************************************/
/***********************************************************/
void derivs10(z,y,dydz)
PREC z,*y,*dydz;

{
PREC c1,c2,c3;
PREC U0(),NN0(),NN0_dz();
extern PREC dsubphi,dsubphi2,subz;
PREC t1,t2;

c1 = U0(z)-c;
c2 = c1*c1;
c3 = c1*c1*c1;

dydz[1] = y[2];
t1 = subphi+dsubphi*(z-subz);
t2 = subphi2+dsubphi2*(z-subz);
dydz[2] = -(NN0(z)/c2)*y[1] - NN0_dz(z)*t2/c3 + y[3]*2.0*NN0(z)*t1/c3;
dydz[3] = 0.0;
}


/***********************************************************/
/***********************************************************/
void derivs20(z,y,dydz)
PREC z,*y,*dydz;
{
PREC c1,c2,c3,c4;
PREC NN0(),NN0_dz(),NN0_ddz();
extern PREC subz,subphi1,subphi2,subphi3,subphi4,subphi5,subphi6;
extern PREC dsubphi1,dsubphi2,dsubphi3,dsubphi4,dsubphi5,dsubphi6;
PREC t1,t2,t3,t4,t5,t6,t7;
PREC NN,dNN,d2NN;

c2 = c*c;
c3 = c2*c;
c4 = c3*c;
t7 = z-subz;
t1 = subphi1 + dsubphi1*t7;
t2 = subphi2 + dsubphi2*t7;
t3 = subphi3 + dsubphi3*t7;
t4 = subphi4 + dsubphi4*t7;
t5 = subphi5 + dsubphi5*t7;
t6 = subphi6 + dsubphi6*t7;

NN = NN0(z);
dNN = NN0_dz(z);
d2NN = NN0_ddz(z);

dydz[1] = y[2];
dydz[2] = -NN/c2*y[1]
           -( r10*NN*(t4 + 4.0*r10*t6) + 0.5*d2NN*t2 - 3.0*r10*dNN*t3)/c4
             + (2.0*dNN*t1 - NN*( (8.0/3.0)*r10*t5  + 2.0*y[3]*t6))/c3;
dydz[3] = 0.0;
}



/***********************************************************/
/***********************************************************/
void derivs11a(z,y,dydz)
PREC z,*y,*dydz;
{
PREC c1,c2,c3,c4;
PREC NN0(),NN0_dz();
extern PREC subz,subphi1,subphi2,subphi3,subphi4,subphi5,subphi6;
extern PREC dsubphi1,dsubphi2,dsubphi3,dsubphi4,dsubphi5,dsubphi6;
PREC t1,t2,t3,t4,t5,t6,t7;
PREC NN,dNN;

c2 = c*c;
c3 = c2*c;
c4 = c3*c;
t7 = z-subz;
t1 = subphi1 + dsubphi1*t7;
t2 = subphi2 + dsubphi2*t7;
t3 = subphi3 + dsubphi3*t7;
t4 = subphi4 + dsubphi4*t7;
t5 = subphi5 + dsubphi5*t7;
t6 = subphi6 + dsubphi6*t7;

NN = NN0(z);
dNN = NN0_dz(z);

dydz[1] = y[2];
dydz[2] = -NN/c2*y[1] - 2.0*t1
           +(3.0*r01*dNN*t3 - 3.0*r01*NN*(t4 + 4.0*r10*t6))/c4
             + (2.0*dNN*t2 - 4.0*NN*(r01*t1 + 0.5*y[3]*t6 + r10*t5))/c3;
dydz[3] = 0.0;
}



/***********************************************************/
/***********************************************************/
void derivs11b(z,y,dydz)
PREC z,*y,*dydz;
{
PREC c1,c2,c3,c4;
PREC NN0(),NN0_dz();
extern PREC subz,subphi1,subphi2,subphi3,subphi4,subphi5,subphi6;
extern PREC dsubphi1,dsubphi2,dsubphi3,dsubphi4,dsubphi5,dsubphi6;
PREC t1,t2,t3,t4,t5,t6,t7;
PREC NN,dNN;

c2 = c*c;
c3 = c2*c;
c4 = c3*c;
t7 = z-subz;
t1 = subphi1 + dsubphi1*t7;
t2 = subphi2 + dsubphi2*t7;
t3 = subphi3 + dsubphi3*t7;
t4 = subphi4 + dsubphi4*t7;
t5 = subphi5 + dsubphi5*t7;
t6 = subphi6 + dsubphi6*t7;

NN = NN0(z);
dNN = NN0_dz(z);

dydz[1] = y[2];
dydz[2] = -NN/c2*y[1] - 6.0*t1
           +(3.0*r01*dNN*t3 + 3.0*r01*NN*(t4 - 6.0*r10*t6))/c4
             + (2.0*dNN*t2 - NN*( 2.0*y[3]*t6 + 12.0*r10*t5))/c3;
dydz[3] = 0.0;
}

/***********************************************************/
/***********************************************************/
void derivs02(z,y,dydz)
PREC z,*y,*dydz;
{
PREC c1,c2;
PREC NN0();
extern PREC subz,subphi1,subphi2,dsubphi1,dsubphi2;
PREC t1,t2;

c2 = c*c;
t1 = subphi1 + dsubphi1*(z-subz);
t2 = subphi2 + dsubphi2*(z-subz);

dydz[1] = y[2];
dydz[2] = -NN0(z)*(y[1]/c2+2.0*(y[3]*t2+r01*t1)/(c2*c)+3.0*r01*r01*t2/(c2*c2)) - t1;
dydz[3] = 0.0;
}



/***********************************************************/
/***********************************************************/
void derivs4(z,y,dydz)
PREC z,*y,*dydz;
{
PREC c1,c2,c3,c4;
PREC U0(),ddU0(),NN0(),NN0_dz(),NN0_ddz();

c1 = U0(z)-y[3];
c2 = c1*c1;
c3 = c1*c1*c1;
c4 = c1*c1*c1*c1;

dydz[1] = y[2];
dydz[2] = (K*K - NN0(z)/c2 +ddU0(z)/c1)*y[1]
           - 2.0*NN0_dz(z)*subphi1/c3 - 0.5*NN0_ddz(z)*subphi2/c4
             + 3.0*r10*NN0_dz(z)*subphi3/c4 - r10*NN0(z)*subphi4/c4
              + (8.0/3.0)*r10*NN0(z)*subphi5/c3 - 4.0*r10*r10*NN0(z)*subphi6/c4
                  + 2.0*r20*NN0(z)*subphi6/c3;
dydz[3] = 0.0;
}



/***********************************************************/
/***********************************************************/

void derivs5(z,y,dydz)
PREC z,*y,*dydz;
{
PREC c1,c2,c3;
PREC U0(),ddU0(),NN0();

c1 = U0(z)-y[3];
c2 = c1*c1;
c3 = c1*c1*c1;

dydz[1] = y[2];
dydz[2] = (K*K - NN0(z)/c2 +ddU0(z)/c1)*y[1] - subphi1 + r01*2.0*NN0(z)*subphi1/c3;
dydz[3] = 0.0;
} 
