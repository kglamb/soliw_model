#include <stdio.h>
#include "header.h"

extern double **U ,**W, **D, **APEdens ,**eta;
extern int I, J;

void store_soln(double A) {
  char file[40];
  FILE *f1;
  int i,j;


  sprintf(file,"binU_A%g",A); 
  f1 = fopen(file,"w");
  for(i=0;i<I;i++)   fwrite(U[i],sizeof(double),J,f1);
  fclose(f1);

  sprintf(file,"binW_A%g",A); 
  f1 = fopen(file,"w");
  for(i=0;i<I;i++)   fwrite(W[i],sizeof(double),J,f1);
  fclose(f1);

  sprintf(file,"binD_A%g",A); 
  f1 = fopen(file,"w");
  for(i=0;i<I;i++)   fwrite(D[i],sizeof(double),J,f1);
  fclose(f1);

  sprintf(file,"binAPEdens_A%g",A); 
  f1 = fopen(file,"w");
  for(i=0;i<I;i++)   fwrite(APEdens[i],sizeof(double),J,f1);
  fclose(f1);

}
