##################################################################
# makefile template for SOLIW
# 
# Modified April 2009 -- Ben Langmuir
#
# This makefile expects GNU make.  If make isn't working, try
# gmake instead.
##################################################################
SHELL = /bin/sh

# This makefile is intended to work without modification for the
# majority of users.  The following variables are meant to be set
# if you need to tweak the parameters here.  The functionality in
# mind is for testing purposes.
#
# INC     -- include location for header files
# LIBS    -- libraries to add to the linking
# CC      -- C compiler
# CFLAGS  -- compile flags
# LDFLAGS -- linking flags
# DEBUG   -- set to y to enable debug mode
# DBG_LVL -- manually set the debug level (only meaningful if DEBUG=y)

# Defaults
# NB: Modifying these may not work if your machine has specific setting below.
#     In such cases, either modify the settings in the machine-dependent section
#     or set these at the command line (e.g. make CC=icc CFLAGS=-fast), which
#     should always override what is set anywhere in this file.

#********************************************************
# ---> Select model file to be used <---
#********************************************************

#MODEL = Michallet_Ivey_tank_waves.c
#MODEL = Michallet_Ivey_tank_data_file.c
#MODEL = Daniel_Bourgaults_case.c
#MODEL = test_background_state_from_file.c
#MODEL = King_Carr_Dritschel.c
#MODEL = tanh_stratification_WNL_shoaling.c
#MODEL = tanh_stratification.c
#MODEL = AsiaEx_shoaling.c
#MODEL = AsiaEx_observed_stratification.c
#MODEL = Xie_et_al.c
#MODEL = tanh_stratification_shear_layer.c
MODEL = three_layer_stratification.c
#MODEL = Luzon_Strait.c
#MODEL = Georges_Bank.c   
#MODEL = Oregon_Shelf_wave_case1_hr.   
#MODEL = exponential_stratification.c

#********************************************************
#********************************************************
#********************************************************

INC =
LIBS =
CC = gcc
CFLAGS = -O2
LDFLAGS =
DEBUG = n
DBG_LVL = 9

#Set to n to diasble spud libraries (but preferably use 'make no-spud' which
#is more robust.
SPUD = n

# Parameters that shouldn't change (or things will likely not work at all)
TESTDIR = tests
# export allows recursively called makes to recieve the variable (i.e. tests)
export OBJDIR  = OBJECT_FILES
BASE_INC = -I. -Isrc -Iinc
BASE_LIBS= -lm -lfftw3
BASE_LDFLAGS =
BASE_CFLAGS =

# Set spud dependencies
ifeq ($(SPUD),y)
  BASE_LIBS += -lspud -lstdc++
endif
export LIBS
export BASE_LIBS

# Set debugging options
ifeq ($(DEBUG),y)
  DEBFLAGS = -g -DDEBUG_LEVEL=$(DBG_LVL)
endif

# Set platform-dependent options
# Use SYSNAME or MACHINE to determine platform
# operating system name
SYSNAME  = $(shell uname -s)
# machine (hardware) type
MACHINE  = $(shell uname -m)

# Apple G5 (Power Macintosh)
# NB: Will capture older PPCs as well, but those probably won't work
ifeq ($(MACHINE),Power Macintosh)
  BASE_CFLAGS += -DOSX
endif


# Set options for specific hosts at uwaterloo
# Some variables here are required for IGW to compile/work
# on these hosts -- these are genereally prefixed with BASE_.
# Other variables are meant to be overriden if a user sets
# the variable at the command line (e.g. CFLAGS).
#
# THESE OPTIONS ONLY APPLY TO UNVERSITY OF WATERLOO MACHINES
HOSTNAME = $(shell hostname)

# moisie2
ifeq ($(HOSTNAME),moisie2.math)
  CFLAGS = -O3
  BASE_INC += -I/usr/include/fftw/
  BASE_LDFLAGS += -L/opt/SUNWspro/lib/
  BASE_LIBS += -lsunmath
endif 

# winisk
ifeq ($(HOSTNAME),winisk)
  # icc will usually give better performance than gcc.
  CC = icc
  CFLAGS = -O3
  BASE_LDFLAGS += -L/usr/local/fftw3-icc/lib/
  BASE_INC += -I/usr/local/fftw3-icc/include/

  # User set CC=gcc
  ifeq ($(CC),gcc)
    CFLAGS = -O3
    BASE_LDFLAGS += -L/usr/local/fftw3-gcc/lib/
    BASE_INC += -I/usr/local/fftw3-gcc/include/
  endif
endif


# thelon
ifeq ($(HOSTNAME),thelon.math.uwaterloo.ca)
  CC = icc
  CFLAGS = -O3 -fp-model precise -openmp -lpthread
  BASE_INC += -I/usr/local/fftw3-icc/include/
  BASE_LDFLAGS += -L /usr/local/fftw3-icc/lib/

  # User set CC=gcc
  ifeq ($(CC),gcc)
    CFLAGS = -O3
    BASE_INC += -I/usr/local/fftw3-gcc/include/
    BASE_LDFLAGS += -L /usr/local/fftw3-gcc/lib/
  endif
endif

# hood
ifeq ($(HOSTNAME),hood)
  CC = icc
  CFLAGS = -O3 -fp-model precise -openmp -lpthread
  BASE_INC += -I/usr/local/fftw3-icc/include/
  BASE_LDFLAGS += -L /usr/local/fftw3-icc/lib/

endif
# gironde
ifeq ($(HOSTNAME),gironde.math.uwaterloo.ca)
  CFLAGS = -O3
  LDFLAGS = -Wl,-stack_size -Wl,10000000 
  ifeq ($(SPUD),y)
    LD = g++
  else
    LD = gcc
  endif
endif


# makobe
ifeq ($(HOSTNAME),makobe.math.uwaterloo.ca)
  CFLAGS = -O3
  LDFLAGS = -Wl,-stack_size -Wl,10000000 
  ifeq ($(SPUD),y)
    LD = g++
  else
    LD = gcc
  endif
endif

# Legacy machines
# flexor
ifeq ($(HOSTNAME),flexor)
  ifeq ($(DEBUG),n)
    CFLAGS = -DNDEBUG -C -64 -IPA -G0 -Ofast=ip35 -w
  else
    CFLAGS = -DDEBUG -O0 -w
  endif
  CC = c99
endif

# snowball
ifeq ($(HOSTNAME),snowball)
  CFLAGS = -Xa -DSTORE_ETRANSPOSE=1 -Caopt -pvctl fullmsg loopcnt=50000 noIassume noloopchg
  CC = c++
endif

# monolith
ifeq ($(HOSTNAME),monolith)
  CFLAGS = -O5
  CC = cc
endif


# Add debug flags if DEBUG=y
BASE_CFLAGS += $(DEBFLAGS)

# Set LD to be the C compiler (unless the user sets it manually at the command line)
ifndef $(LD)
  LD = $(CC)
else
  CFLAGS += $(DEBFLAGS)
endif

export CC
export LD
export CFLAGS
export BASE_CFLAGS
export LDFLAGS
export BASE_LDFLAGS

VPATH = .:models:src

export FILES_SRC = main.c get_eigfns1.c ode_rkf45.c input_output_routines.c get_eta.c compute_solitary_wave.c store.c sin_transforms.c interpolate.c allocation.c cubic_spline_routines.c helper_functions.c

FILES = $(MODEL) $(FILES_SRC)

#Objects need to have a .o extension
OBJECTS = $(addprefix $(OBJDIR)/,$(addsuffix .o, $(basename $(FILES))))

PROGRAMS = soliw $(addprefix soliw, 1 2 3)

.PHONY: all spud no-spud clean test
all: soliw
spud: 
	cp GNUmakefile GNUmakefile.old
	cp inc/header.h inc/header.h.old
	sed '1,/SPUD = [yn]/s/SPUD = [yn]/SPUD = y/' GNUmakefile.old > GNUmakefile
	sed 's/#define SPUD [0-9]/#define SPUD 1/' inc/header.h.old > inc/header.h
	@echo ""
	@echo "Spud enabled, now run 'make'"

no-spud:
	cp GNUmakefile GNUmakefile.old
	cp inc/header.h inc/header.h.old
	sed '1,/SPUD = [yn]/s/SPUD = [yn]/SPUD = n/' GNUmakefile.old > GNUmakefile
	sed 's/#define SPUD [0-9]/#define SPUD 0/' inc/header.h.old > inc/header.h 
	@echo
	@echo "Spud diasbled, now run 'make'"

# Quick notes on makefiles:
# $@ represents the target file
# $< represents the first dependency
# For implicit rules, the first dependency is the source file .c
$(PROGRAMS) :$(OBJECTS) GNUmakefile
	$(LD) $(CFLAGS) $(BASE_CFLAGS) $(OBJECTS) $(BASE_LDFLAGS) $(LDFLAGS) $(LIBS) $(BASE_LIBS) -o $@

lint: $(FILES) GNUmakefile  
	lint $(FILES)

$(OBJDIR)/%.o : %.c GNUmakefile
	$(shell if test ! -d $(OBJDIR); then mkdir $(OBJDIR); fi)
	$(CC) $(CFLAGS) $(BASE_CFLAGS) $(BASE_INC) $(INC) -c $< -o $@

clean:
	-rm $(OBJDIR)/*.o
	-rm $(PROGRAMS)
	cd $(TESTDIR); $(MAKE) $@ 
test:
	@cp inc/header.h inc/header.h.tmp
	@sed '1,/I [0-9]*/s/I [0-9]*/I 300/; 1,/J [0-9]*/s/J [0-9]*/J 100/' inc/header.h.tmp > inc/header.h
	$(MAKE) $(addprefix $(OBJDIR)/,$(addsuffix .o, $(basename $(FILES_SRC))))
	cp -r $(OBJDIR) $(TESTDIR) 
	cd $(TESTDIR); $(MAKE) $@
	@mv inc/header.h.tmp inc/header.h

hires_test:
	@cp inc/header.h inc/header.h.tmp
	@sed '1,/I [0-9]*/s/I [0-9]*/I 600/; 1,/J [0-9]*/s/J [0-9]*/J 200/' inc/header.h.tmp > inc/header.h
	$(MAKE) $(addprefix $(OBJDIR)/,$(addsuffix .o, $(basename $(FILES_SRC))))
	cp -r $(OBJDIR) $(TESTDIR)/HIRES_$(OBJDIR)
	cd $(TESTDIR); $(MAKE) $@
	@mv inc/header.h.tmp inc/header.h

#Packages the source so that it can be distributed
.PHONY: release
release:
	mkdir soliw_model
	mkdir soliw_model/src
	mkdir soliw_model/models
	mkdir soliw_model/$(OBJDIR)
	cp src/*.c soliw_model/src
	mkdir soliw_model/inc
	cp inc/*.h soliw_model/inc
	cp models/*.c soliw_model/models
	cp GNUmakefile soliw_startup soliw_model/.
	tar -cvf soliw_model_`date +%d.%m.%y`.tar soliw_model/*
	rm -r soliw_model
	@echo
	@echo Notice: soliw_startup is taken from the current directory
