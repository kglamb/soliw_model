#include <stdio.h>
#include <math.h>
#include "header.h"

/***  three layer stratification      **/

#if 1

  extern double drho_halved1;
  extern double drho_halved2;
  extern double zpyc1 = -0.2;
  extern double zpyc2 = -0.75;
  extern  double dpyc1 = 0.03;
  extern  double dpyc2 = 0.03;

#endif

#define twice_atanh0p99 5.29330482472499


extern double H,L,g;
void solitary_waves_to_compute()
{
   void compute_solitary_wave();
   int i;

#if 0   // for stratification 1
   compute_solitary_wave(1.0e-2,L,"WNL",1.0e-7);
   compute_solitary_wave(2.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(3.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(4.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(5.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(6.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(7.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(8.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(9.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(10.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(12.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(14.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(16.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(18.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(20.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(22.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(24.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(26.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(28.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(30.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(32.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(34.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(36.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(38.0e-2,L,"prev",1.0e-7);
   compute_solitary_wave(40.0e-2,L,"prev",1.0e-7);
#elif 0
   compute_solitary_wave(1.0e-2,L,"WNL",1.0e-7);
   compute_solitary_wave(0.8e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.6e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.55e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.50e-2,L,"prev",1.0e-7);
   //   compute_solitary_wave(0.495e-2,L,"prev",1.0e-7);
   //   compute_solitary_wave(0.490e-2,L,"prev",1.0e-7);
   //   compute_solitary_wave(0.485e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.480e-2,L,"prev",1.0e-7);
   //   compute_solitary_wave(0.475e-2,L,"prev",1.0e-7);
   //   compute_solitary_wave(0.470e-2,L,"prev",1.0e-7);
   //   compute_solitary_wave(0.465e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.460e-2,L,"prev",1.0e-7);
   //   compute_solitary_wave(0.455e-2,L,"prev",1.0e-7);
   //   compute_solitary_wave(0.450e-2,L,"prev",1.0e-7);
   //   compute_solitary_wave(0.445e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.440e-2,L,"prev",1.0e-7);
   //   compute_solitary_wave(0.395e-2,L,"prev",1.0e-7);
   //   compute_solitary_wave(0.390e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.420e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.40e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.38e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.36e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.34e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.32e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.30e-2,L,"prev",1.0e-7);
   /*
   compute_solitary_wave(0.385e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.380e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.375e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.370e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.365e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.360e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.355e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.350e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.345e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.340e-2,L,"prev",1.0e-7);
   */
#elif 0  // strat 2 depressions
   compute_solitary_wave(1.0e-2,L,"WNL",1.0e-7);
   compute_solitary_wave(0.8e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.6e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.40e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.32e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.30e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.28e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.27e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.26e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.25e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.24e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.23e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.22e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.21e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.209e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.208e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.207e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.206e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.205e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.204e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.203e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.202e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.201e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.20e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.19e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.18e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.16e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.14e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.12e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.10e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.08e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.06e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.04e-2,L,"prev",1.0e-7);
   compute_solitary_wave(0.02e-2,L,"prev",1.0e-7);
#else // strat 2 elevations
   compute_solitary_wave(1.0e-2,L,"WNL",1.0e-7);
   for(i=1;i<=4;i++){
     compute_solitary_wave(1.0e-2 - i*0.2e-2,L,"prev",1.0e-7);
   }
   /*
   compute_solitary_wave(1.0e-2,L,"WNL",1.0e-7);
   compute_solitary_wave(1.2e-2,L,"prev",1.0e-7);
   compute_solitary_wave(1.4e-2,L,"prev",1.0e-7);
   compute_solitary_wave(1.6e-2,L,"prev",1.0e-7);
   compute_solitary_wave(1.8e-2,L,"prev",1.0e-7);
   compute_solitary_wave(2.0e-2,L,"prev",1.0e-7);
   */
#endif


#if 0
   compute_solitary_wave(50.0,L,"WNL",1.0e-6);
   #if 1
      for(i=1;i<=35;i++) compute_solitary_wave(50.0+i*10.0,L,"prev",1.0e-6);
   //   for(i=1;i<=600;i++) compute_solitary_wave(1000.0+i*5.0,L,"prev",0.0001);
      for(i=1;i<=1320;i++) compute_solitary_wave(400.0+i*5.0,L,"prev",1.0e-6);
   #else
      for(i=1;i<=30;i++) compute_solitary_wave(50.0+i*5.0,L,"prev",1.0e-4);
      for(i=1;i<=3400;i++) compute_solitary_wave(200.0+i*2.0,L,"prev",1.0e-4);
   #endif

#elif 0

   compute_solitary_wave(0.0025,L,"WNL",1.0e-6);
   #if 1
      for(i=1;i<=11;i++) compute_solitary_wave(0.0025+i*0.0005,L,"prev",1.0e-6);
   //   for(i=1;i<=600;i++) compute_solitary_wave(1000.0+i*5.0,L,"prev",0.0001);
      //      for(i=1;i<=1320;i++) compute_solitary_wave(0.02+i*0.0001,L,"prev",1.0e-6);
      //      for(i=1;i<=1320;i++) compute_solitary_wave(0.008+i*0.0001,L,"prev",1.0e-6);
      for(i=1;i<=500;i++) compute_solitary_wave(0.008+i*0.000025,L,"prev",1.0e-6);
   #else
      for(i=1;i<=30;i++) compute_solitary_wave(50.0+i*5.0,L,"prev",1.0e-4);
      for(i=1;i<=3400;i++) compute_solitary_wave(200.0+i*2.0,L,"prev",1.0e-4);
   #endif

#endif

   //   for(i=1;i<=20;i++) compute_solitary_wave(100.0+i*20.0,L,"prev",0.0001);
   //   for(i=1;i<=100;i++) compute_solitary_wave(500.0+i*10.0,L,"prev",0.0001);
   //  for(i=5;i<=31;i++) compute_solitary_wave(i*0.25e2,L,"prev",0.0001);
   //   for(i=1;i<=20;i++) compute_solitary_wave(775.0+i*2.5,L,"prev",0.0001);
   //   for(i=1;i<=40;i++) compute_solitary_wave(600.0+2.5*i,L,"prev",0.0001);
   //   for(i=1;i<=500;i++) compute_solitary_wave(1000.0+5.0*i,L,"prev",0.0001);

}


   double density(double z)
   {
     return -drho_halved1*tanh((z-zpyc1)/dpyc1) -drho_halved2*tanh((z-zpyc2)/dpyc2);
   }
   /*********************************************************/
   /*********************************************************/
   double d_density(double z)
   {
     double s1,s2;
     s1 = 1.0/cosh((z-zpyc1)/dpyc1);
     s2 = 1.0/cosh((z-zpyc2)/dpyc2);
     return -drho_halved1*s1*s1/dpyc1 - drho_halved2*s2*s2/dpyc2;
   }
   /*********************************************************/
   /*********************************************************/
   double d2_density(double z)
   {
     double s1,s2,t1,t2;
     s1 = 1.0/cosh((z-zpyc1)/dpyc1);
     s2 = 1.0/cosh((z-zpyc2)/dpyc2);
     t1 = tanh((z-zpyc1)/dpyc1);
     t2 = tanh((z-zpyc2)/dpyc2);
     return 2*drho_halved1*t1*s1*s1/(dpyc1*dpyc1) + 2*drho_halved2*t2*s2*s2/(dpyc2*dpyc2);
   }


   /*********************************************************/
   /*********************************************************/

double Uback(double z)
{
return(0.0);
}

double dUback(double z)
{
 return(0.0);
}

double d2Uback(double z)
{
return(0.0);
}

void setup_background_info(){}
