/*****************************************************************************/
/*****************************************************************************/
/***   compute_solitary_wave(A,domainwidth,eta_sousrce,epsilon)            ***/
/***                                                                       ***/
/***              A: specified available potential energy of wave,         ***/
/***                 = integral of Ea                                      ***/
/***                                                                       ***/
/***    domainwidth: horizontal length of domain wave is                   ***/
/***                  computed in.                                         ***/
/***                                                                       ***/
/***     eta_source: if WNL use weakly non-linear theory for initial guess ***/
/***                 if prev use previous wave for initial guess.          ***/
/***                 if "etafile" load from  file "eta".                   ***/
/***                                                                       ***/
/***        epsilon: one convergence criteria. Iterate in get_eta()        ***/
/***                 until maximum difference between successive           ***/
/***                 etas is less than epsilon*H (H=water depth).          ***/
/***                                                                       ***/
/***                                                                       ***/
/***                                                                       ***/
/***   This routine calles get_eta() which returns eta and                 ***/
/***   coef = g*H/c^2 where c = wave propagation speed.                    ***/
/***   The rest of the routine computes and stores various                 ***/
/***   quantities associated with the wave.                                ***/
/***                                                                       ***/
/***                                                                       ***/
/***   Grid is IxJ cells. Values of eta computed at cell centres.          ***/
/*****************************************************************************/
/*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "header.h"
#include "allocation.h"
#include "sin_transforms.h"

#define eps 0.00001
#define  hmin 0.00001

extern int I, J;

static double partpos[2]; 
static double APEdens_val (double z, double eta, double rho);
double Uback ();
void partvel ();
inline double zval(int j, double dz, double Lz);
inline double xval(int i, double dx);
extern void get_eta (), odeint (), store_soln ();
extern double density (double);
extern double interp_find_poi (double f0, double f1, double f2, int i);
extern double interp_find_poi_index (double f0, double f1, double f2, int i);
extern double interp_right_intersect_index (double v, double f0, double f1, double f2, int i);
extern double interp_left_intersect_index (double v, double f0, double f1, double f2, int i);
extern double at_i_j (double i, double j, int N, int M, double *G);
extern double g,H,L;
double coef;

// Output options
extern int NUM_PART;
extern int PARTICLE_TRACKING;
extern int UBACKGROUND;
extern int UIEG_OUT;
extern int D_OUT;
extern int TEST_OUT;

#if MacOSX
extern double **U, **W, **D, **eta;
#else
#endif

//double U[I][J], W[I][J], D[I][J], APEdens[I][J], eta[I][J];
double **U=NULL, **W=NULL, **D=NULL, **APEdens=NULL, **eta=NULL;
static double phspd, dx, dz,dz_part;


void compute_solitary_wave_allocation(){

  U = alloc_matrix(I,J);
  W = alloc_matrix(I,J);
  D = alloc_matrix(I,J);
  APEdens = alloc_matrix(I,J);
  eta = alloc_matrix(I,J);

}

void compute_solitary_wave (double A, double domainwidth, 
                            char eta_source[], double epsilon) {

 double KE, KEpert,PE, APE, MASS, temp, half_length,half_length_U;
 double height, dens_ext;
 double surU, max_surU, min_surU, maxU, minU, surtrans, bottrans; 
 double time, dt, xinit, Ubk;
 int  TrappedCore_Flag, iCore_start, iCore_end, Core_Bnd;
 double Amp_Core, temp_Amp_Core;
 double int_half_width,eta_bnd,deltaz;

 int iUmax, jUmax, iUmin, jUmin, Ietaext, Jetaext;
 int i, j, m, I_lambda;
 double Ri,minRi,minRi_z,Dlower,Dupper, Uzsq,Uz,NN;
 double xleft,xright,ratio;

 FILE *f1, *f2, *f3;
 char file[80]; 

 double eta_dz[I][J];
 double eta_dx[I][J];

 dx = domainwidth/I;
 dz = H/J;

 f1 = fopen ("eta_zvals", "w");
 for (j=0; j<J; j++) fprintf (f1, "%1.6e\n", zval(j,dz,H));
 fclose (f1);

 get_eta (A, domainwidth, &coef, eta_source, epsilon);

/*
   printf ("SCALING ETA\n");
   f1 = fopen ("solitary_wave_data", "a");
   fprintf (f1, "SCALING eta\n");
   fclose (f1);
   for (i=1; i<I; i++) for (j=1; j<=J; j++) eta[i][j] *= 1.08;
 */

 phspd = sqrt (g*H/coef);
 printf ("phspd = %1.6e\n", phspd);
 f1 = fopen ("solitary_wave_data", "a");
 fprintf (f1, "     phspd = %1.5e,  coef = %1.5e\n", phspd, coef);
 fclose (f1);

/*********************************************************/
/****  Compute velocity and density fields from eta.  ****/
/****  Values computed at cell centres at points      ****/
/****  ((i+0.5)*dx,(j+0.5)*dz) --> at the             ****/
/****  same points as eta.                            ****/
/********************************************************/

/* Computing velocities */
// sin_x_deriv((double*)eta, (double*)eta_dx, I, J,  L);

 sin_x_deriv((double*)eta[0], (double*)eta_dx[0], I, J,  L);
 sin_z_deriv((double*)eta[0], (double*)eta_dz[0], I, J,  H);


 if(UBACKGROUND){
    for(i=0; i<I; i++){
       for(j=0; j<J; j++){
	 Ubk = Uback (zval(j,dz,H) - eta[i][j]);
	  U[i][j] = Ubk + (phspd-Ubk) * eta_dz[i][j];
       }
    }
    for(j=0; j<J; j++){
       for(i=0; i<I; i++){
	 Ubk = Uback (zval(j,dz,H) - eta[i][j]);
	  W[i][j] = -(phspd-Ubk) * eta_dx[i][j];
       }
    }
 }
 else{
    for(i=0;i<I;i++) for(j=0;j<J;j++) U[i][j] = phspd * eta_dz[i][j];
    for(i=0;i<I;i++) for(j=0;j<J;j++) W[i][j] = -phspd * eta_dx[i][j];
 }

 for(i=0;i<I;i++) for(j=0;j<J;j++) D[i][j] = density(zval(j,dz,H) - eta[i][j]);
 for(i=0;i<I;i++) for(j=0;j<J;j++) APEdens[i][j] = APEdens_val(zval(j,dz,H),eta[i][j],D[i][j]);

/********************************************************/
/********************************************************/
/***                                                  ***/
/*** Calculate and store various quantities.          ***/
/***                                                  ***/
/********************************************************/
/********************************************************/

/**********************************************/
/***  0) Store velocity and density fields ****/
/**********************************************/

 store_soln(A);

/***************************************/
/***  1) Find extreme value of eta.  ***/
/***************************************/

/* first find extreme value of eta on the grid */
double eta_ext = 0.0;
for(i=1; i<I; i++) {
   for(j=1; j<J; j++) {
      if(fabs (eta[i][j]) > eta_ext) {
	 eta_ext = fabs(eta[i][j]);
	 Ietaext = i;
	 Jetaext = j;
      }
   }
}

/**    Next interpolate in x to find extreme value of eta at     **/
/**    three heights: j = Jetaext-1, Jetaext, and Jetaext+1      **/
 
 double x_intpl_eta_ext_above,x_intpl_eta_ext,x_intpl_eta_ext_below;
 double ipl_Ietaext_above,ipl_Ietaext,ipl_Ietaext_below;


 /* get interpolated eta extremes */

 x_intpl_eta_ext_above = interp_find_poi(eta[Ietaext-1][Jetaext+1],
                                         eta[Ietaext][Jetaext+1],
                                         eta[Ietaext+1][Jetaext+1],Ietaext-1);
                                                
 x_intpl_eta_ext = interp_find_poi(eta[Ietaext-1][Jetaext],
                                   eta[Ietaext][Jetaext],
                                   eta[Ietaext+1][Jetaext],Ietaext-1);

 x_intpl_eta_ext_below = interp_find_poi(eta[Ietaext-1][Jetaext-1],
                                         eta[Ietaext][Jetaext-1],
                                         eta[Ietaext+1][Jetaext-1],Ietaext-1);

 /* Get their horizontal grid location (grid coordinate, not x value) */
 /*  -> they should be identical                                      */

 ipl_Ietaext_above = interp_find_poi_index(eta[Ietaext-1][Jetaext+1],
                                         eta[Ietaext][Jetaext+1],
                                         eta[Ietaext+1][Jetaext+1],Ietaext-1);
                                                
 ipl_Ietaext = interp_find_poi_index(eta[Ietaext-1][Jetaext],
                                   eta[Ietaext][Jetaext],
                                   eta[Ietaext+1][Jetaext],Ietaext-1);

 ipl_Ietaext_below = interp_find_poi_index(eta[Ietaext-1][Jetaext-1],
                                         eta[Ietaext][Jetaext-1],
                                         eta[Ietaext+1][Jetaext-1],Ietaext-1);

 /* next interpolate in vertical */

 double ipl_eta_ext =  interp_find_poi(x_intpl_eta_ext_below,
                                            x_intpl_eta_ext,
                                              x_intpl_eta_ext_above,
                                                Jetaext-1);
 double ipl_Jetaext =  interp_find_poi_index(x_intpl_eta_ext_below,
                                            x_intpl_eta_ext,
                                              x_intpl_eta_ext_above,
                                                Jetaext-1);
 double ipl_dens_ext = at_i_j(ipl_Ietaext, ipl_Jetaext, I, J, (double*)D[0]);
 dens_ext = D[Ietaext][Jetaext];


 printf ("       grid values: eta_ext = %1.5e, Ietaext = %d, Jetaext = %d\n",
           eta[Ietaext][Jetaext], Ietaext, Jetaext);
 printf ("interpolate values: eta_ext = %1.5e, ipl_Ietaext = %1.5e, ipl_Jetaext = %1.5e\n",
           ipl_eta_ext, ipl_Ietaext, ipl_Jetaext);


 f1 = fopen ("solitary_wave_data", "a");
 fprintf(f1, "     eta_ext = %1.5e at (x,z) = (%1.4e,%1.4e)\n",
	 ipl_eta_ext, -0.5*domainwidth+(ipl_Ietaext+0.5)*dx, -H+(ipl_Jetaext+0.5)*dz);
 fprintf(f1, "     x values for interpolated eta_ext at three heights:\n");
 fprintf(f1,"         x(below,centre,above) =  %1.5e   %1.5e  %1.5e\n",
	       -0.5*domainwidth+(ipl_Ietaext_below+0.5)*dx, 
	       -0.5*domainwidth+(ipl_Ietaext+0.5)*dx,
	       -0.5*domainwidth+(ipl_Ietaext_above+0.5)*dx);
 fprintf (f1, "     density along extremal = %1.5e\n", ipl_dens_ext);
 fclose (f1);

 f1 = fopen ("etaextvals", "a");
 fprintf (f1, "%1.6e\n", ipl_eta_ext);
 fclose (f1);

 f1 = fopen ("Usur_WaveCentre_vals", "a");
 fprintf (f1, "%1.6e\n", at_i_j (ipl_Ietaext, (double)J-0.5, I, J, (double*)U[0]));
 fclose (f1);

/*******************************************************************/
/*** 1b) find isopycnal which undergoes maximum displacement.    ***/
/*******************************************************************/

 sprintf (file, "isopycnal_profile_A%g", A);
 f1 = fopen (file, "w");
//sprintf (file, "isopycnal_profile_A%g_data", A);
//f2 = fopen (file, "w");
 int_half_width = 0.0;
 for(i=0; i<I; i++) {
    j = J-1;
    while(D[i][j] < ipl_dens_ext && j >= 1) j--;
       if(D[i][j] >= ipl_dens_ext) {
       deltaz = (D[i][j]-ipl_dens_ext)/(D[i][j]-D[i][j+1])*dz;
       height = zval(j,dz,H) + deltaz;

       if(i==0) eta_bnd = height;
       else int_half_width += height-eta_bnd;
       fprintf (f1, "%1.6e %1.6e\n", -0.5*domainwidth+xval(i,dx), height);
      //      fprintf (f2, "%d, %d, %1.6e,%1.6e %1.6e\n", i, j, ipl_dens_ext, ipl_D0, ipl_D1-ipl_D0);
      }
  }
 fclose(f1);

 int_half_width *= dx;
 int_half_width *= 0.5/ipl_eta_ext;
 f1 = fopen("integrated_halfwidth_vals","a");
 // fprintf(f1,"%1.6e\n",fabs(0.5/ipl_eta_ext*int_half_width));
 fprintf(f1,"%1.6e\n",fabs(int_half_width));
 fclose(f1);
//fclose (f2);


/*******************************************************************/
/*** 1c) find isopycnal which undergoes maximum displacement.    ***/
/*******************************************************************/
#if 0
sprintf (file, "isopycnal_profile_D24p5_A%g", A);
f1 = fopen (file, "w");
sprintf (file, "isopycnal_profile_D24p5_A%g_data", A);
f2 = fopen (file, "w");
for (i=0; i<I; i++) {
	j = J-1;
	while (D[i][j] < 0.0245 && j >= 1) j--;
	if(D[i][j] >= 0.0245){
		double ipl_D0, ipl_D1;
		/* Interpolate between grid points, to account for staggered grid */
		ipl_D0 = at_i_j ((double)i-0.5, (double)j-0.5, I, J, (double*)D][0]);
		ipl_D1 = at_i_j ((double)i-0.5, (double)j+0.5, I, J, (double*)D[0]);

		height = zval(j,dz,H) + (0.0245-ipl_D0)/(ipl_D1-ipl_D0)*dz;
		fprintf (f1, "%1.6e %1.6e\n", -0.5*domainwidth + xval(i,dx), height);
		fprintf (f2, "%d, %d, %1.6e,%1.6e %1.6e\n", i, j, 0.0245, ipl_D0, ipl_D1-ipl_D0);
	}
}
fclose (f1);
fclose (f2);
#endif

/*******************************************************************/
/*** 2) find extreme surface velocity perturbation and compute   ***/
/***    the half length based on surface velocity.               ***/
/*******************************************************************/

/* ipl_ indicates an inter/extrapolated value */

double U_s[I];
for (i=0; i<I; i++) U_s[i] = at_i_j ((double)i, (double)J-0.5, I, J, (double*) U[0]);
//for (i=0; i<I; i++) U_s[i] = 1.5*U[i][J-1] - 0.5*U[i][J-2];
double ipl_m = ((double)I-1.0)/2.0;
m = (int) ipl_m;
double ipl_max_surU = at_i_j (ipl_m, (double)J-0.5, I, J, (double*)U[0]);
//double ipl_max_surU = (I%2 == 0) ? 9.0/8.0 * U_s[I/2-1] - 1.0/8.0 * U_s[I/2-2] : U_s [(I-1)/2];

printf ("imax = %1.3e,  max_surU = %1.6e\n", ipl_m, ipl_max_surU);
f1 = fopen ("solitary_wave_data", "a");
fprintf (f1, "     extreme surface velocity U[m][J]= %1.6e at x = %1.6e\n",
	 at_i_j((double)m, (double)J-0.5, I, J, (double*) U[0]), 
         xval(m, dx) - 0.5*domainwidth);
fprintf (f1, "     Usurext (U[m][J]) = %1.6e\n", 
         at_i_j((double)m, (double)J-0.5, I, J, (double*) U[0]));
fprintf (f1, "     Ubotext (U[m][0]) = %1.6e\n", at_i_j((double)m, -0.5, I, J, (double*)U[0]));
fclose (f1);

i = (int)ipl_m + 1;
double U_ = fabs (U_s[i] - U_s[0]);
while (U_ > 0.5*fabs(ipl_max_surU-U_s[0])) {
	i--;
	U_ = fabs (U_s[i] - U_s[0]);
}
double ipl_i = interp_right_intersect_index (0.5*ipl_max_surU, U_s[i], U_s[i+1], U_s[i+2], i);
//double ipl_i = (double)i + (0.5*ipl_max_surU - U_s[i])/(U_s[i+1]-U_s[i]);

/*half_length = (ipl_m-ipl_i)*dx + dx*(at_i_j ((double)ipl_i, (double)J-0.5, I, J, (double*) U)
                                     - 0.5 * at_i_j ((double)ipl_m, (double)J-0.5, I, J, (double*) U)
                                     - 0.5 * at_i_j (-0.5, (double)J-0.5, I, J, (double*) U));
*/
half_length_U = (ipl_m - ipl_i) * dx;

f1 = fopen ("solitary_wave_data", "a");
fprintf (f1, "     half length based on surface current = %1.5e\n",half_length_U);
fclose (f1);
f1 = fopen ("sur_halfwidth_vals", "a");
fprintf (f1, "%1.6e\n", half_length_U);
fclose (f1);

/***********************************************/
/*** 3) calculate the half-length based on  ****/
/***   isopycnal with maximum displacement. ****/
/***********************************************/

i = Ietaext;
double prev_temp;
prev_temp = temp = fabs(ipl_eta_ext);
// find location where eta(D=Dext) first drops below half ipl_eta_ext
while(temp > 0.5*fabs(ipl_eta_ext)) {  
   // find j just above or on point where D = Dext
   j = 0;
   while(D[i][j] > ipl_dens_ext) j++;   
   prev_temp = temp;
   ratio = (ipl_dens_ext-D[i][j-1])/(D[i][j]-D[i][j-1]);
   temp = fabs( eta[i][j-1] + ratio*(eta[i][j]-eta[i][j-1]) );
   i++;                                        
}

// Now have prev_temp > 0.5*fabs(ipl_eta_ext) >= temp. 
// Note prev_temp and temp correspond to values at i-2 and i-1.
 i--;
double ipl_I_lambda = i-1 + (0.5*fabs(ipl_eta_ext)-prev_temp)/(temp-prev_temp);

half_length = (ipl_I_lambda-ipl_Ietaext)*dx;
printf ("half_length based on density contour = %1.4e\n", half_length);
f1 = fopen ("solitary_wave_data", "a");
fprintf (f1, "     half_length based on density contour = %1.5e\n", half_length);
fclose (f1);
f1 = fopen ("ext_iso_halfheight_halfwidth_vals", "a");
fprintf (f1, "%1.6e\n", half_length);
fclose (f1);
f1 = fopen ("lambda_vals", "a");
fprintf (f1, "%1.6e\n", 2*half_length);
fclose (f1);

/********************************************************/
/****  4) Calculate energy and mass anomaly in wave  ****/
/********************************************************/
KE = KEpert = PE = MASS = APE = 0.0;
for(i=0; i<I; i++) {
   for(j=0; j<J; j++) {
      MASS += D[i][j] - D[0][j];
      KE += U[i][j]*U[i][j]+W[i][j]*W[i][j];
      if(UBACKGROUND) Ubk = Uback(zval(j,dz,H));
      else Ubk = 0.0;
      KEpert += U[i][j]*U[i][j]+W[i][j]*W[i][j] - Ubk*Ubk ;
      PE += (D[i][j]-density(zval(j,dz,H)))*(zval(j,dz,H));
      APE += APEdens[i][j];
   }
}

MASS *= dz*dx;
KE *= 0.5*dz*dx;
KEpert *= 0.5*dz*dx;
PE *= g*dz*dx;
APE *= dz*dx;
printf ("KE=%1.5e, PE=%1.5e, APE = %1.5e\n", KE, PE, APE);
f1 = fopen ("energyvals", "a");
fprintf (f1, "%1.6e\n", KE + PE);
fclose (f1);
f1 = fopen ("KEvals", "a");
fprintf (f1, "%1.6e\n", KE);
fclose (f1);
f1 = fopen ("KEpert_vals", "a");
fprintf (f1, "%1.6e\n", KEpert);
fclose (f1);
f1 = fopen ("PEvals", "a");
fprintf (f1, "%1.6e\n", PE);
fclose (f1);
f1 = fopen ("intAPEdens_vals", "a");
fprintf (f1, "%1.6e\n", APE);
fclose (f1);
f1 = fopen ("APEvals", "a");
fprintf (f1, "%1.6e\n", A);
fclose (f1);
f1 = fopen ("massvals", "a");
fprintf (f1, "%1.6e\n", MASS);
fclose (f1);

f1 = fopen ("solitary_wave_data", "a");
fprintf (f1, "\n     perturbation KE = %1.6e\n", KEpert);
fprintf (f1, "     perturbation PE = %1.6e\n", PE);
fprintf (f1, "     integrated APEdens = %1.6e\n", APE);
fprintf (f1, "     mass perturbation = %1.6e\n\n", MASS);
fclose (f1);


/***********************************************/
/****  5) Horizontal profiles at mid-depth  ****/
/***********************************************/
if (D_OUT) {
	sprintf (file, "D%d", J/2);
	f1 = fopen (file, "w");
	for (i=0; i<I; i++) fprintf (f1, "%1.6e\n",  D[i][J/2]-D[0][J/2]);
	fclose (f1);
}

/***********************************************/
/****  6) Horizontal profiles at 0.75*H     ****/
/***********************************************/

if (D_OUT) {
	sprintf (file, "D%d", 3*J/4);
	f1 = fopen (file, "w");
	for (i=0; i<I; i++) fprintf (f1, "%1.6e\n", D[i][3*J/4]-D[0][3*J/4]);
	fclose (f1);
}
/***********************************************/
/****  7) Surface and bottom profiles       ****/
/***********************************************/

sprintf (file, "Usur_A%g", A);
f1 = fopen (file, "w");
for (i=0; i<I; i++) fprintf (f1, "%1.6e\n", at_i_j ((double)i, (double)J-0.5, I, J, (double*) U[0]));
fclose (f1);
sprintf (file, "Usur_vs_x_A%g", A);
f1 = fopen (file, "w");
 for (i=0; i<I; i++) fprintf (f1, "%1.6e %1.6e\n", xval(i,dx) - domainwidth/2.0,
		at_i_j ((double)i, (double)J-0.5, I, J, (double*)U[0]));
fclose (f1);
if (UBACKGROUND) {
	sprintf (file, "UsurPert_vs_x_A%g", A);
	f1 = fopen (file, "w");
	for (i=0; i<I; i++) fprintf (f1, "%1.6e %1.6e\n", xval(i,dx) - domainwidth/2.0,
			at_i_j ((double)i, (double)J-0.5, I, J, (double*)U[0]) - at_i_j(-0.0, (double)J-0.5, I, J, (double*) U[0]) );
	fclose (f1);
}
sprintf (file, "Ubot_A%g", A);
f1 = fopen (file, "w");
for (i=0; i<I; i++) fprintf (f1, "%1.6e\n", at_i_j ((double)i, -0.5, I, J, (double*) U[0]));
fclose (f1);
sprintf (file, "Ubot_vs_x_A%g", A);
f1 = fopen (file, "w");
 for (i=0; i<I; i++) fprintf (f1, "%1.6e %1.6e\n", xval(i,dx) - domainwidth/2.0,
		at_i_j ((double)i, -0.5, I, J, (double*) U[0]));
fclose (f1);
if (UBACKGROUND) {
	sprintf (file, "UbotPert_vs_x_A%g", A);
	f1 = fopen (file, "w");
	for (i=0; i<I; i++) fprintf (f1, "%1.6e %1.6e\n", xval(i,dx) - domainwidth/2.0,
			at_i_j ((double)i, -0.5, I, J, (double*) U[0]) - at_i_j (0.0, -0.5, I, J, (double*) U[0]));
	fclose (f1);
}
if (TEST_OUT) {
	/* This outputs a horizontal and vertical profiles for W for testing purposes.
	   Both profiles pass through the point of maximum W (iMaxW, jMaxW)
	 */
	/* Find point of maximum W */
	double maxW = 0.0;
	int iMaxW = 0;
	int jMaxW = 0;
	for (i=0; i<I; i++) {
		for (j=0; j<J; j++) {
			if (W[i][j] > maxW) {
				maxW = W[i][j];
				iMaxW = i;
				jMaxW = j;
			}
		}
	}

	/* Vertical profile from j = 0 to j = J, with i = iMaxW */
	sprintf (file, "W_vertical_A%g", A);
	f1 = fopen (file, "w");

	for (j=0; j<J; j++) fprintf (f1, "%1.6e\n", W[iMaxW][j]);
	fclose (f1);

	/* Horizontal profile from i = 0 to i = I, with j = jMaxW */
	sprintf (file, "W_horizontal_A%g", A);
	f1 = fopen (file, "w");

	for (i=0; i<I; i++) fprintf (f1, "%1.6e\n", W[i][jMaxW]);
	fclose (f1);

}

/******************************************************/
/****  8) x values for horizontal profiles 0.75*H  ****/
/******************************************************/

sprintf (file, "x_A%g", A);
f1 = fopen (file, "w");
 for (i=0; i<I; i++) fprintf (f1, "%1.6e\n", xval(i,dx));
fclose (f1);

/**********************************************************/
/***  9) find minimum and maximum horizontal velocities ***/
/**********************************************************/

maxU = 0.0;
minU = 0.0;
for (i=0; i<I; i++) {
	for (j=0; j<J; j++) {
		if (U[i][j] > maxU) {
			maxU = U[i][j];
			iUmax = i;
			jUmax = j;
		}
		if (U[i][j] < minU) {
			minU = U[i][j];
			iUmin = i;
			jUmin = j;
		}
	}
}
f1 = fopen ("solitary_wave_data", "a");
 fprintf (f1, "     max(U) =  %1.6e at z = %1.6e\n", maxU, zval(jUmax, dz, H));
 fprintf (f1, "     min(U) =  %1.6e at z = %1.6e\n", minU, zval(jUmin, dz, H));
fprintf (f1, "     maxU/phspd = %1.6e\n", maxU/phspd);
fclose (f1);
f1 = fopen ("maxUvals", "a");
fprintf (f1, "%1.6e\n", maxU);
fclose (f1);
f1 = fopen ("maxUvals_with_ij", "a");
fprintf (f1, "%1.6e, (%d,%d), (%1.6e,%1.6e)\n",
	 maxU, iUmax, jUmax, xval(iUmax, dx), zval(jUmax, dz, H));
fclose (f1);
f1 = fopen ("minUvals", "a");
fprintf (f1, "%1.6e\n", minU);
fclose (f1);
f1 = fopen ("minUvals_with_ij", "a");
fprintf (f1, "%1.6e, (%d,%d), (%1.6e,%1.6e)\n", minU, iUmin, jUmin,
	 xval(iUmin, dx), zval(jUmin, dz, H));
fclose (f1);

/****************************************************/
/***  9) Transport distance at surface and bottom ***/
/****************************************************/

surtrans = 0.0;
bottrans = 0.0;
for (i=0; i<I; i++) {
	double Usur_extrap_tmp = at_i_j ((double)i, (double)J-0.5, I, J, (double*) U[0]);
	double Ubot_extrap_tmp = at_i_j ((double)i, -0.5, I, J, (double*) U[0]);

	surtrans += Usur_extrap_tmp/(phspd - Usur_extrap_tmp);
	bottrans += Ubot_extrap_tmp/(phspd - Ubot_extrap_tmp);
}
surtrans *= dx;
bottrans *= dx;

f1 = fopen ("solitary_wave_data", "a");
fprintf (f1, "      surface transport distance = %1.6e\n", surtrans);
fprintf (f1, "     bottom transport distance = %1.6e\n", bottrans);
fclose (f1);


f1 = fopen ("surf_transport_vals", "a");
fprintf (f1, "%1.6e\n", surtrans);
fclose (f1);
f1 = fopen ("bottom_transport_vals", "a");
fprintf (f1, "%1.6e\n", bottrans);
fclose (f1);

f1 =fopen ("cvals", "a");
fprintf (f1, "%1.6e\n", phspd);
fclose (f1);

/******************************************************************************/
/**** 10) Store vertical profiles. Find minimum Ri for Dlower < D < Dupper ****/
/******************************************************************************/


 sprintf (file, "Uvert_A%g", A);
 f1 = fopen (file, "w");
 for (j=0; j<J; j++) fprintf (f1, "%1.6e\n", U[Ietaext][j]);
 fclose (f1);

 sprintf (file, "eta_WaveCentre_vs_z_A%g", A);
 f1 = fopen (file, "w");
 for (j=0; j<J; j++) fprintf (f1, "%1.6e %1.6e\n", eta[Ietaext][j], zval(j,dz,H));
 fclose (f1);

 sprintf (file, "z_minus_eta_WaveCentre_vs_z_A%g", A);
 f1 = fopen (file, "w");
 for (j=0; j<J; j++) fprintf (f1, "%1.6e %1.6e\n", zval(j,dz,H) - eta[Ietaext][j],
		zval(j,dz,H));
 fclose (f1);

 sprintf (file, "Uvert_vs_z_A%g", A);
 f1 = fopen (file, "w");
 for (j=0; j<J; j++) fprintf (f1, "%1.6e %1.6e\n", U[Ietaext][j], zval(j,dz,H));
 fclose (f1);

 if (UIEG_OUT) {
	sprintf (file, "U_ieq1_vs_z_A%g", A);
	f1 = fopen (file, "w");
	for (j=0; j<J; j++) fprintf (f1, "%1.6e %1.6e\n", U[1][j], zval(j,dz,H));
	fclose (f1);
	sprintf (file, "U_ieq2_vs_z_A%g", A);
	f1 = fopen (file, "w");
	for (j=0; j<J; j++) fprintf (f1, "%1.6e %1.6e\n", U[2][j], zval(j,dz,H));
	fclose (f1);

	sprintf (file, "U_ieqI_vs_z_A%g", A);
	f1 = fopen (file, "w");
	for (j=0; j<J; j++) fprintf (f1, "%1.6e %1.6e\n", U[I-1][j], zval(j,dz,H));
	fclose (f1);
	sprintf (file, "U_ieqIm1_vs_z_A%g", A);
	f1 = fopen (file, "w");
	for (j=0; j<J; j++) fprintf (f1, "%1.6e %1.6e\n", U[I-2][j], zval(j,dz,H));
	fclose (f1);
}
 sprintf (file, "Dvert_vs_z_A%g", A);
 f1 = fopen (file, "w");
 for (j=0; j<J; j++) fprintf (f1, "%1.6e %1.6e\n", D[Ietaext][j], zval(j,dz,H));
 fclose (f1);

 Dupper = D[Ietaext][0]-0.05*(D[Ietaext][0]-D[Ietaext][J-1]);
 Dlower = D[Ietaext][0]-0.95*(D[Ietaext][0]-D[Ietaext][J-1]);

 sprintf (file, "Ri_vert_A%g", A);
 f1 = fopen (file, "w");
 sprintf (file, "NN_vert_A%g", A);
 f2 = fopen (file, "w");
 sprintf (file, "Uz_vert_A%g", A);
 f3 = fopen (file, "w");

 minRi = 1.0e3;
 double Uzsq_max = -1.0;
 double Uzsq_max_z = 1.0;
 double U_at_max_shear;
#if 0
 for(j=2;j<J-1;j++){
     Uzsq = U[Ietaext][j+1]-U[Ietaext][j-1];
     Uzsq = *= Uzsq;
     if(Uzsq > Uzsq_max){
        Uzsq_max = Uzsq;
        U_at_max_shear = U[Ietaext][j];
        Uzsq_max_z = zval(j,dz,H);
      }
     Ri = -2.0*dz*g*(D[Ietaext][j+1]-D[Ietaext][j-1]);
     if(fabs(Uzsq) < 1.0e-14) Ri = 1000.0;
     else Ri = Ri/Uzsq;
     fprintf (f1, "%1.6e %1.6e\n", Ri, zval(j,dz,H));
     if(D[Ietaext][j]>Dupper ? 0 : D[Ietaext][j]<Dlower ? 0 : 1){
         if(Ri<minRi){
	     minRi = Ri;
             minRi_z = zval(j,dz,H);
	 }
     }
 }
#else
 for(j=2;j<=J-1;j++){
     Uz = 0.5*(U[Ietaext][j+1]-U[Ietaext][j-1])/dz;
     Uzsq = Uz*Uz;
     if(Uzsq > Uzsq_max){
        Uzsq_max = Uzsq;
        U_at_max_shear = U[Ietaext][j];
        Uzsq_max_z = zval(j,dz,H);
      }
     NN = -0.5*g*(D[Ietaext][j+1]-D[Ietaext][j-1])/dz;
     if(fabs(Uzsq) < 1.0e-14) Ri = 1000.0;
     else Ri = NN/(Uz*Uz);
     fprintf (f1, "%1.6e %1.6e\n", Ri, zval(j,dz,H));
     fprintf (f2, "%1.6e %1.6e\n", NN, zval(j,dz,H));
     fprintf (f3, "%1.6e %1.6e\n", Uz, zval(j,dz,H));
     if(D[Ietaext][j]>Dupper ? 0 : D[Ietaext][j]<Dlower ? 0 : 1){
         if(Ri<minRi){
	     minRi = Ri;
             minRi_z = zval(j,dz,H);
	 }
     }
 }
#endif

 fclose (f1);
 fclose (f2);
 fclose (f3);

 f1 = fopen("Rimin_vals","a");
 fprintf(f1,"%1.8e\n",minRi);
 fclose(f1);

 f1 = fopen("Rimin_z_vals","a");
 fprintf(f1,"%1.8e\n",minRi_z);
 fclose(f1);

 f1 = fopen("Uz_max_vals","a");
 fprintf(f1,"%1.8e\n",0.5*sqrt(Uzsq_max)/dz);
 fclose(f1);

 f1 = fopen("Uz_max_z_vals","a");
 fprintf(f1,"%1.8e\n",Uzsq_max_z);
 fclose(f1);

 f1 = fopen("U_at_max_shear_vals","a");
 fprintf(f1,"%1.8e\n",U_at_max_shear);
 fclose(f1);

 double Rimin_vals[I];
 double Rimin_z_vals[I];
 for(i=1;i<I;i++){
    minRi = 1.0e3;
    for(j=2;j<J;j++){
       Uzsq = U[i][j+1]-U[i][j-1];
       Uzsq *= Uzsq;
       Ri = -2.0*dz*g*(D[i][j+1]-D[i][j-1]);
       if(fabs(Uzsq) < 1.0e-14) Ri = 1000.0;
       else Ri = Ri/Uzsq;
       //       fprintf (f1, "%1.6e %1.6e\n", Ri, zval(j,dz,H));
       if(D[i][j]>Dupper ? 0 : D[i][j]<Dlower ? 0 : 1){
           if(Ri<minRi){
  	     minRi = Ri;
             minRi_z = zval(j,dz,H);
	   }
       }
    }
    Rimin_vals[i] = minRi;
    Rimin_z_vals[i] = minRi_z;
 }
 sprintf (file, "Ri_min_vs_x_A%g", A);
 f1 = fopen (file, "w");
 sprintf (file, "Ri_min_z_vs_z_A%g", A);
 f2 = fopen (file, "w");
 int istart = 0;
 int iend = 0;
 int istart0p2 = 0;
 int iend0p2 = 0;
 for(i=1;i<I;i++){
   fprintf(f1,"%1.6e  %1.6e\n", -0.5*domainwidth+xval(i,dx),Rimin_vals[i]);
   fprintf(f2,"%1.6e  %1.6e\n", -0.5*domainwidth+xval(i,dx),Rimin_z_vals[i]);
   if(istart==0 && Rimin_vals[i]<0.25) istart = i;
   if(istart != 0 && iend==0 && Rimin_vals[i]>0.25) iend = i;
   if(istart0p2==0 && Rimin_vals[i]<0.2) istart0p2 = i;
   if(istart0p2 != 0 && iend0p2==0 && Rimin_vals[i]>0.2) iend0p2 = i;
 }
 fclose(f1);
 fclose(f2);

 // Now find length of domain with Ri < 0.25
 xleft=0.0;
 xright=0.0;
 if(istart*iend != 0){
    printf("Rimin_vals[istart-1] = %1.5e\n",Rimin_vals[istart-1]);
    printf("Rimin_vals[istart] = %1.5e\n",Rimin_vals[istart]);
    printf("Rimin_vals[iend] = %1.5e\n",Rimin_vals[iend]);
    printf("Rimin_vals[iend+1] = %1.5e\n",Rimin_vals[iend+1]);
    ratio = (Rimin_vals[istart-1]-0.25)/(Rimin_vals[istart-1]-Rimin_vals[istart]);
    xleft = xval(istart-1,dx) + ratio*dx;
    ratio = (Rimin_vals[iend]-0.25)/(Rimin_vals[iend]-Rimin_vals[iend+1]);
    xright = xval(iend,dx) + ratio*dx;
 }
 f1 =fopen ("Lx_vals", "a");
 fprintf (f1, "%1.6e\n", xright-xleft);
 fclose (f1);
 f1 =fopen ("Lx_over_lambda_vals", "a");
 fprintf (f1, "%1.6e\n", (xright-xleft)/(2.0*half_length));
 fclose (f1);

 // Now find length of domain with Ri < 0.2
 xleft=0.0;
 xright=0.0;
 if(istart0p2*iend0p2 != 0){
    printf("Rimin_vals[istart0p2-1] = %1.5e\n",Rimin_vals[istart0p2-1]);
    printf("Rimin_vals[istart0p2] = %1.5e\n",Rimin_vals[istart0p2]);
    printf("Rimin_vals[iend0p2] = %1.5e\n",Rimin_vals[iend0p2]);
    printf("Rimin_vals[iendp2+1] = %1.5e\n",Rimin_vals[iend0p2+1]);
    ratio = (Rimin_vals[istart0p2-1]-0.2)/(Rimin_vals[istart0p2-1]-Rimin_vals[istart0p2]);
    xleft = xval(istart0p2-1,dx) + ratio*dx;
    ratio = (Rimin_vals[iend0p2]-0.2)/(Rimin_vals[iend0p2]-Rimin_vals[iend0p2+1]);
    xright = xval(iend0p2,dx) + ratio*dx;
 }
 f1 =fopen ("Lx_0p2_vals", "a");
 fprintf (f1, "%1.6e\n", xright-xleft);
 fclose (f1);


/********************************************************/
/***  11) U = 0.0 contour and density values along it ***/
/********************************************************/

 sprintf	(file, "zeroU_A%g", A);
 f1 = fopen (file, "w");
 sprintf (file, "zeroU_den_A%g", A);
 f2 = fopen (file, "w");
 sprintf (file, "zeroU_x_A%g", A);
 f3 = fopen (file, "w");

 for (i=0; i<I; i++) {
	j = 0;
	while (U[i][j]*U[i][j+1] > 0.0) j++;
	temp = zval(j,dz,H) - dz*U[i][j]/(U[i][j+1]-U[i][j]);
	fprintf (f1, "%1.6e\n", temp);
	fprintf (f2, "%1.6e\n", D[i][j] + (temp/dz-j)*(D[i][j+1]-D[i][j]));
	fprintf (f3, "%1.6e\n", xval(i,dx));
 }
 fclose (f1); fclose (f2); fclose (f3);

 // Find z and U at centre of pycnocline (D=1)

 double Dcentre = 1.0;
 double dD = fabs(D[Ietaext][1]-Dcentre);
 for(j=2;j<J;j++){
   if(fabs(D[Ietaext][j]-Dcentre) < dD){
      dD = fabs(D[Ietaext][j]-Dcentre);
      i = j;
   }
 }


 double zcentre=zval(i,dz,H);
 double Ucentre = U[Ietaext][i];

 if(D[Ietaext][i] > Dcentre){
   zcentre += (Dcentre-D[Ietaext][i])/(D[Ietaext][i+1]-D[Ietaext][i])*dz;
   ratio = (zcentre-zval(i,dz,H))/dz;
   Ucentre = U[Ietaext][i] + ratio*(U[Ietaext][i+1]-U[Ietaext][i]);
 }
 if(D[Ietaext][i] < Dcentre){
   zcentre += (Dcentre-D[Ietaext][i])/(D[Ietaext][i]-D[Ietaext][i-1])*dz;
   ratio = (zcentre-zval(i,dz,H))/dz;
   Ucentre = U[Ietaext][i] - ratio*(U[Ietaext][i-1]-U[Ietaext][i]);
 }

 f1 = fopen("U_at_pycnocline_centre_vals","a");
 fprintf(f1,"%1.8e\n",Ucentre);
 fclose(f1);

 f1 = fopen("z_at_pycnocline_centre_vals","a");
 fprintf(f1,"%1.8e\n",zcentre);
 fclose(f1);

/******************************************/
/***  12) Check for presence of a core  ***/
/******************************************/

TrappedCore_Flag = 0; /** off, there is no trapped core **/
Amp_Core = 0.0;
if(ipl_eta_ext < 0.0) {
   for(i=0; i<I; i++) { 
      Core_Bnd = 0; /** on => already stored core boundary for current x value **/
      for(j=0; j<J; j++) {
         if(zval(j,dz,H) - eta[i][j] > 0.0) {
 	    if(TrappedCore_Flag == 0) { 
		iCore_start = i;
		sprintf (file, "core_region_A%g", A);
		f1 = fopen (file, "w");
		sprintf (file, "core_boundary_A%g", A);        
		f2 = fopen (file, "w");
		TrappedCore_Flag = 1; /** turn on => there is a trapped core **/
	    }
	  iCore_end = i;
 	  fprintf (f1, "%1.6e, %1.6e\n", -0.5*domainwidth + xval(i,dx), zval(j,dz,H));
	  if(Core_Bnd == 0){ /** first j value for this i with j*dz-H-eta > 0 **/
	        Core_Bnd = 1;
	        temp_Amp_Core =  -zval(j-1, dz, H)
		                    + (zval(j-1, dz, H)-eta[i][j-1])*dz
                                              /(dz + eta[i][j-1]-eta[i][j]);
 	        if (temp_Amp_Core >= Amp_Core) Amp_Core = temp_Amp_Core;
		fprintf (f2, "%1.6e %1.6e\n", -0.5*domainwidth + xval(i,dx), -temp_Amp_Core);
	  }
	 }
      }
   }
 }
 else{
    for(i=0; i<I; i++) { 
	Core_Bnd = 0; /** on => already stored core boundary for current x value **/
	for(j=J-1; j>=0; j--) {
	  if(zval(j,dz,H)+H-eta[i][j] < 0.0) {
	      if(TrappedCore_Flag == 0) { 
		iCore_start = i;
		sprintf (file, "core_region_A%g", A);
		f1 = fopen (file, "w");
		sprintf (file, "core_boundary_A%g", A);        
		f2 = fopen (file, "w");
		TrappedCore_Flag = 1;  /** turn on => there is a trapped core **/
	      }
	      iCore_end = i;
	      fprintf (f1, "%1.6e, %1.6e\n", -0.5*domainwidth + xval(i,dx),
                                               zval(j,dz,H));

	      if(Core_Bnd == 0) { /** first j value for this i with j*dz-H-eta < -H **/
		Core_Bnd = 1;
		temp_Amp_Core =  zval(j,dz,H) + H  
 		                   + (zval(j,dz,H)+H-eta[i][j])*dz
                                      /(dz - eta[i][j+1]+eta[i][j]);
		if (temp_Amp_Core >= Amp_Core) Amp_Core = temp_Amp_Core;
		fprintf (f2, "%1.6e %1.6e\n", -0.5*domainwidth + xval(i,dx),
                                               -H+temp_Amp_Core);
	      }
	   }
	}
    }
 }
if (TrappedCore_Flag==1 ) {
	fclose (f1);
	fclose (f2);
	f2 = fopen ("solitary_wave_data", "a");
	fprintf (f2, "     CORE: amplitude = %1.6e,  width = %1.6e\n", Amp_Core,
			(iCore_end-iCore_start)*dx);
	fclose (f2);
	f2 = fopen ("CoreAmp_vs_eta", "a");
	fprintf (f2, "%1.6e %1.6e\n", ipl_eta_ext, Amp_Core);
	fclose (f2);
}


/************************************************************/
/***  13) Particle tracking: computed in reference frame  ***/
/***      fixed with wave. x ranges from 0 to domainwidth ***/
/***      and wave centred at domainwidth/2.0             ***/
/************************************************************/

if (PARTICLE_TRACKING) { 
	if (maxU >= phspd) {
		printf ("Wave has overturned: not doing particle tracking\n");
	} else {
		xinit = 0.99*domainwidth; 
		dt = 0.5*dx/phspd; 
		sprintf (file, "particle_advection_distances%g", A);  /*** clear file  ***/
		f1 = fopen (file, "w");
		fclose (f1);
		dz_part = H/(NUM_PART-1);
		for (i=1; i<=NUM_PART; i++) {
			partpos[0] = xinit;
			partpos[1] = -H + (i-1)*dz_part;
			time = 0.0;
			sprintf (file, "particle_x%d", i);
			f1 = fopen (file, "w");
			sprintf (file, "particle_z%d", i);
			f2 = fopen (file, "w");
			sprintf (file, "particle_t%d", i);
			f3 = fopen (file, "w");
			fprintf (f1, "%1.6e\n", partpos[0] + phspd*time - xinit);  
			fprintf (f2, "%1.6e\n", partpos[1]);
			fprintf (f3, "%1.6e\n", time);
			while (partpos[0] > 2*dx) {
				odeint (time, time+dt, dt, hmin, dt, eps, partpos, 2, partvel);
				time += dt;
				/*** store particle position, correcting to reference ***/
				/*** frame fixed with fluid at infinity. Store such   ***/
				/*** that initial position is at x = 0                ***/
				fprintf (f1, "%1.6e\n", partpos[0] + phspd*time - xinit);  
				fprintf (f2, "%1.6e\n", partpos[1]);
				fprintf (f3, "%1.6e\n", time);
			}
			fclose (f1); fclose(f2); fclose(f3);
			sprintf (file, "particle_advection_distances%g", A);
			f1 = fopen (file, "a");
			fprintf (f1, "%1.5e  %1.5e\n", -H + (i-1)*dz_part, partpos[0]+phspd*time-xinit);
			fclose (f1);
		}
	}
 }

f1 = fopen ("solitary_wave_data", "a");
fprintf (f1, "\n------------------------------------------------------------------\n\n");
fclose (f1);

}

/***********************************************************************************/
/***********************************************************************************/
/***********************************************************************************/

void partvel (double t, double *x, double *dxdt) {

 double Al, Ar;
 int i, j;

 i = (x[0]/dx - 0.5);
 j = ((x[1]+H)/dz - 0.5);
 /* printf("i = %d, j = %d\n",i,j); */
 if(j==0 || j == J-1) {
    dxdt[0] = U[i][j] + (x[0]-xval(i,dx))/dx*(U[i+1][j]-U[i][j]);
    dxdt[1] = 0.0;
 }
 else{
    Al = U[i][j] +   (x[1]-zval(j,dz,H))/dz*(U[i][j+1]-U[i][j]);
    Ar = U[i+1][j] + (x[1]-zval(j,dz,H))/dz*(U[i+1][j+1]-U[i+1][j]);
    dxdt[0] = Al +   (x[0]-xval(i,dx))/dx*(Ar-Al);
    Al = W[i][j] +   (x[1]-zval(j,dz,H))/dz*(W[i][j+1]-W[i][j]);
    Ar = W[i+1][j] + (x[1]-zval(j,dz,H))/dz*(W[i+1][j+1]-W[i+1][j]);
    dxdt[1] = Al +   (x[0]-xval(i,dx))/dx*(Ar-Al);
 }
 dxdt[0] -= phspd;   /**  add background flow  **/
}



/***********************************************************************************/
/*** Computes the available potential energy density at grid point (i,j) divided ***/
/*** by g that is, E_p/g where E_p is the expression used by Sheppard (1993) and ***/
/*** Scotti et al 2006                                                           ***/
/***                                                                             ***/
/***     E_p = g*int_z^(z-eta){rhobar(s)-rho}ds                                  ***/
/***                                                                             ***/
/***  where rhobar(z) is the reference background density (taken as the initial  ***/
/***  density) and rho is the local density.                                     ***/
/***********************************************************************************/

double APEdens_val (double z, double eta, double rho)
{
    double t1,t2;
    int k,num = 200;
    double ddz;

    t2 = -eta*rho;

    /** integrate density(z) from z to z-eta **/
    ddz = -eta/num;
    t1 = 0.5*(density(z)+density(z-eta));
    /* Integral -- simpson's rule? */
    for (k=1; k<num; k++) t1 += density(z+k*ddz);
    t1 *= ddz;
    return (g*(t1-t2));
}


/***********************************************************************************/
/***********************************************************************************/

/* Calculates the z location of a point j, given dz and the length of the domain Lz. */
inline double zval(int j, double dz, double Lz){
  return((j+0.5)*dz-Lz);
}

/* Calculates the x location of a point i, given dx */
inline double xval(int i, double dx){
  return((i+0.5)*dx);
}
