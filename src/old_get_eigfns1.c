/***********************************************************************/
/***      This subroutine calculates the eigenfunctions              ***/
/***********************************************************************/
#include <stdio.h>
#include <math.h>
#include "header.h"

#define eps1 0.000001
#define eps2 0.000000001
#define eps3 0.000001
#define Nc 1000
#define max_mode 1
#define nvar 3

extern double dNN0, NN0(), Uback(), dUback(), d2Uback();

void linear_eigfn_derivs();
static double get_eigenvalue();
static void get_phi(), first_order_coeff_UBACK(), first_order_coeff_NOUBACK();
static void shoot();

double Cmax = 1.5;   // may is be changed below if below two-layer long wave speed
double Cmin = 0.1;   // this may need to be changed
int kmax=500;
static int vstep = 5000;

extern double density();
extern void score();
extern void odeint (double t1, double t2, double h, double hmin, double hmax, double epsilon,
             double *y, int dim, void (*derivs) (double, double*, double*));
extern double* alloc_vector (int n);
extern double** alloc_matrix (int n, int m);
extern void free_vector (double *p);
extern void free_matrix (double **p);
extern double H, clw,g;

static double *cc, *phi_dz, *phi10_dz, *phiH, *E, *dEdz;
static double Iden, I10, I01, I20;
static int i_max;
double alpha10, *zz, *phi, *phi10, *E10, *Z10;
double r10, r01, r20;

static double subphi, subphi2, dsubphi, dsubphi2, subz;

extern int UBACKGROUND;

void get_eigfns1()
{
int i;
FILE *f1;

phi =    alloc_vector (vstep);
phi_dz = alloc_vector (vstep);
phi10 =  alloc_vector (vstep);
E10 =    alloc_vector (vstep);
Z10 =    alloc_vector (vstep);
phi10_dz = alloc_vector (vstep);
E =      alloc_vector (vstep);
dEdz =   alloc_vector (vstep);
zz =     alloc_vector (vstep);
phiH =   alloc_vector (Nc);
cc =     alloc_vector (Nc);
for (i=0; i<vstep; ++i) zz[i] = (i)*H/(vstep-1)-H;
printf("zz[0] = %1.6e,  zz[1] = %1.6e,  zz[vstep-1] = %1.6e\n",
       zz[0], zz[1], zz[vstep-1]);

clw = get_eigenvalue();

get_phi (vstep);
f1 = fopen ("phi", "w");
for (i=0; i<vstep; ++i)  fprintf (f1, "%+1.4e %+1.4e\n", zz[i], phi[i]);
fclose (f1);
f1 = fopen ("phi_dz", "w");
for (i=0; i<vstep; ++i)  fprintf (f1, "%+1.4e %+1.4e\n", zz[i], phi_dz[i]);
fclose (f1);

printf ("back from get_phi\n");

if (UBACKGROUND) first_order_coeff_UBACK (vstep);
else first_order_coeff_NOUBACK (vstep);

printf ("back from first order coefficients\n");

}

/*****************************************************************/
/*****************************************************************/
static double get_eigenvalue ()
{
int i, mode;
double v, delv, f, Dc, *yy, dz;
double Cmax_TwoLayer;
FILE *f1;

yy = alloc_vector (nvar);
dz = H/kmax;
if (!UBACKGROUND){
	Cmax = 2.0;
	Cmax_TwoLayer = g*(density(-H) - density(0.0))*H/4.0;
	printf ("Cmax = %1.4e\n", Cmax);
	printf (" Maximum linear long wave speed for two layer fluid with same density difference is %1.6e\n", Cmax_TwoLayer);
	if (Cmax_TwoLayer > Cmax) {
		Cmax = 1.5*Cmax_TwoLayer;
		printf ("Changed Cmax to %1.6e\n", Cmax);
	}
}
Dc = (Cmax-Cmin)/(Nc-1);

for (i=0; i<Nc; i++) cc[i] = Cmin + (Nc - i)*Dc;
printf ("searching for roots of phi(H,c) = 0. c between %f and %f\n",
        cc[0], cc[Nc-1]);

for (i=0; i<Nc; i++) {
	if (cc[i]==0.0) phiH[i] = 0.0;
	else {
		yy[0] = 0.0;
		yy[1] = 1.0;
		yy[2] = cc[i];
		//odeint (yy, nvar, -H, 0.0, eps2, dz, 0.0, &nok, &nbad, linear_eigfn_derivs);
		odeint (-H, 0.0, dz, 0.0, 10.0, eps2, yy, nvar, linear_eigfn_derivs);
		phiH[i] = yy[0];
	}
}
f1 = fopen ("phiH_c", "w");
for (i=0; i<Nc; ++i) fprintf (f1, "%+1.4e %+1.4e %d\n", cc[i], phiH[i], i);
fclose (f1);

mode = 0;
for (i=0; i<Nc-1; i++) {
	if (phiH[i]*phiH[i+1] < 0.0) {
		++mode;
		v = cc[i];
		delv = cc[i+1] - cc[i];
		shoot (nvar, &v, &delv, -H, 0.0, eps2, dz, 0.0, &f, linear_eigfn_derivs);
		while (fabs(f) > eps1) {
			shoot (nvar, &v, &delv, -H, 0.0, eps2, dz, 0.0, &f, linear_eigfn_derivs);
		}
		if (mode >= max_mode) break;
	}
}
printf ("linear long-wave phase speed is c = %g\n\n", v);
free_vector (yy);
return (v);
}



/***************************************************************************/
/*****                 Linear vertical structure function              *****/
/***************************************************************************/

static void get_phi (int step) {
int i;
double dz, phi_max, *y;

y = alloc_vector (nvar);
dz = H/kmax;

y[0] = 0.0;
y[1] = 1.0;
y[2] = clw;
phi[0] = y[0];
phi_dz[0] = y[1];

for (i=1; i<step; ++i) {
//	odeint (y, nvar, zz[i-1], zz[i], eps2, dz, 0.0, &nok, &nbad, linear_eigfn_derivs); 
	odeint (zz[i-1], zz[i], dz, 0.0, 10.0, eps2, y, nvar, linear_eigfn_derivs);
	phi[i] = y[0];
	phi_dz[i] = y[1];
}
  
phi_max = 0.0;
for (i=0; i<step; ++i) {
	if (fabs (phi[i]) > phi_max) {
		i_max = i;
		phi_max = fabs (phi[i]);
	}
}
   
printf ("maximum value of phi, set to one, is at zz[%d] = %f\n\n", i_max, zz[i_max]);
for (i=0; i<step; ++i) {
	phi[i] = phi[i]/phi_max;
	phi_dz[i] = phi_dz[i]/phi_max;
}
  
free_vector (y);
}

/********************************************************************************/
/********************************************************************************/
static void shoot (int nn, double *v, double *delv, double x1, double x2,
                double eps, double h1, double hmin, double *f, void (*derivs)())

{
double *y, dfdv, dff;

y = alloc_vector (nn);
y[0] = 0.0;
y[1] = 1.0;
y[2] = *v;
odeint (x1, x2, h1, hmin, 10.0, eps, y, nn, derivs);
*f = y[0];
*v += *delv;
y[0] = 0.0;
y[1] = 1.0;
y[2] = *v;
odeint (x1, x2, h1, hmin, 10.0, eps, y, nn, derivs);
dff = y[0];
dfdv = (dff - *f)/(*delv);
dff = -dff/dfdv;
*delv = dff;
free_vector (y);
}



/***************************************************************************/
/***************************************************************************/
/*****                                                                 *****/
/*****       First-order nonlinear vertical structure function         *****/
/*****                                                                 *****/
/***************************************************************************/
/***************************************************************************/

/*************************************************************************/
/*************************************************************************/
void linear_eigfn_derivs (double x, double *y, double *dydx) {
double t1;
dydx[0] = y[1];
if (UBACKGROUND) {
	t1 = y[2]-Uback(x);
	dydx[1] = -( NN0(x)/(t1*t1) + d2Uback(x)/t1 )*y[0];
} else dydx[1] = -NN0(x)/(y[2]*y[2])*y[0];

dydx[2] = 0.0;
}
/*************************************************************************/
/*************************************************************************/

static void first_order_coeff_UBACK (int step)
{
extern double *phi, *phi_dz;
int i, j;
double dz;
double *p1, *p2, *p3, int3, int6, int12;
double dum, emax, emz, t1;
FILE *f1;

int3 = int6 = int12 = 0.0;
dz = zz[1] - zz[0];

/*  This loop defines the pieces needed to evaluate r01 and r10 */
emax=0.0;
for (i=0; i<step; ++i) {
	dum = clw-Uback (zz[i]);
	E[i] = clw*phi[i]/dum;
	if (fabs (E[i]) > emax) {
		emax = fabs (E[i]);
		emz = zz[i];
	}

	dEdz[i]=(clw/dum)*(phi_dz[i]+dUback(zz[i])*phi[i]/dum);
}
 
for (j=0; j<step; ++j){
	E[j] = E[j]/emax;
	dEdz[j] = dEdz[j]/emax;
	phi[j]=phi[j]/emax;
	phi_dz[j]=phi_dz[j]/emax;
}
printf ("\nThe maximum of E is set to 1 and phi is recalculated");
printf ("\n  The maximum %1.8e occurs at z = %+1.4e \n", emax, emz);

f1 = fopen ("phi", "w");
for (i=0; i<vstep; ++i) fprintf (f1, "%+1.4e %+1.4e\n", zz[i], phi[i]);
fclose (f1);
f1 = fopen ("phi_dz", "w");
for (i=0; i<vstep; ++i) fprintf (f1, "%+1.4e %+1.4e\n", zz[i], phi_dz[i]);
fclose (f1);
f1 = fopen ("E", "w");
for (i=0; i<vstep; ++i) fprintf (f1, "%+1.4e %+1.4e\n", zz[i], E[i]);
fclose (f1);
f1 = fopen ("dEdz", "w");
for (i=0; i<vstep; ++i) fprintf (f1, "%+1.4e %+1.4e\n", zz[i], dEdz[i]);
fclose (f1);

for (i=0; i<step; ++i) {
	if (i==0 || i==step-1) {
		t1 = clw-Uback(zz[i]);
		int3 += 0.5*dEdz[i]*dEdz[i]*dEdz[i]*t1*t1;
		int6 += 0.5*dEdz[i]*dEdz[i]*t1;
		int12 += 0.5*E[i]*E[i]*t1*t1;
	} else {
		t1 = clw-Uback(zz[i]);
		int3 += dEdz[i]*dEdz[i]*dEdz[i]*t1*t1;
		int6 += dEdz[i]*dEdz[i]*t1;
		int12 += E[i]*E[i]*t1*t1;
	}
}

int3  *= dz;
int6  *= dz;
int12 *= dz;


Iden = 2*int6;
I10  = -1.5*int3/clw;
I01  = -int12;
r10  = I10/Iden;
r01  = I01/Iden;


printf ("r10  = %+1.4e\n", r10);
printf("r01 = %1.4e\n", r01);

}

/*************************************************************************/
/*************************************************************************/

static void first_order_coeff_NOUBACK (int step)
{
extern double *phi, *phi_dz;
FILE *f1;
int i;
double dz;
double int3, int6, int12;
int3 = int6 = int12 = 0.0;

dz = zz[1] - zz[0];

for (i=0; i<step; ++i) {
	E[i] = phi[i];
	dEdz[i] = phi_dz[i];
}
 
f1 = fopen ("phi", "w");
for (i=0; i<vstep; ++i) fprintf (f1, "%+1.4e %+1.4e\n", zz[i], phi[i]);
fclose (f1);
f1 = fopen ("phi_dz", "w");
for (i=0; i<vstep; ++i) fprintf (f1, "%+1.4e %+1.4e\n", zz[i], phi_dz[i]);
fclose (f1);
f1 = fopen ("E", "w");
for (i=0; i<vstep; ++i) fprintf (f1, "%+1.4e %+1.4e\n", zz[i], E[i]);
fclose (f1);
f1 = fopen ("dEdz", "w");
for (i=0; i<vstep; ++i) fprintf (f1, "%+1.4e %+1.4e\n", zz[i], dEdz[i]);
fclose (f1);
for (i=0; i<step; ++i) {
	if (i==0 || i==step-1) {
		int3 += 0.5*phi_dz[i]*phi_dz[i]*phi_dz[i];
		int6 += 0.5*phi_dz[i]*phi_dz[i];
		int12 += 0.5*phi[i]*phi[i];
	} else {
		int3 += phi_dz[i]*phi_dz[i]*phi_dz[i];
		int6 += phi_dz[i]*phi_dz[i];
		int12 += phi[i]*phi[i];
	}
}
      
int3  *= dz;
int6  *= dz;
int12 *= dz;

Iden = 2*int6;
I10  = -1.5*int3;
I01  = -clw*int12;
r10  = I10/Iden;
r01  = I01/Iden;

printf ("Iden  = %+1.6e\n", Iden);
printf ("r10  = %+1.6e\n", r10);
printf ("r01 = %1.6e\n", r01);
dz = clw*clw;
dz = dz*dz*dz;
dz *= Iden*Iden*r01/(r10*clw);
dz = clw*clw*clw*Iden*Iden*r01/r10;

printf ("for adiabatic shoaling: c^6*Iden^2*6.0*r01/(r10*c) = %1.6e\n", dz);
}
