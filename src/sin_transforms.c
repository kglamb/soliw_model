/* These routines use a spectral method to solve poisson's equation with SOLIW
   in mind as the application.
   2009, Chris Subich
   */

#include <stdio.h>
#include <fftw3.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

/* libm usually defines M_PI, but sometimes not */
#ifdef M_PI
#define Pi M_PI
#else
#define Pi 3.1415926535897932384
#endif

/* Implementation of sin-based transforms */

void sin_pois_solve(double * lhs, double * rhs, int N, int M,
      double Lx, double Lz) {
   /* Because of an idiosyncracy of FFTW, we have to build a plan that includes
      the arrays to transform.  We don't want to keep rebuilding this plan, so
      we can keep track of the source and dest array.  If they're the same
      as previous calls, then we can re-use the FFTW plan. */
   static double * source = 0;
   static double * dest = 0;
   static fftw_plan src_transform = NULL;
   static fftw_plan dst_transform = NULL;
   int ii, jj;

   if (lhs != dest || rhs != source) {
      /* Recomputing FFTW plans */
      printf("sin_pois_solve: recomputing FFTW plans.. ");
      source = rhs;
      dest = lhs;

      /* Planning with FFTW_<anything but ESTIMATE> trashes the array.
         We want to -copy- the rhs array (which contains our source data)
         to a new memory location, plan, and then copy back */
      double * temp_rhs = calloc(N*M,sizeof(double));
      if (!temp_rhs) {
         printf("ERROR ALLOCATING MEMORY\n");
         abort();
      }
      memcpy(temp_rhs,rhs,N*M*sizeof(double));
      /* Forward 2D sine transform */
      src_transform = fftw_plan_r2r_2d(N,M,source,dest,
            FFTW_RODFT10,FFTW_RODFT10,FFTW_PATIENT);
      /* 2D inverse sine transform */
      dst_transform = fftw_plan_r2r_2d(N,M,dest,dest,
            FFTW_RODFT01,FFTW_RODFT01,FFTW_PATIENT);
      memcpy(rhs,temp_rhs,N*M*sizeof(double));
      free(temp_rhs);
      printf("done!\n");
   }
   if (src_transform == NULL || dst_transform == NULL) {
      fprintf(stderr,"Error -- null fftw plan\n");
      abort();
   }
   /* Transform rhs to get sine coefficients */
   fftw_execute(src_transform);
   for (jj = 0; jj < M; jj++) {
      for (ii = 0; ii < N; ii++) {
         /* Now, divide by -(k^2 + m^2) to invert the Laplacian */
         double k = (ii+1)*Pi/Lx;
         double m = (jj+1)*Pi/Lz;
         /* Also, include N*M as a normalization factor */
         lhs[ii*M + jj] /= -(k*k + m*m)*(N*M)*4; 
      }
   }
   /* Transform lhs (in place) to invert the sine transform and
      get the answer */
   fftw_execute(dst_transform);
   /* et voila! */
}



void sin_x_deriv(double * f, double * fx, int N, int M, double Lx) {
   /* Takes the DST-based derivative along the x (first) dimension */
   static double * source = 0;
   static double * dest = 0;
   static fftw_plan src_transform = NULL;
   static fftw_plan dst_transform = NULL;
   int ii,jj;

   //   printf("entered sin_x_deriv\n");

   if (f != source || fx != dest) {
      /* Recomputing FFTW plans */
      printf("sin_x_deriv: recomputing FFTW plans.. ");
      source = f;
      dest = fx;

      /* Planning with FFTW_<anything but ESTIMATE> trashes the array.
         We want to -copy- the rhs array (which contains our source data)
         to a new memory location, plan, and then copy back */
      double * temp_source = calloc(N*M,sizeof(double));
      if (!temp_source) {
         fprintf(stderr,"ERROR ALLOCATING MEMORY\n");
         abort();
      }
      memcpy(temp_source,source,N*M*sizeof(double));
      /* Forward 1D sine transform */
      fftw_r2r_kind kind  = FFTW_RODFT10;
      src_transform = fftw_plan_many_r2r(1, &N, M,
            source, NULL, M, 1, dest, NULL, M, 1,
            &kind, FFTW_PATIENT);
      /* Backward 1D -cosine- transform */
      kind = FFTW_REDFT01; 
      dst_transform  = fftw_plan_many_r2r(1, &N, M,
            dest, NULL, M, 1, dest, NULL, M, 1,
            &kind, FFTW_PATIENT);
      memcpy(source,temp_source,N*M*sizeof(double));
      free(temp_source);
      printf("done!\n");
   }
   if (src_transform == NULL || dst_transform == NULL) {
      fprintf(stderr,"Error -- null fftw plan\n");
      abort();
   }

   /* Now, go forwards */
   //   printf("calling fftw_execute\n");

   fftw_execute(src_transform);
   //   printf(".... back\n");
   /* To take derivatives in sine-space, multiply by the x-wavenumber. 
      We just have to be careful to shift frequencies by one, since
      the cosine transform includes a DC term that the sine transform
      lacks.  Thus, the output at freq[i,k] is really determined by
      the input at freq[i-1,k].  So, go -backwards- */
   for (jj = M-1; jj >= 0; jj--) {
      for (ii = N-1; ii >= 1; ii--) {
         double k = (ii)*Pi/Lx;
//         fprintf(stderr,"%.3f %.3f\n",k,dest[ii-1 + N*jj]);
         dest[ii*M + jj] = k*dest[(ii-1)*M + jj]/(N)/2;
      }
      /* And set zero-frequency to 0 */
      dest[0*M + jj] = 0;
   }
   /* And take the reverse transform */
   fftw_execute(dst_transform);
}


void sin_z_deriv(double * f, double * fz, int N, int M, double Lz) {
   /* Takes the DST-based derivative along the z (second) dimension */
   static double * source = 0;
   static double * dest = 0;
   static fftw_plan src_transform = NULL;
   static fftw_plan dst_transform = NULL;
   int ii,jj;

   if (f != source || fz != dest) {
      /* Recomputing FFTW plans */
      printf("sin_z_deriv: recomputing FFTW plans.. ");
      source = f;
      dest = fz;

      /* Planning with FFTW_<anything but ESTIMATE> trashes the array.
         We want to -copy- the rhs array (which contains our source data)
         to a new memory location, plan, and then copy back */
      double * temp_source = calloc(N*M,sizeof(double));
      if (!temp_source) {
         fprintf(stderr,"ERROR ALLOCATING MEMORY\n");
         abort();
      }
      memcpy(temp_source,source,N*M*sizeof(double));
      /* Forward 1D sine transform */
      fftw_r2r_kind kind  = FFTW_RODFT10;
      src_transform = fftw_plan_many_r2r(1, &M, N,
            source, NULL, 1, M, dest, NULL,  1, M,
            &kind, FFTW_PATIENT);
      /* Backward 1D -cosine- transform */
      kind = FFTW_REDFT01; 
      dst_transform  = fftw_plan_many_r2r(1, &M, N,
            dest, NULL, 1, M, dest, NULL, 1, M,
            &kind, FFTW_PATIENT);
      memcpy(source,temp_source,N*M*sizeof(double));
      free(temp_source);
      printf("done!\n");
   }
   if (src_transform == NULL || dst_transform == NULL) {
      fprintf(stderr,"Error -- null fftw plan\n");
      abort();
   }

   /* Now, go forwards */
   fftw_execute(src_transform);

   /* Again, to take derivatives in sine-space, multiply by the
      wavenumber.  The up-shift is now along the -second-
      dimension (z), rather than x. */
   for (jj = M-1; jj >= 1; jj--) {
      double k = (jj)*Pi/Lz;
      for (ii = N-1; ii >= 0; ii--) {
         dest[ii*M + jj] = k*dest[(ii)*M + (jj-1)]/(M)/2;
      }
   }
   for (ii = 0; ii < N; ii++) {
      // Set zero-frequency to 0
      dest[ii*M + 0] = 0;
   }
   /* And take the reverse transform */
   fftw_execute(dst_transform);
}

