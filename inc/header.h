#define BINARY 1
#define Pi 3.141592653589793

#define SPUD 0
/* It may be possible to move this into read_startup() in main.c, but
   make sure nowhere else is using this to grab options.
   */
#if SPUD
struct spud_option{
	void* option;
	char* key;
	int key_len;
	int is_required;
};
#endif
