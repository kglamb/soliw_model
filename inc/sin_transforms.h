/* sin_transforms.h -- prototypes for sin-based transforms to:
      1) Solve Poisson's problem Lap(eta) = rhs
      2) Find d/dx (eta)
      3) Find d/dz (eta)

   with soliw in mind as the application */

void sin_pois_solve(double * lhs, // left hand side -- answer
      double * rhs, // right hand side -- given
      int N, int M, // number of grid points in first and second dimensions
      double Lx, double Lz // lengths in first, second dimensions
      );

void sin_x_deriv(double * f, // given function
      double * fx, // its derivatve (output parameter)
      int N, int M, // number of grid points in dimensions 1, 2
      double Lx); // grid length

void sin_z_deriv(double * f, // given function
      double * fz, // derivative (output)
      int N, int M, // number of grid points in dimensions 1, 2
      double Lz); // grid length
