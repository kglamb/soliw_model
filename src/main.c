/*******************************************************************/
/**             soliw: a solitary wave calculator                 **/
/**                                                               **/
/**    Developed by Kevin Lamb, Bangjun Wan and Marek Stastna.    **/
/**                                                               **/
/*******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "header.h"
#if SPUD
#include <cspud.h>
#endif

double g,L,H;
int I, J;

//Options for background current and particle tracking
int max_iterate;
int min_iterate;

int UBACKGROUND;
int PARTICLE_TRACKING;
int NUM_PART;
int BACKGROUND_STATE_FROM_DATA;
int Ndata_vals;
int OPPOSITE_POLARITY;

// background density and velocity parameters
double drho_halved1,drho_halved2;
double zpyc1, zpyc2, dpyc1, dpyc2;
double U1, U2, U3, zsh1, zsh2, dsh1, dsh2;


double relax;    // relaxation parameter in get_eta():  eta = relax*eta + (1-relax)*eta0;

// Output file options
int ETA_OUT; // eta#
int UIEG_OUT; // U_ieg*
int DETA_OUT; // deta*
int ETA_VERT_OUT; // eta_vert*
int D_OUT; // D#
int TEST_OUT; //Only for use by test program

// If compiled with spud, use this flag to determine whether the current
// parameters file is a spud file or a clear text file.
#if SPUD
int SPUD_FLAG;
#endif

void usage_error (const char* program_name);
void read_startup (const char* filename, const int filename_len);

extern void initialize_nonlinear (), store_sol (), solitary_waves_to_compute ();
extern void store_background_profiles (), clear_output_files ();
extern void setup_background_info();
extern void get_eta_allocation(void);
extern void compute_solitary_wave_allocation(void);


main (int argc, char *argv[]){

// Process command line options:
char* default_filename="soliw_startup";
char* filename;

#if SPUD
   SPUD_FLAG = 0; //Default is to not use spud if unknown or no suffix.
   if (argc > 3) usage_error (argv[0]); //too many parameters

   int i;
   for (i=1; i<argc; i++) {
	if (!strcmp (argv[i], "-spud")) SPUD_FLAG = 1;
	else if (strstr (argv[i], ".xml")) {
		SPUD_FLAG = 1;
		filename = argv[i];
	} else filename = argv[i];
   }
#else
         // No file specified, use default
   if (argc == 1) filename = default_filename; 
         // Use specified filename
   else if (argc == 2) filename = argv[1]; 
   else usage_error (argv[0]); //too many parameters
#endif

/* Check that the parameters file exists */

 FILE* test;
 test = fopen(filename, "r");
 if(test == (FILE*) NULL) {
    fprintf(stderr, "File %s does not exist, or you do not have adequate permissions\n", filename);
    usage_error(argv[0]);
 }
 fclose(test);

 FILE *f1;
 read_startup(filename, strlen(filename));

 f1 = fopen("soliw_graphics","w");
 fprintf(f1,"%d %d %1.8e %1.8e %1.8e\n",I,J,L,H,g);
 fclose(f1);

 if(BINARY){
    f1 = fopen("precision","w");
    fprintf(f1,"%d",(int)sizeof(double));
    fclose(f1);
    f1 = fopen("machine","w");
    fwrite(&I, (int)sizeof(int), 1, f1);
    fclose(f1);
 }

 if(BACKGROUND_STATE_FROM_DATA) setup_background_info();
 store_background_profiles ();
 clear_output_files ();
 get_eta_allocation();
 compute_solitary_wave_allocation();
 solitary_waves_to_compute ();
 exit (0);
}

/*****************************************************************************/
/*****************************************************************************/
void usage_error (const char* program_name) {
fprintf (stderr, "usage: %s [-spud] filename\n", program_name);
exit (1); 
}


/*****************************************************************************/
/*****************************************************************************/
void read_startup (const char* filename, const int filename_len){
#if SPUD   /** haven't changed SPUD option for read background state from data files **/
if (SPUD_FLAG){
	cspud_clear_options ();
	cspud_load_options (filename, &filename_len);
	
	// Temporary storage for variables that change type.
	// e.g. string "true" -> int 1
	// Set any defaults now for optional parameters.
	NUM_PART = 20;
	g = 9.81;
	max_iterate = 1000;
	min_iterate = 20;
	char PART_tmp[6] =     "false";
	char UBACK_tmp[6]; //Not optional
	char ETA_tmp[6] =      "true";
	char UIEG_tmp[6] =     "false";
	char DETA_tmp[6] =     "false";
	char ETA_VERT_tmp[6] = "false";
	char D_tmp[6] =        "false";

	// Option keys, and key lengths
	// To add an option, increase NUM_SPUD_OPTIONS and then add your option
	// to the array spud_options { pointer-to-variable, key, -1, required?}
	// The spud_option struct is defined in header.h in case you want to
	// use it outside of main.
#define NUM_SPUD_OPTIONS 16
	struct spud_option spud_options[NUM_SPUD_OPTIONS] = {
  	        { &I, "/required/I", -1, 1 },
		{ &J, "/required/J", -1, 1 },
		{ &L,     "/required/L", -1, 1 },
		{ &H,     "/required/H", -1, 1 },
		{ &UBACK_tmp, "/required/background_current", -1, 1 },

		{ &g, "/optional/g", -1, 0 },
		{ &max_iterate, "/optional/max_iterate", -1, 0 },
		{ &min_iterate, "/optional/min_iterate", -1, 0 },
		{ &PART_tmp, "/optional/particle_tracking/enable", -1, 0 },
		{ &NUM_PART, "/optional/particle_tracking/number_of_particles", -1, 0 },
		{ &D_tmp, "/optional/output/D", -1, 0 },
		{ &ETA_VERT_tmp, "/optional/output/eta_vert", -1, 0 },
		{ &DETA_tmp, "/optional/output/deta", -1, 0 },
		{ &UIEG_tmp, "/optional/output/u_ieg", -1, 0 },
		{ &ETA_tmp, "/optional/output/eta", -1, 0 },
		{ &TEST_OUT, "/optional/test", -1, 0 }
	};
	int i;		
	for (i=0; i<NUM_SPUD_OPTIONS; i++) {
		spud_options[i].key_len = strlen (spud_options[i].key);
		if (cspud_get_option (spud_options[i].key,
		                      &spud_options[i].key_len,
		                      spud_options[i].option)
		    == SPUD_KEY_ERROR && spud_options[i].is_required) {
			fprintf (stderr, "Error: Required option %s missing from spud options file %s.", spud_options[i].key, filename);
		}
	}
	// Set options that need type change (e.g. string -> int)
	// NOTE: using strstr (substrings) because spud doesn't return nice strings
	// They often lack \0 terminator.
	if (strstr (UBACK_tmp, "true")!= NULL) UBACKGROUND = 1;
	else UBACKGROUND = 0;
	if (strstr (PART_tmp, "true")!= NULL) PARTICLE_TRACKING = 1;
	else PARTICLE_TRACKING = 0;
	if (strstr (ETA_tmp, "true")!= NULL) ETA_OUT = 1;
	else ETA_OUT = 0;
	if (strstr (UIEG_tmp, "true")!= NULL) UIEG_OUT = 1;
	else UIEG_OUT = 0;
	if (strstr (DETA_tmp, "true")!= NULL) DETA_OUT = 1;
	else DETA_OUT = 0;
	if (strstr (ETA_VERT_tmp, "true")!= NULL) ETA_VERT_OUT = 1;
	else ETA_VERT_OUT = 0;
	if (strstr (D_tmp, "true")!= NULL) D_OUT = 1;
	else D_OUT = 0;
}
else { // non-spud option file
#endif// Parameters file input ruthlessly copied from igw project
	FILE* f1;
	char line[256];
	char label[128], data[128];
	char* pch;
	int line_count, param_count, param_count_needed;
	int result;

	// Set defaults for optional parameters here
	g = 9.81;
	NUM_PART=20;
	TEST_OUT=0;
	max_iterate = 1000;
	min_iterate = 20;
        relax = 0.2;
        BACKGROUND_STATE_FROM_DATA = 0;
        OPPOSITE_POLARITY = 0;

	f1 = fopen (filename, "r");
	line_count = 0;
	param_count = 0;
	param_count_needed = 5;
	while (fgets (line, 256, f1) != NULL){
		line_count++;
		/* Remove comments */
		pch = strchr (line, '#');
		if (pch != NULL) {
			*pch = '\n';
			*(pch + 1) = '\0';
		}
		if (strlen (line) > 1) {
			/* line contained more than just a comment or blank */
			pch = strtok (line, " =");
			if (pch != NULL) {
				strcpy (label,pch);

				pch = strtok (NULL, " =\n");
				if (pch != NULL) {
					strcpy (data, pch);
					param_count++;
				} else {
					fprintf (stderr, "Bad input in %s: line %d\n", 
                                                   filename, line_count);
					exit (1);
				}
				
				if (strcmp("I", label) == 0) I = atoi (data);
				else if (!strcmp ("J", label)) J = atoi (data);
				else if (!strcmp("H", label)) H = atof (data);
				else if (!strcmp("L", label)) L = atof (data);
				else if (!strcmp("UBackground", label)) UBACKGROUND = atoi (data);
				else if (!strcmp("Background_data_file", label)) {
                                        Ndata_vals = atoi(data);
					if(Ndata_vals<0){
					  fprintf(stderr, "Negative Ndata_vals: Exiting\n");
                                          exit(0);
					}
                                        BACKGROUND_STATE_FROM_DATA = (Ndata_vals) ? 1: 0;
					param_count_needed++;
				}
				else if (!strcmp("ParticleTracking", label)) {
					PARTICLE_TRACKING = atoi (data);
					param_count_needed++;
				} else if (!strcmp ("NumberOfParticles", label)) {
					NUM_PART = atoi (data);
					param_count_needed++;
				} else if (!strcmp ("g", label)) {
					g = atof (data);
					param_count_needed++;
				} else if (!strcmp ("relax", label)) {
					relax = atof (data);
					param_count_needed++;
				} else if (!strcmp ("eta", label)){
					ETA_OUT = atoi (data);
					param_count_needed++;
				} else if (!strcmp ("u_ieg", label)){
					UIEG_OUT = atoi (data);
					param_count_needed++;
				} else if (!strcmp ("deta", label)){
					DETA_OUT = atoi (data);
					param_count_needed++;
				} else if (!strcmp ("eta_vert", label)){
					ETA_VERT_OUT = atoi (data);
					param_count_needed++;
				} else if (!strcmp ("D", label)){
					D_OUT = atoi (data);
					param_count_needed++;
				} else if (!strcmp ("TestOut", label)) {
					TEST_OUT = atoi (data);
					param_count_needed++;
				} else if (!strcmp ("maxIterate", label)) {
					max_iterate = atoi (data);
					param_count_needed++;
				} else if (!strcmp ("minIterate", label)) {
					min_iterate = atoi (data);
					param_count_needed++;
				} else if (!strcmp ("OPPOSITE_POLARITY", label)) {
					OPPOSITE_POLARITY = atoi (data);
					param_count_needed++;
				} else if (!strcmp ("drho_halved1", label)) {
					drho_halved1 = atof (data);
					param_count_needed++;
				} else if (!strcmp ("drho_halved2", label)) {
					drho_halved2 = atof (data);
					param_count_needed++;
				} else if (!strcmp ("zpyc1", label)) {
					zpyc1 = atof (data);
					param_count_needed++;
				} else if (!strcmp ("zpyc2", label)) {
					zpyc2 = atof (data);
					param_count_needed++;
				} else if (!strcmp ("dpyc1", label)) {
					dpyc1 = atof (data);
					param_count_needed++;
				} else if (!strcmp ("dpyc2", label)) {
					dpyc2 = atof (data);
					param_count_needed++;
				} else if (!strcmp ("U1", label)) {
					U1 = atof (data);
					param_count_needed++;
				} else if (!strcmp ("U2", label)) {
					U2 = atof (data);
					param_count_needed++;
				} else if (!strcmp ("U3", label)) {
					U3 = atof (data);
					param_count_needed++;
				} else if (!strcmp ("zsh1", label)) {
					zsh1 = atof (data);
					param_count_needed++;
				} else if (!strcmp ("zsh2", label)) {
					zsh2 = atof (data);
					param_count_needed++;
				} else if (!strcmp ("dsh1", label)) {
					dsh1 = atof (data);
					param_count_needed++;
				} else if (!strcmp ("dsh2", label)) {
					dsh2 = atof (data);
					param_count_needed++;
				} else{
					fprintf (stderr, "Warning: Couldn't identify LABEL: '%s' in %s file\n", label, filename);
					param_count--;
				}
			}
		}
	}
	fclose(f1);
	if (param_count != param_count_needed) {
		fprintf(stderr, "Warning: Not enough parameters.\n");
		exit (1);
	}
#if SPUD
}
#endif

printf("I = %d,  J = %d\n", I, J);
printf("L = %1.5e    H = %1.5e    g = %1.5e\n", L, H, g);
printf("relax = %1.5e    Ubackground = %d\n", relax, UBACKGROUND);
}
