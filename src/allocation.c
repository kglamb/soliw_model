/* Allocation routines for vectors and matricies with size
   known at run time.  All memory is checked after allocation.

   2009, Ben Langmuir

   Rewrote the (2D/3D) allocation routines to allocate continuous blocks
   of memory, and made a few other adjustments.
   2013, Michael Dunphy
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "allocation.h"
#include "helper_functions.h"

static inline void check_mem(void *p, size_t s);
static inline void check_ptr(void *p);

static size_t total = 0;

void allocate_stats(void) {
    iprintf("allocate.c: %lu bytes (%.3f MB) allocated in total\n",total,(double)total/1.0e6);
}

/* Allocate a vector of length n with integer type. */
int* alloc_int_vector (long int n)
{
    if (n==0) return(NULL);

    int *p = (int*) calloc((size_t)n, sizeof(int));
    check_mem (p, (size_t)n*sizeof(int));

    total+=(size_t)n*sizeof(int);
    return p;
}

/* Allocate a vector of length n. */
double* alloc_vector (long int n)
{
    if (n==0) return(NULL);

    double *p = (double*) calloc((size_t)n, sizeof(double));
    check_mem(p, (size_t)n*sizeof(double));

    total+=(size_t)n*sizeof(double);
    return p;
}

/* Allocate a matrix with n "rows" and m "columns". */
double** alloc_matrix (int n, int m)
{
    if (n*m==0) return(NULL);

    double **p = (double**) malloc((size_t)n*sizeof(double*));
    check_mem(p, (size_t)(n*m)*sizeof(double));

    double *data = calloc((size_t)(n*m), sizeof(double));
    check_mem(data, (size_t)(n*m)*sizeof(double));

    // Setup pointers
    int i;
    for (i=0; i<n; i++) p[i] = data + i*m;

    total+=(size_t)(n*m)*sizeof(double);
    return p;
}

/* Allocate a 3D array with n "rows" and m "columns" and o "layers". */
double*** alloc_3darray (int n, int m, int o)
{
    if (n*m*o==0) return(NULL);

    double ***p = (double***) malloc((size_t)n*sizeof(double**));
    check_mem(p, (size_t)(n*m*o)*sizeof(double));

    double **pp = (double**) malloc((size_t)(m*n)*sizeof(double*));
    check_mem(pp, (size_t)(n*m*o)*sizeof(double));

    double *data = calloc((size_t)(n*m*o), sizeof(double));
    check_mem(data, (size_t)(n*m*o)*sizeof(double));

    // Setup pointer pointers
    int i;
    for (i=0; i<n; i++) p[i] = pp + i*m;

    // Setup pointers
    for (i=0; i<n*m; i++) pp[i] = data + i*o;

    total+=(size_t)(n*m*o)*sizeof(double);
    return p;
}

/* Free a dynamically allocated vector. */
void free_vector(double *p)
{
    check_ptr(p);
    free(p);
}

/* Free a dynamically allocated int vector. */
void free_int_vector(int *p)
{
    check_ptr(p);
    free(p);
}

/* Free a dynamically allocated matrix. */
void free_matrix(double **p)
{
    check_ptr(p);
    free(p[0]); // free the data
    free(p); // free the pointers
}

/* Free a dynamically allocated 3d array. */
void free_3darray(double ***p)
{
    check_ptr(p);
    free(p[0][0]); // free the data
    free(p[0]); // free the pointers
    free(p); // free the pointer pointers
}

/* Check that memory was allocated properly. */
static inline void check_mem(void *p, size_t s)
{
    if (p == NULL) {
        fprintf(stderr, "ERROR: Allocation failed (size %lu)\n", s);
        exit(EXIT_FAILURE);
    }
}
/* Verify that a pointer is not NULL. */
static inline void check_ptr(void *p)
{
    if (p == NULL) {
        fprintf(stderr, "ERROR: Attempting to free a NULL pointer\n");
        exit(EXIT_FAILURE);
    }
}
