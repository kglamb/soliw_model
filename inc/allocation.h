#ifndef __ALLOCATION_H__
#define __ALLOCATION_H__

#include <stdlib.h>

int* alloc_int_vector(long int n);
double* alloc_vector(long int n);
double** alloc_matrix(int n, int m);
double*** alloc_3darray(int n, int m, int o);
void free_int_vector(int *p);
void free_vector(double *p);
void free_matrix(double **p);
void free_3darray(double ***p);
void allocate_stats(void);
#endif  /* __ALLOCATION_H__ */
