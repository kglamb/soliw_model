#include <stdio.h>
#include <math.h>
#include "header.h"

/***  single thermocline stratification      **/

#if 0

   double drho_halved = 0.001;     /** density jump halved        **/
   double zpyc = -15.0;            /** centre of thermocline      **/
   double dpyc = 8.0;             /** thermocline width: uses    **/
                               /** tanh(z*2*atanh(.99)/dpyc)  **/
#elif 1

   double drho_halved = 0.5; 
   double zpyc = -0.1;      
   double dpyc = 0.1;          /** tanh(z*2*atanh(.99)/dpyc)  **/   
                          
#else /** for Tim's Ocean stratifications  **/

   double drho_halved = 0.004878;     /** density jump halved        **/
   double zpyc = -600.0;            /** centre of thermocline      **/
   double dpyc = 300.0;             /** thermocline width: uses    **/
                                /** tanh(z*2*atanh(.99)/dpyc)  **/
#endif


#define twice_atanh0p99 5.29330482472499

/*****************************************************************************/
/*****************************************************************************/
/***   Test for coefficients: 2-layer fluid                                ***/
/***                                                                       ***/
/***   c = sqrt(g'*h1*h2/(h1+h2),   beta = -r01 = c*h1*h2/6                ***/
/***  alpha = -2*r10*c    = 1.5*c*(h1-h2)/(h1*h2)                          ***/
/***  alpha1 = -3*r20*c*c = -3*c/(8*h1^2*h2^2)*(h1^2+h2^2+6*h1*h2)         ***/
/***                                                                       ***/
/***  for h1 = 40, h2 = 60, delta_rho/rho_0 = 0.01 get                     ***/
/***    c = 1.5344,  beta = 613.8, alpha = -0.01918, alpha1 = -0.001958    ***/
/***    ==> r10 = 6.25e-3,  r20 = 2.77e-4                                  ***/
/***                                                                       ***/
/*****************************************************************************/
/*****************************************************************************/

extern double H,L,g;
void solitary_waves_to_compute()
{
   void compute_solitary_wave();
   int i;

#if 0   // for stratification 2
   compute_solitary_wave(510.0,L,"WNL",1.0e-6);
   compute_solitary_wave(715.0,L,"prev",1.0e-6);
   compute_solitary_wave(1205.0,L,"prev",1.0e-6);
   compute_solitary_wave(1525.0,L,"prev",1.0e-6);
   compute_solitary_wave(1680.0,L,"prev",1.0e-6);
   compute_solitary_wave(1885.0,L,"prev",1.0e-6);
   compute_solitary_wave(2145.0,L,"prev",1.0e-6);
   compute_solitary_wave(2545.0,L,"prev",1.0e-6);
   compute_solitary_wave(3245.0,L,"prev",1.0e-6);
#elif 0   // for stratification 3
   compute_solitary_wave(1060.0,L,"WNL",1.0e-6);
   compute_solitary_wave(1150.0,L,"prev",1.0e-6);
   compute_solitary_wave(1250.0,L,"prev",1.0e-6);
   compute_solitary_wave(1375.0,L,"prev",1.0e-6);
   compute_solitary_wave(1525.0,L,"prev",1.0e-6);
   compute_solitary_wave(1725.0,L,"prev",1.0e-6);
   compute_solitary_wave(1990.0,L,"prev",1.0e-6);
   compute_solitary_wave(2395.0,L,"prev",1.0e-6);
   compute_solitary_wave(3125.0,L,"prev",1.0e-6);
#elif 0   // for stratification 4
   compute_solitary_wave(520.0,L,"WNL",1.0e-6);
   compute_solitary_wave(585.0,L,"prev",1.0e-6);
   compute_solitary_wave(665.0,L,"prev",1.0e-6);
   compute_solitary_wave(775.0,L,"prev",1.0e-6);
   compute_solitary_wave(845.0,L,"prev",1.0e-6);
   compute_solitary_wave(930.0,L,"prev",1.0e-6);
   compute_solitary_wave(1035.0,L,"prev",1.0e-6);
#elif 0   // for stratification 5
   compute_solitary_wave(800.0,L,"WNL",1.0e-6);
   compute_solitary_wave(1100.0,L,"prev",1.0e-6);
   compute_solitary_wave(1400.0,L,"prev",1.0e-6);
   compute_solitary_wave(1700.0,L,"prev",1.0e-6);
   compute_solitary_wave(2000.0,L,"prev",1.0e-6);
   compute_solitary_wave(2265.0,L,"prev",1.0e-6);
   compute_solitary_wave(2585.0,L,"prev",1.0e-6);
   compute_solitary_wave(3065.0,L,"prev",1.0e-6);
   compute_solitary_wave(3870.0,L,"prev",1.0e-6);
#elif 0   // for stratification 7
   compute_solitary_wave(0.03525,L,"WNL",1.0e-6);
   compute_solitary_wave(0.04525,L,"prev",1.0e-6);
   compute_solitary_wave(0.06175,L,"prev",1.0e-6);
   compute_solitary_wave(0.07525,L,"prev",1.0e-7);
   compute_solitary_wave(0.09650,L,"prev",1.0e-7);
   compute_solitary_wave(0.13650,L,"prev",1.0e-7);
   /*
   compute_solitary_wave(1210.0,L,"WNL",1.0e-6);
   compute_solitary_wave(1330.0,L,"prev",1.0e-6);
   compute_solitary_wave(1470.0,L,"prev",1.0e-6);
   compute_solitary_wave(1655.0,L,"prev",1.0e-6);
   compute_solitary_wave(1890.0,L,"prev",1.0e-6);
   compute_solitary_wave(2200.0,L,"prev",1.0e-6);
   compute_solitary_wave(2680.0,L,"prev",1.0e-6);
   compute_solitary_wave(3515.0,L,"prev",1.0e-6);
   */
#elif 0   // for stratification 8
   compute_solitary_wave(940.0,L,"WNL",1.0e-6);
   compute_solitary_wave(1045.0,L,"prev",1.0e-6);
   compute_solitary_wave(1155.0,L,"prev",1.0e-6);
   compute_solitary_wave(1295.0,L,"prev",1.0e-6);
   compute_solitary_wave(1465.0,L,"prev",1.0e-6);
   compute_solitary_wave(1680.0,L,"prev",1.0e-6);
#elif 1   // for stratification 11
   compute_solitary_wave(0.04025,L,"WNL",1.0e-7);
   compute_solitary_wave(0.04850,L,"prev",1.0e-7);
   compute_solitary_wave(0.06025,L,"prev",1.0e-7);
   compute_solitary_wave(0.07750,L,"prev",1.0e-7);
   compute_solitary_wave(0.10725,L,"prev",1.0e-7);
   compute_solitary_wave(0.11725,L,"prev",1.0e-7);
   compute_solitary_wave(0.12725,L,"prev",1.0e-7);
   compute_solitary_wave(0.13725,L,"prev",1.0e-7);
   compute_solitary_wave(0.14725,L,"prev",1.0e-7);
   compute_solitary_wave(0.15725,L,"prev",1.0e-7);
   compute_solitary_wave(0.16725,L,"prev",1.0e-7);
   compute_solitary_wave(0.17525,L,"prev",1.0e-7);
#elif 0  // for stratification 12
   compute_solitary_wave(440.0,L,"WNL",1.0e-6);
   compute_solitary_wave(480.0,L,"prev",1.0e-6);
   compute_solitary_wave(525.0,L,"prev",1.0e-6);
   compute_solitary_wave(570.0,L,"prev",1.0e-6);
   compute_solitary_wave(620.0,L,"prev",1.0e-6);
   compute_solitary_wave(750.0,L,"prev",1.0e-6);
   compute_solitary_wave(840.0,L,"prev",1.0e-6);
   compute_solitary_wave(945.0,L,"prev",1.0e-6);
   compute_solitary_wave(1070.0,L,"prev",1.0e-6);
#elif 0   // for stratification 14
   compute_solitary_wave(595.0,L,"WNL",1.0e-6);
   compute_solitary_wave(665.0,L,"prev",1.0e-6);
   compute_solitary_wave(750.0,L,"prev",1.0e-6);
   compute_solitary_wave(855.0,L,"prev",1.0e-6);
   compute_solitary_wave(980.0,L,"prev",1.0e-6);
#endif


#if 0
   compute_solitary_wave(50.0,L,"WNL",1.0e-6);
   #if 1
      for(i=1;i<=35;i++) compute_solitary_wave(50.0+i*10.0,L,"prev",1.0e-6);
   //   for(i=1;i<=600;i++) compute_solitary_wave(1000.0+i*5.0,L,"prev",0.0001);
      for(i=1;i<=1320;i++) compute_solitary_wave(400.0+i*5.0,L,"prev",1.0e-6);
   #else
      for(i=1;i<=30;i++) compute_solitary_wave(50.0+i*5.0,L,"prev",1.0e-4);
      for(i=1;i<=3400;i++) compute_solitary_wave(200.0+i*2.0,L,"prev",1.0e-4);
   #endif

#elif 0

   compute_solitary_wave(0.0025,L,"WNL",1.0e-6);
   #if 1
      for(i=1;i<=11;i++) compute_solitary_wave(0.0025+i*0.0005,L,"prev",1.0e-6);
   //   for(i=1;i<=600;i++) compute_solitary_wave(1000.0+i*5.0,L,"prev",0.0001);
      //      for(i=1;i<=1320;i++) compute_solitary_wave(0.02+i*0.0001,L,"prev",1.0e-6);
      //      for(i=1;i<=1320;i++) compute_solitary_wave(0.008+i*0.0001,L,"prev",1.0e-6);
      for(i=1;i<=500;i++) compute_solitary_wave(0.008+i*0.000025,L,"prev",1.0e-6);
   #else
      for(i=1;i<=30;i++) compute_solitary_wave(50.0+i*5.0,L,"prev",1.0e-4);
      for(i=1;i<=3400;i++) compute_solitary_wave(200.0+i*2.0,L,"prev",1.0e-4);
   #endif

#elif 0

   compute_solitary_wave(0.005,L,"WNL",1.0e-6);
   compute_solitary_wave(0.01,L,"prev",1.0e-6);
   compute_solitary_wave(0.015,L,"prev",1.0e-6);
   compute_solitary_wave(0.02,L,"prev",1.0e-6);
   compute_solitary_wave(0.025,L,"prev",1.0e-6);
   compute_solitary_wave(0.03,L,"prev",1.0e-6);
   compute_solitary_wave(0.03325,L,"prev",1.0e-6);
   compute_solitary_wave(0.039,L,"prev",1.0e-6);
   compute_solitary_wave(0.047,L,"prev",1.0e-6);
   /*
   compute_solitary_wave(0.005,L,"WNL",1.0e-6);
   #if 1
      for(i=1;i<=10;i++) compute_solitary_wave(0.005+i*0.0005,L,"prev",1.0e-6);
   //   for(i=1;i<=600;i++) compute_solitary_wave(1000.0+i*5.0,L,"prev",0.0001);
      //      for(i=1;i<=1320;i++) compute_solitary_wave(0.02+i*0.0001,L,"prev",1.0e-6);
      //      for(i=1;i<=1320;i++) compute_solitary_wave(0.008+i*0.0001,L,"prev",1.0e-6);
      for(i=1;i<=500;i++) compute_solitary_wave(0.01+i*0.0001,L,"prev",1.0e-6);
   #else
      for(i=1;i<=30;i++) compute_solitary_wave(50.0+i*5.0,L,"prev",1.0e-4);
      for(i=1;i<=3400;i++) compute_solitary_wave(200.0+i*2.0,L,"prev",1.0e-4);
   #endif
   */
#endif

   //   for(i=1;i<=20;i++) compute_solitary_wave(100.0+i*20.0,L,"prev",0.0001);
   //   for(i=1;i<=100;i++) compute_solitary_wave(500.0+i*10.0,L,"prev",0.0001);
   //  for(i=5;i<=31;i++) compute_solitary_wave(i*0.25e2,L,"prev",0.0001);
   //   for(i=1;i<=20;i++) compute_solitary_wave(775.0+i*2.5,L,"prev",0.0001);
   //   for(i=1;i<=40;i++) compute_solitary_wave(600.0+2.5*i,L,"prev",0.0001);
   //   for(i=1;i<=500;i++) compute_solitary_wave(1000.0+5.0*i,L,"prev",0.0001);
   /*
   compute_solitary_wave(2.0e-5,L,"prev",0.0001);
   compute_solitary_wave(3.0e-5,L,"prev",0.0001);
   compute_solitary_wave(4.0e-5,L,"prev",0.0001);
   compute_solitary_wave(5.0e-5,L,"prev",0.0001);
   compute_solitary_wave(6.0e-5,L,"prev",0.0001);
   compute_solitary_wave(7.0e-5,L,"prev",0.0001);
   compute_solitary_wave(8.0e-5,L,"prev",0.0001);
   */

}


   double density(double z)
   {
     //     return(1.0-drho_halved*tanh( 2.0*atanh(0.99)*(z-zpyc)/dpyc));
     return(-drho_halved*tanh( 2.0*atanh(0.99)*(z-zpyc)/dpyc));
   }
   /*********************************************************/
   /*********************************************************/
   double d_density(double z)
   {
   double s,m;
   m = twice_atanh0p99/dpyc;
   s=1.0/cosh(m*(z-zpyc));
   return(-drho_halved*m*s*s);
   }
   /*********************************************************/
   /*********************************************************/
   double d2_density(double z)
   {
   double s,m;
   m = twice_atanh0p99/dpyc;
   s=1.0/cosh(m*(z-zpyc));

   return(2*drho_halved*m*m*tanh(m*(z-zpyc))*s*s);
   }


   /*********************************************************/
   /*********************************************************/

double Uback(double z)
{
return(0.0);
}

double dUback(double z)
{
 return(0.0);
}

double d2Uback(double z)
{
return(0.0);
}

void setup_background_info(){}
