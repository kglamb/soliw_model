/* odeint is an ODE integration routine using a mixed 4th and 5th
   order Runge-Kutta-Fehlberg scheme.  Since the same coefficients
   can be used to get 4th and 5th order approximations of a single
   step, we get free error checking and time step scaling
   
   This routine integrates from t1, y to t2 using the derivs
   function.
 
   2009, Ben Langmuir
   */

#include <stdio.h>
//#include <stdlib.h>
#include <math.h>
//#include "header.h"

/* Coefficients used to calculate k1:k6 in rkf45_step
 * Choice is a# is a coefficient of k1, b# k2, etc.
 * q# is a coefficient of t
 */

static const double q2 = 1.0/4.0, q3 = 3.0/8.0;
static const double q4 = 12.0/13.0, q5 = 1.0, q6 = 1.0/2.0;
static const double a2 = 1.0/4.0, a3 = 3.0/32.0;
static const double a4 = 1932.0/2197.0, a5 = 439.0/216, a6 = -8.0/27.0;
static const double b3 = 9.0/32.0, b4 = -7200.0/2197.0, b5 = -8.0, b6 = 2.0;
static const double c4 = 7296.0/2197.0, c5 = 3680.0/513.0, c6 = -3544.0/2565.0;
static const double d5 = -845.0/4104.0, d6 = 1859.0/4104.0;
static const double e6 = -11.0/40.0;
/* Coefficients used to calculate y4 and y5 approximations in rkf45_step. */
static double r1 = 1.0/360.0, r3 = -128.0/4275.0;
static double r4 = -2197.0/75240.0, r5 = 1.0/50.0, r6 = 2.0/55.0;
static double y41 = 25.0/216.0, y43 = 1408.0/2565.0;
static double y44 = 2197.0/4104.0, y45 = -1.0/5.0;

/* Runge-Kutta-Fehlberg coefficients and temporary storage for inside step */
static double k1[5], k2[5], k3[5], k4[5], k5[5], k6[5];
static double dydt[5];
static double ytmp[5];


void rkf45_step (double *t, double *h, double hmin, double epsilon, double *y, int dim, void (*derivs) (double, double*, double*));

double *alloc_vector (int n);
void free_vector (double *p);


/* This is the driver routine for rkf45_step.  It loops from time t1 to t2 and enforces
   the bound hmin and max on the step size h.
   */
void odeint(double t1, double t2, double hin, double hmin, double hmax, double epsilon,
            double *y, int dim, void (*derivs) (double, double*, double*))
{
	double t = t1;
	/* Ignore the sign on h, and set it to the sign on t2-t1 */
	double h = (t2 > t1)? fabs(hin) : -fabs(hin);

	/* hmin and hmax should be positive */
	hmin = fabs (hmin);
	hmax = fabs (hmax);
	
	while (t < t2) {
		if (fabs(h) < hmin) h = hmin;
		if (fabs(h) > hmax) h = hmax;
		if (t + h > t2) h = t2 - t;
		/* Perform the rkf45 step, which may change h */
		rkf45_step (&t, &h, hmin, epsilon, y, dim, derivs);
	}
}


/* Moves time forward from t to t+h, unless h is too big.  Automatically adjusts h
   for the next step.
   */
void rkf45_step (double *t, double *h, double hmin, double epsilon,
                double *y, int dim, void (*derivs) (double, double*, double*))
{
	double err_max;
	double hnew;
	int i;

	/* Set k1:k6  the coefficients for the Runge-Kutta-Fehlberg step.
	   Each dimension is done independently.  ytmp is used to store
	   the appropriate position for derivs.
           */
/* k1 */
	for (i=0; i<dim; i++) ytmp[i] = y[i];
	derivs (*t, ytmp, dydt);
	for (i=0; i<dim; i++) k1[i] = *h * dydt[i];

/* k2 */
	for (i=0; i<dim; i++) ytmp[i] = y[i] + a2*k1[i];
	derivs (*t+q2*(*h), ytmp, dydt);
	for (i=0; i<dim; i++) k2[i] = *h * dydt[i];

/* k3 */
	for (i=0; i<dim; i++) ytmp[i] = y[i] + a3*k1[i] + b3*k2[i];
	derivs (*t+q3*(*h), ytmp, dydt);
	for (i=0; i<dim; i++) k3[i] = *h * dydt[i];

/* k4 */
	for (i=0; i<dim; i++) ytmp[i] = y[i] + a4*k1[i] + b4*k2[i] + c4*k3[i];
	derivs (*t+q4*(*h), ytmp, dydt);
	for (i=0; i<dim; i++) k4[i] = *h * dydt[i];

/* k5 */
	for (i=0; i<dim; i++) ytmp[i] = y[i] + a5*k1[i] + b5*k2[i] + c5*k3[i] + d5*k4[i];
	derivs (*t+q5*(*h), ytmp, dydt);
	for (i=0; i<dim; i++) k5[i] = *h * dydt[i];

/* k6 */
	for (i=0; i<dim; i++) ytmp[i] = y[i] + a6*k1[i] + b6*k2[i] + c6*k3[i] + d6*k4[i] + e6*k5[i];
	derivs (*t+q6*(*h), ytmp, dydt);
	for (i=0; i<dim; i++) k6[i] = *h * dydt[i];

	/* Get the error and the 4th order approximation */
	err_max = 0.0;
	for (i=0; i<dim; i++) {
		double err;
		err = fabs (r1*k1[i] + r3*k3[i] + r4*k4[i] + r5*k5[i] + r6*k6[i]);
		if (err > err_max) err_max = err;
	}
	/* Move time forward if err_max is appropriate */
	if (err_max < epsilon || (*h) < 2.0*hmin) {
		for (i=0; i<dim; i++) {
			y[i] += y41*k1[i] + y43*k3[i] + y44*k4[i] + y45*k5[i];
		}
		(*t) += (*h);
	}

	/* Set s (the scaling factor for h)
	 * And don't let it be too big or small */
	double s;
	s = pow (epsilon / err_max, 0.25);
	if (s < 0.1) s = 0.1;
	if (s > 4.0) s = 4.0;
	/* Scale h for the next time step (or re-doing the current time step */
	(*h) *= s;

	return;
}

